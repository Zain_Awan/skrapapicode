<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => true,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA4P5W4ds:APA91bF-g_i0NbeWK44PMu9nnWwyqV3I9e_c0RtCYj5DNBjR5Nm23eyXwkjgIdp93e5Dv9FrtLjOhsz8H6sj61l5Mx74zS_rIxRuffFsBfOFConG5lbEkyHfegApAQVrRNPJhpstaZDd'),
        'sender_id' => env('FCM_SENDER_ID', '966339781083'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
