<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
     /*'mangopay' => [
      'env'    => env('MANGOPAY_ENV', 'sandbox'),  // or "production"
      'key'    => env('MANGOPAY_KEY','byoot2017'),// your Mangopay client ID
      'secret' => env('MANGOPAY_SECRET','HriKxV3qyn6C0GbXkSVHddjvDb4GeUiVjc3WHmnXTtE7o9hkrN'), // your Mangopay client password
  */
    'mangopay' => [
        'env'    => env('MANGOPAY_ENV', 'sandbox'),  // or "production"
        'key'    => env('MANGOPAY_KEY','hussainalhilli'),// your Mangopay client ID
        'secret' => env('MANGOPAY_SECRET','HBymywqbLHQx9trO6EjGDvACH5qJkPKqz64Kcf9xdc1sEer1rD'),
     ],

];
