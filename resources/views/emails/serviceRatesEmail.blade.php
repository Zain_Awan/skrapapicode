<?php
?>
        <!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>Skrap : Waste collection, on-demand.</title>

</head>
<body>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<div style="width: 100%; margin: 20px auto;">
    <div style="background-color: #0099cc; padding: 5px;" >

        <img src="http://skrap.xyz/assets/images/white_logo_email.png" style="width: 150px;" >


    </div>
    <section style="width:100%; margin-right: auto; margin-left: auto; margin-top: 50px; margin-bottom: 50px;">
        <p style="font-family: 'Open Sans', sans-serif;">
            Hello Admin,
        </p>
        <p style="font-family: 'Open Sans', sans-serif;"><?php echo $title; ?>, please check details below.</p>
        <?php if($district_code){
            ?>
        <p style="font-family: 'Open Sans', sans-serif; padding-top: 20px; "><span style="color: #0099cc;"><b>District Code:</b></span> <span style="font-weight: bold;"> <?php echo $district_code; ?> </span></p>
        <?php
        }
        ?>
        <?php if($service_names){
            ?>
        <table border="1" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            <thead>
            <tr>
                <th style="padding:5px;" align="center" valign="top">Area Code</th>
                <th style="padding:5px;" align="center" valign="top">Borough</th>
               <?php
                foreach ($service_names as $serName){
                    echo '<th style="padding:5px;" align="center" valign="top">'.$serName->service_name.'</th>';
                }
                ?>
            </tr>
            </thead>
            <?php
            foreach ($service_rates as $serRate){
                ?>
            <tr>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['district_code'] ?>
                </td>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['Borough'] ?>
                </td>
                <?php /*if($serRate['service6']){*/?><!--
                <td style="padding:5px;" align="center" valign="top">
                    <?php /*echo $serRate['service6'] */?>
                </td>
                --><?php /*}*/?>
                <?php if($serRate['service7']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service7'] ?>
                </td>
                <?php }?>
                <?php if($serRate['service8']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service8'] ?>
                </td>
                <?php }?>
                <?php /*if($serRate['service9']){*/?><!--
                <td style="padding:5px;" align="center" valign="top">
                    <?php /*echo $serRate['service9'] */?>
                </td>
                --><?php /*}*/?>
                <?php if($serRate['service10']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service10'] ?>
                </td>
                <?php }?>
                <?php /*if($serRate['service11']){*/?><!--
                <td style="padding:5px;" align="center" valign="top">
                    <?php /*echo $serRate['service11'] */?>
                </td>
                --><?php /*}*/?>
                <?php if($serRate['service12']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service12'] ?>
                </td>
                <?php }?>
                <?php if($serRate['service13']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service13'] ?>
                </td>
                <?php }?>
                <?php if($serRate['service14']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service14'] ?>
                </td>
                <?php }?>
                <?php if($serRate['service16']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service16'] ?>
                </td>
                <?php }?>
                <?php if($serRate['service17']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service17'] ?>
                </td>
                <?php }?>
                <?php if($serRate['service18']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service18'] ?>
                </td>
                <?php }?>
                <?php if($serRate['service19']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service19'] ?>
                </td>
                <?php }?>
                <?php if($serRate['service20']){?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php echo $serRate['service20'] ?>
                </td>
                <?php }?>
                <?php /*if($serRate['service21']){*/?><!--
                <td style="padding:5px;" align="center" valign="top">
                    <?php /*echo $serRate['service21'] */?>
                </td>
                <?php /*}*/?>
                <?php /*if($serRate['service22']){*/?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php /*echo $serRate['service22'] */?>
                </td>
                <?php /*}*/?>
                <?php /*if($serRate['service23']){*/?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php /*echo $serRate['service23'] */?>
                </td>
                <?php /*}*/?>
                <?php /*if($serRate['service24']){*/?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php /*echo $serRate['service24'] */?>
                </td>
                <?php /*}*/?>
                <?php /*if($serRate['service25']){*/?>
                <td style="padding:5px;" align="center" valign="top">
                    <?php /*echo $serRate['service25'] */?>
                </td>
                --><?php /*}*/?>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <?php }
        ?>
        <p style="font-family: 'Open Sans', sans-serif;">
            Many thanks,
        </p>
        <p style="font-family: 'Open Sans', sans-serif;">
            Skrap Team
        </p>
    </section>

    <div class="container-fluid" style="margin-top: 0px;  background-image: url(http://skrap.xyz/assets/images/contact_bg.png);width: 100%;height: 20vh;    min-height: 35vh; padding: 0;  background-size: cover; background-position-x: center;    background-position-y: center;margin-left: auto;margin-right: auto;">

        <section style="padding-top:30px; text-align: center;">

            <img src="http://skrap.xyz/assets/images/fb_ico.png" style="width: 30px;" alt="Download Skrap app from play store">
            <img src="http://skrap.xyz/assets/images/twiter_ico.png" style="width: 30px;" alt="Download Skrap app from play store">
            <img src="http://skrap.xyz/assets/images/linkedin_ico.png" style="width: 30px;" alt="Download Skrap app from play store">


        </section>

        <section style="width: 50%; margin:30px auto; text-align: center;">

            <p class=""><img src="http://skrap.xyz/assets/images/google_btn.png"  alt="Download Skrap app from play store">
            </p>

        </section>

        <section style="width: 100%; margin:30px auto; text-align: center;">
            <img src="http://skrap.xyz/assets/images/email-outline.png" style="width: 14px;">
            info@skrap.xyz
            |
            <img src="http://skrap.xyz/assets/images/web.png" style="width: 14px;">
            www.skrap.xyz
        </section>

    </div>

</div>

</body>
</html>