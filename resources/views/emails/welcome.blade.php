<?php foreach($data as $key){
    
}
?>
        <!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>Skrap : Waste collection, on-demand.</title>

</head>
<body>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<div style="width: 100%; margin: 20px auto;">
    <div style="background-color: #0099cc; padding: 5px;" >

        <img src="http://skrap.xyz/assets/images/white_logo_email.png" style="width: 150px;" >


    </div>
    <section style="width:85%; margin-right: auto; margin-left: auto; margin-top: 50px; margin-bottom: 50px;">
        <p style="font-family: 'Open Sans', sans-serif;">
            Hello,
        </p>
        <p style="font-family: 'Open Sans', sans-serif;">Thank you for signing up with Skrap. Login to get started with the following details below.</p>
        <?php $user_name = $data['user_name']; ?>
        <?php $user_pwd = $data['user_pwd']; ?>
        <p style="font-family: 'Open Sans', sans-serif; padding-top: 20px; "><span style="color: #0099cc;"><b>User Name:</b></span> <?php echo $user_name; ?></p>

        <p style="font-family: 'Open Sans', sans-serif;padding-bottom: 20px;"><span style="color: #0099cc;"><b>User Password:</b></span> <?php echo $user_pwd; ?></p>



        <p style="font-family: 'Open Sans', sans-serif;">
            Your account is now up and running, you are able to order immediately.
        </p>
        <p style="font-family: 'Open Sans', sans-serif;">
            Many thanks,
        </p>
        <p style="font-family: 'Open Sans', sans-serif;">
            Skrap Team
        </p>
    </section>

    <div class="container-fluid" style="margin-top: 0px;  background-image: url(http://skrap.xyz/assets/images/contact_bg.png);width: 100%;height: 20vh;    min-height: 35vh; padding: 0;  background-size: cover; background-position-x: center;    background-position-y: center;margin-left: auto;margin-right: auto;">

        <section style="padding-top:30px; text-align: center;">

            <img src="http://skrap.xyz/assets/images/fb_ico.png" style="width: 30px;" alt="Download Skrap app from play store">
            <img src="http://skrap.xyz/assets/images/twiter_ico.png" style="width: 30px;" alt="Download Skrap app from play store">
            <img src="http://skrap.xyz/assets/images/linkedin_ico.png" style="width: 30px;" alt="Download Skrap app from play store">


        </section>

        <section style="width: 50%; margin:30px auto; text-align: center;">

            <p class=""><img src="http://skrap.xyz/assets/images/google_btn.png"  alt="Download Skrap app from play store">
            </p>

        </section>

        <section style="width: 100%; margin:30px auto; text-align: center;">
            <img src="http://skrap.xyz/assets/images/email-outline.png" style="width: 14px;">
            info@skrap.xyz
            |
            <img src="http://skrap.xyz/assets/images/web.png" style="width: 14px;">
            www.skrap.xyz
        </section>

    </div>

</div>

</body>
</html>