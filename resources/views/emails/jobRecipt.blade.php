<?php use Carbon\Carbon; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <title>Skrap : Waste collection, on-demand.</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700" rel="stylesheet">


</head>
<body>
<div style="width: 100%; margin: 20px auto;">
    <div style="background-color: #0099cc; padding:15px 10px 80px 10px;" >

        <div style="width:30%; float:left;">
            <img src="http://skrap.xyz/assets/images/white_logo_email.png" style="width: 150px;" >
        </div>
        <div style="width:60%; float: right; text-align: right; color: #FFF; padding:20px 30px 5px 10px; font-family:'Nunito Sans', sans-serif;  ">
            <span>
                Total: £ <?php echo $job_detail['transaction_cost']; ?>
                <br>
                <?php
                $seconds = $appointment_detail['appointment_date'] / 1000;
                echo $a_date = Carbon::createFromTimestamp($seconds,'Europe/London')->format('D, M d, Y');
                ?>
            </span>
        </div>
    </div>
    <p style="font-family: 'Open Sans', sans-serif; text-align: center; font-size: 14px; font-weight: bold;margin-top: 25px; display: flex; width: 80%; margin-right: auto; margin-left: auto;">
        Hi <?php echo $data['first_name']; ?>, here's your receipt for your order on <?php
        $seconds = $appointment_detail['appointment_date'] / 1000;
        echo $a_date = Carbon::createFromTimestamp($seconds,'Europe/London')->format('D, M d, Y');
        ?>
    </p>
   <?php
        if($job_detail['is_paid'] == 0){
            ?>
            <p style="font-family: 'Open Sans', sans-serif; text-align: center; font-size: 14px; font-weight: normal;margin-top: 25px; display: flex; width: 80%; margin-right: auto; margin-left: auto;">
            <?php
                $string = base64_encode('dhfihsdfhadfsd4f156128169544sdfsadf454s4f6s5d4fsd4s1fsd89f4s-'.$data['mobile_number'].'-dhfihsdfhadfsd4f156128169544sdfsadf454s4f6s5d4fsd4s1fsd89f4s-'.base64_decode($data['user_pwd']).'-28169544sdfsadf454s4f6s5d4fsd4s1fsd89f4s-'.$appointment_detail['appointment_id']);
                echo 'Payment for this order is pending &nbsp; <a href="'.$_ENV['FRONTEND'].'/login?ZGFua29nYWk='.$string.'">click here</a> &nbsp; to pay online through your credit/debit card.';

                ?>
            </p>
    <?php
        }
    ?>
    <section style="width:85%; margin-right: auto; margin-left: auto; margin-top: 25px; margin-bottom: 50px; border: 1px solid #0099cc;  border-radius: 10px;  overflow:hidden;   block:inline;">

        <div style="background: #f0f7ed;border-top-right-radius: 10px;    border-top-left-radius: 10px;">
            <p style="font-size: 25px; color: #0099cc; border-bottom: 2px solid #0099cc; padding: 10px 0px 10px 10px; margin-top: 0px; font-family: 'Open Sans';">
                Service details</p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 0px 10px 10px; float: left; width: 45%; font-weight: bold; font-family:'Nunito Sans', sans-serif;">
                Booking ID</p>
            <p style="padding: 10px 0px 10px 10px; float: right; width: 45%; text-align: right; font-family:'Nunito Sans', sans-serif;">
                SK<?php echo $job_detail['job_id']; ?></p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 0px 10px 10px; float: left; width: 45%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                Service Type</p>
            <p style="padding: 10px 0px 10px 10px; float: right; width: 45%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                <?php if($appointment_detail['appointment_type'] == 1){
                        echo 'Grab Hire';
                } else if($appointment_detail['appointment_type'] == 2){
                        echo 'Skip Hire';
                } else if($appointment_detail['appointment_type'] == 3){
                        echo 'Junk Removal';
                } ?></p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 0px 10px 10px; float: left; width: 45%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                Service Name</p>
            <p style="padding: 10px 0px 10px 10px; float: right; width: 45%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                <?php if($appointment_detail['appointment_type'] == 2 && $job_detail['skip_loc_type'] == 0){
                        echo $service_detail['service_name'].' (Deliver)';
                        }else if ($appointment_detail['appointment_type'] == 2 && $job_detail['skip_loc_type'] == 2){
                        echo $service_detail['service_name'].' (Wait & Load)';
                    }else{
            echo $service_detail['service_name'];
            }
                ?>
            </p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 0px 10px 10px; float: left; width: 45%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                Additional Services</p>
            <p style="padding: 10px 0px 10px 10px; float: right; width: 45%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                N/A</p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 0px 10px 10px; float: left; width: 45%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                <?php
                if($appointment_detail['appoint_end_time'] == '' || $appointment_detail['appoint_end_time'] == null){
                    echo 'Job start time';
                }else{
                    echo 'Completed on';
                }
                    ?>
                </p>
            <p style="padding: 10px 0px 10px 10px; float: right; width: 45%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                <?php
                if($appointment_detail['appoint_end_time'] == '' || $appointment_detail['appoint_end_time'] == null){
                    $seconds = $appointment_detail['appointment_date'] / 1000;
                    echo $d_date = Carbon::createFromTimestamp($seconds,'Europe/London')->format('d, M, Y');
                    echo ' '.$d_time = Carbon::createFromTimestamp($seconds,'Europe/London')->format('H:i');
                }else{
                    $seconds = $appointment_detail['appoint_end_time'] / 1000;
                    echo $d_date = Carbon::createFromTimestamp($seconds,'Europe/London')->format('d, M, Y');
                    echo ' '.$d_time = Carbon::createFromTimestamp($seconds,'Europe/London')->format('H:i');
                }
                 ?></p></p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 0px 10px 10px; float: left; width: 45%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                Address</p>
            <p style="padding: 10px 0px 10px 10px; float:right; ; width: 45%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                <?php echo $job_detail['job_address']; ?></p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 10px 10px 10px; float: left; width: 35%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                Payment Status</p>
            <p style="padding: 10px 10px 10px 10px; float: right; width: 35%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                <?php
                if($job_detail['is_paid'] == 0){
                    echo 'Pending';
                }else{
                    echo 'Paid';
                }
                ?>
            </p>
        </div>
        <div style="clear: both;"></div>
        <div style="background: #f0f7ed;  width: auto;">
            <p style="font-size: 25px; color: #0099cc; border-bottom: 2px solid #0099cc; padding: 10px 0px 10px 10px; margin-top: 0px; font-family: 'Open Sans';">
                Cost break down</p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 10px 10px 10px; float: left; width: 35%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                Service cost</p>
            <p style="padding: 10px 10px 10px 10px; float: right; width: 35%; text-align: right; font-family:'Nunito Sans', sans-serif;">
                £ <?php echo $job_detail['service_rate']; ?></p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 10px 10px 10px; float: left; width: 35%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                Promotion</p>
            <p style="padding: 10px 10px 10px 10px; float: right; width: 35%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                £ <?php echo $job_detail['discount']; ?></p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto; ">
            <p style="padding: 10px 10px 10px 10px; float: left; width: 35%; font-weight: bold;border-top: 1px solid #ececec; border-bottom: 1px solid #ececec; font-family:'Nunito Sans', sans-serif;">
                Sub total</p>
            <p style="padding: 10px 10px 10px 10px; float: right; width: 35%; text-align: right;border-top: 1px solid #ececec;border-bottom: 1px solid #ececec; font-family:'Nunito Sans', sans-serif;">
                £ <?php echo $job_detail['transaction_cost']; ?></p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 10px 10px 10px; float: left; width: 35%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                VAT 20%</p>
            <p style="padding: 10px 10px 10px 10px; float: right; width: 35%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                £ <?php $discount = 20/100;
                echo $discount = round($job_detail['transaction_cost'] * $discount);
                ?> </p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto;">
            <p style="padding: 10px 10px 10px 10px; float: left; width: 35%; font-weight: bold;font-family:'Nunito Sans', sans-serif;">
                Net Cost</p>
            <p style="padding: 10px 10px 10px 10px; float: right; width: 35%; text-align: right;font-family:'Nunito Sans', sans-serif;">
                £ <?php echo $afterVat = $job_detail['transaction_cost'] - $discount;?></p>
        </div>
        <div style="clear: both;"></div>
        <div style="width: 85%;margin-right: auto; margin-left: auto; border-top: 1px solid #ececec;border-bottom: 1px solid #ececec; background-color: #f0f7ed; height: 60px">
            <p style="padding: 10px 10px 10px 10px; float: left; width: 35%; font-weight:bold; font-family:'Nunito Sans', sans-serif;">
                Total Cost</p>
            <p style="padding: 10px 10px 10px 10px; float: right; width: 35%; text-align: right; font-family:'Nunito Sans', sans-serif;">
                £ <?php echo $job_detail['transaction_cost']; ?></p>
        </div>
        <div style="clear: both;"></div>
        <p style="font-family: 'Open Sans', sans-serif;  text-align: center; font-size: 10px;">
            Skrap VAT Number: 290937863
        </p>

    </section>
    <div style="clear: both;"></div>
    <p style="font-family: 'Open Sans', sans-serif; padding-top: 10px; padding-bottom: 20px; text-align: center; font-size: 20px;">
        Thank you for using Skrap Service.
    </p>
    <div style="clear: both;"></div>
    <section style="width: 100%; margin:30px auto; text-align: center; font-family:'Nunito Sans', sans-serif; background: #0099cc; padding: 20px 0px; color: #FFF;">

        info@skrap.xyz &nbsp;  &nbsp;         |  &nbsp;&nbsp;          www.skrap.xyz
    </section>
</div>
</div>
</body>
</html>