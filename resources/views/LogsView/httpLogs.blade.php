<?php
/**
 * Created by PhpStorm.
 * User: Zain Awan
 * Date: 09/03/2018
 * Time: 9:42 PM
 */
?>
<html>
<head>
    <meta charset="utf-8" />
    <title>Skrap Logs</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/datatables/DT_bootstrap.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.css" rel="stylesheet" />
    <link href="assets/css/style-default.css" rel="stylesheet" id="style_color" />
    <style>
        .container{
            max-width: 100% !important;
            width: 100% !important;
            text-align: left !important;
            padding:0 !important;
            margin:0 !important;
            margin-bottom: -121px !important;
            margin-top: -298px !important;
        }
        body{
            color:#888888 !important;
            background: #222 !important;
        }
        table{
            background-color: transparent !important;
            box-shadow: none !important;
            border: 1px solid #ddd !important;
            margin: 0 !important;
            border-collapse: collapse !important;
        }
        tr{
            font-weight: bold;
            font-family: "MyriadPro-Light";
            font-size: 16px !important;
        }
        h2{
            font-weight: bolder;
            font-family: "MyriadPro-Light";
        }
        pre{
            width:228%;
            font-weight: normal;
        }
        h4{
            float: none !important;
            font-size: 47px !important;
            font-weight: normal !important;
            padding: 26px 11px 10px 15px !important;
            line-height: 0px !important;
            margin: 0 !important;
            font-family: 'MyriadPro-Regular';
        }
        .widget.red .widget-title {
            height:50px;
            text-align: center;
        }
        .exception-summary{
            padding:0 !important;
            margin:0 !important;
            display: none;
            width:500px !important;
        }
        span{
            text-align: left !important;
            font-size: 20px !important;
        }
        .block{
            text-align: left !important;
            font-size: 20px !important;
            padding:0 !important;
            margin:0 !important;
        }
        a{
            font-size: 16px !important;
        }
    </style>
</head>
<body>
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE widget-->
        <div class="widget red">
            <div class="widget-title">
                <h4>SKRAP API LOGS</h4>
            </div>
            <div class="widget-body">
                <table class="table table-striped table-bordered" id="sample_1">
                    <thead>
                    <tr style="font-size: 14px;">
                        <th>Request Url</th>
                        <th class="hidden-phone">Request Method</th>
                        <th class="hidden-phone">Status Code</th>
                        <th class="hidden-phone">Request Ip</th>
                        <th class="hidden-phone">Requested Time</th>
                        <th class="hidden-phone">Request Duration/Ms</th>
                        <th class="hidden-phone">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $count = 0;
                    foreach($data as $logs){
                    ?>
                    <tr>
                        <td><?php echo $logs['Request_Url']; ?></td>
                        <td><?php echo $logs['Request_method']; ?></td>
                        <td><?php echo $logs['Request_Status_code']; ?></td>
                        <td><?php echo $logs['Request_Ip']; ?></td>
                        <td><?php echo $logs['Requested at']; ?></td>
                        <td><?php echo $logs['Request time']; ?></td>
                        <td><button onclick="show_detail(<?php echo $count ;?>)" id="<?php echo 'show'.$count; ?>" class="btn btn-inverse"><i class="icon-chevron-down"></i></button><button style="display: none;" onclick="hide_detail(<?php echo $count ;?>)" id="<?php echo 'hide'.$count; ?>" class="btn btn-inverse"><i class="icon-chevron-up"></i></button></td>
                    <tr id='<?php echo 'detail'.$count; ?>' style="display: none;">
                        <td colspan="1" style="display: none;"><?php echo $logs['Request_Url']; ?></td>
                        <td colspan="6" style="width:100%;display: block;"><h2>Request </h2><pre><?php echo $logs['Request']; ?></pre></td>
                        <td colspan="6" style="width:100%;display: block;"><h2>Response </h2><pre><?php echo $logs['Response']; ?></pre></td>
                        <td colspan="1" style="display: none;"><?php echo $logs['Request_method']; ?></td>
                        <td colspan="1" style="display: none;"><?php echo $logs['Request_Status_code']; ?></td>
                        <td colspan="1" style="display: none;"><?php echo $logs['Request_Ip']; ?></td>
                        <td colspan="1" style="display: none;"></td>
                    </tr>
                    </tr>
                    <?php
                    $count = $count+1;
                    }
                    ?>
                    </tbody>
                </table>

            </div>
        </div>
        <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
</body>
<script src="assets/js/jquery-1.8.3.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
<script type="text/javascript" src="assets/datatables/DT_bootstrap.js"></script>
<script>
    $('#sample_1').dataTable({
        "searching": true,
        "ordering": true,
        "lengthChange": true,
        "lengthMenu": [[25, 50, 100, 500, 1000],[25, 50, 100, 500, "Max"]],
        "pageLength": 22
    });
    function show_detail(id){
        $('#detail'+id).fadeIn();
        $('#show'+id).css('display','none');
        $('#hide'+id).css('display','block');
    }
    function hide_detail(id){
        $('#detail'+id).fadeOut();
        $('#hide'+id).css('display','none');
        $('#show'+id).css('display','block');
    }
</script>
</html>