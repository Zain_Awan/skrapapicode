<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class scheduleJobTbl extends Model
{
    protected $table = 'tbl_schedule_job';
    public $timestamps = false;
    protected function insertion($data){
    	$result = scheduleJobTbl::insert($data);
    	return $result;
    }
    protected function selectionById($jobid){
    	$result = scheduleJobTbl::select('schedule_date','job_type')->where('job_id','=',$jobid)->where('status','=',0)->where('is_delete','=',0)->get();
    	return $result;
    }
    protected function deletescheduleJob($job_id){
        $result = scheduleJobTbl::where('job_id',$job_id)->delete();
        return $result;
    }
}
