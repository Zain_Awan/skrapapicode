<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stripeChargeTbl extends Model
{
    protected $table = 'tbl_stripe_customer_charge';
    public $timestamps = false;
    protected function insertCharge($charge){
        $result = stripeChargeTbl::insert($charge);
        return $result;
    }
}
