<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class postCodeTbl extends Model
{
    protected $table = 'tbl_district_code';
    public $timestamps = false;

    protected function getAllPriceLess(){
        $result = DB::table('tbl_district_code')
            ->leftJoin('tbl_skip_rates', function($leftjoin) {
                $leftjoin->on('tbl_skip_rates.district_code','=','tbl_district_code.district_code');
            })
            ->select('tbl_district_code.district_id','tbl_district_code.district_code')
            ->WhereNull('tbl_skip_rates.district_code')
            ->groupby('district_code')
            ->get();
        return $result;
    }
}
