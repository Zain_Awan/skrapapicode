<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class stripeSourcesTbl extends Model
{
    protected $table = 'tbl_stripe_customer_sources';
    public $timestamps = false;

    protected function insertSource($source){
        $result = stripeSourcesTbl::insert($source);
        return $result;
    }
    protected function getSourceId($stripe_source_id){
        $result = stripeSourcesTbl::select('source_id')
            ->where('stripe_source_id','=',$stripe_source_id)
            ->where('is_delete','=',0)
            ->get();
        return $result;
    }
    protected function getSourceIdByCustomerId($customer_id){
        $result = stripeSourcesTbl::select('stripe_source_id')
            ->where('customer_id','=',$customer_id)
            ->where('is_delete','=',0)
            ->get();
        return $result;
    }
    protected function setDefaultSource($user_id, $source_id){
        $result = DB::table('tbl_stripe_customer')
            ->leftJoin('tbl_stripe_customer_sources','tbl_stripe_customer_sources.customer_id','=','tbl_stripe_customer.customer_id')
            ->where('tbl_stripe_customer.user_id', '=', $user_id)
            ->where('tbl_stripe_customer_sources.stripe_source_id','=',$source_id)
            ->where('tbl_stripe_customer_sources.is_delete','=',0)
            ->update(array('tbl_stripe_customer_sources.is_default'=>1));
        return $result;
    }
    protected function getDefaultSource($user_id){
        $result = DB::table('tbl_stripe_customer')
            ->leftJoin('tbl_stripe_customer_sources','tbl_stripe_customer_sources.customer_id','=','tbl_stripe_customer.customer_id')
            ->select('tbl_stripe_customer_sources.stripe_source_id')
            ->where('tbl_stripe_customer.user_id', '=', $user_id)
            ->where('tbl_stripe_customer_sources.is_default','=',1)
            ->where('tbl_stripe_customer_sources.is_delete','=',0)
            ->where('tbl_stripe_customer_sources.status','=',0)
            ->get();
        return $result;
    }
    protected function removeAllDefaultSources($user_id){
        $result = DB::table('tbl_stripe_customer')
            ->leftJoin('tbl_stripe_customer_sources','tbl_stripe_customer_sources.customer_id','=','tbl_stripe_customer.customer_id')
            ->where('tbl_stripe_customer.user_id', '=', $user_id)
            ->update(array('tbl_stripe_customer_sources.is_default'=>0));
        return $result;
    }
    protected function DeleteResource($source_id){
        $result = stripeSourcesTbl::where('stripe_source_id','=',$source_id)
            ->update(array('is_delete'=>1));
        return $result;
    }
}
