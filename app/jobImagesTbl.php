<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobImagesTbl extends Model
{
    protected $table = 'tbl_job_images';
    public $timestamps = false;

    protected function insertJobImages($data){
        $result = jobImagesTbl::insert($data);
        return $result;
    }
    protected function getJobImages($job_id){
       $result = jobImagesTbl::select('image_id','image_url','job_id','status','is_delete','save_date')
           ->where('job_id','=',$job_id)
           ->where('status','=',0)
           ->where('is_delete','=',0)
           ->get();
       return $result;
    }
    protected function deleteJobImages($job_id){
        $result = jobImagesTbl::where('job_id','=', $job_id)->delete();
        return $result;
    }
}
