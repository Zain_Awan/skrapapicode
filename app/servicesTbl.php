<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class servicesTbl extends Model
{
    protected $table = 'tbl_services';
    public $timestamps = false;

    protected function insertion($data){
    	$result = userTbl::insert($data);
     	return $result;
    }
    protected function selection($service_type){
        if($service_type == 5){
            $result = servicesTbl::select('service_id','service_name','description','parent_id as service_type')->where('status','=', 0)->where('is_delete','=',0)->where('parent_id','!=',0)->get();
        }elseif ($service_type == 0){
            $result = servicesTbl::select('service_id','service_name','description','parent_id as service_type')->where('status','=', 0)->where('is_delete','=',0)->where('parent_id','=',0)->where('sub_cat','=',0)->get();
        }
        else{
            $result = servicesTbl::select('service_id','service_name','description','parent_id as service_type')->where('status','=', 0)->where('is_delete','=',0)->where('parent_id','=',$service_type)->get();
        }
        return $result;
    }
    protected function getServiceNames($ids){
        $result = servicesTbl::select('service_id','service_name','parent_id As service_type')
            ->where('parent_id','!=',0)
            ->whereIn('service_id',$ids)
            ->where('is_delete','=',0)
            ->where('status','=',0)
            ->get();
        return $result;
    }
    protected function selectSingle(){
    	
    }
    protected function updation(){

    }
    protected function deletion(){

    }
}
