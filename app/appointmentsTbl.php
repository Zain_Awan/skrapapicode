<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;

class appointmentsTbl extends Model
{
    protected $table = 'tbl_appointments';
    public $timestamps = false;
    protected function insertion($data){
        try{
            $result = appointmentsTbl::insert($data);
            return $result;
        }catch(Exception $exception){
            return $exception;
        }

    }
    protected function selectSingle($job_id){
    	$res = appointmentsTbl::select('appointment_id','appointment_date','pickup_date','cancel_status','save_date','appointment_status','heading_time','appoint_start_time','appoint_end_time','appointment_type')
            ->where('status','=',0)
            ->where('is_delete','=',0)
            ->where('job_id','=',$job_id)
            ->get();
        return $res;
    }
    protected function cancelAppointment($id,$data){
    	$result = appointmentsTbl::where('job_id','=',$id)->update($data);
        return $result;
    }
    protected function updateAppointmentStatus($id,$status){
    	$res = appointmentsTbl::where('appointment_id','=',$id)->update($status);
    	return $res;
    }
    protected function getAppointment($id){
    	$res = appointmentsTbl::select('appointment_id','job_id','appointment_date','cancel_status','appointment_status','save_date')
            ->where('appointment_id','=',$id)
            ->where('status','=',0)
            ->where('is_delete','=',0)
            ->get();
    	return $res;
    }
    protected function deleteAppointment($job_id){
        $res = appointmentsTbl::where('job_id',$job_id)->update(array('is_delete'=>1));
        return $res;
    }
    protected function getJobIdByAppointment($app_id){
        $res = appointmentsTbl::where('appointment_id',$app_id)->select('job_id')->get();
        return $res;
    }
    protected function getCronJobData($time,$range,$current,$alert_id){
        $jobCoupon = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
           /* ->leftJoin('tbl_notifications',function($leftjoin){
                $leftjoin->on('tbl_notifications.job_id', '=', 'tbl_job.job_id')
                    ->on('tbl_notifications.status','=',DB::raw(0))
                    ->on('tbl_notifications.is_delete','=',DB::raw(0));
            })*/
            ->leftJoin('tbl_users', 'tbl_job.driver_user_id', '=', 'tbl_users.user_id')
            ->select('tbl_appointments.appointment_id','tbl_job.job_id',DB::raw(''.$current.' - tbl_job.job_start_time As Diffrence'),'tbl_users.user_id','tbl_users.device_id','tbl_users.ios_id')
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.appointment_status','!=',3)
            ->where('tbl_appointments.cancel_status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_job.status','!=',2)
            ->where('tbl_job.is_delete','=',0)
            //->WhereNull('tbl_notifications.alert_id')
            ->whereRaw('('.$current.' - tbl_job.job_start_time <= '.$time.' and '.$current.' - tbl_job.job_start_time >= '.$range.')')
            ->get();
        return $jobCoupon;
    }
    protected function getAppointmentTime($appointment_id){
        $res = appointmentsTbl::select('appointment_id','save_date')
            ->where('appointment_id','=',$appointment_id)
            ->where('status','=',0)
            ->where('is_delete','=',0)
            ->get();
        return $res;
    }
    protected function updateJobAppointment($appointment_id,$jobData){
        $updated = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.appointment_id','=',$appointment_id)
            ->update($jobData);
        return $updated;
    }
    protected function getAppointmentId($job_id){
        $res = appointmentsTbl::select('appointment_id')
            ->where('status','=',0)
            ->where('is_delete','=',0)
            ->where('job_id','=',$job_id)
            ->get();
        return $res;
    }
    protected function updatePickup($job_id,$update_appointment){
        $result = appointmentsTbl::where('job_id','=',$job_id)
            ->update($update_appointment);
        return $result;
    }
}
