<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class jobRequestTbl extends Model
{
    protected $table = 'tbl_job_req';
    public $timestamps = false;

	protected function insertion($data){
    	$result = jobRequestTbl::insert($data);
    	return $result;
    }
    protected function selectSingle($job_id,$driver_user_id){
    	$result = jobRequestTbl::select('req_id')->where('job_id','=',$job_id)->where('driver_user_id','=',$driver_user_id)->where('status','=',0)->where('is_delete','=',0)->get();
    	jobRequestTbl::where('req_id',$result[0]->req_id)->update(array('is_accepted'=>1));
        return $result;
    }
    protected function checkIfexist($user_id,$job_id){
        $result = jobRequestTbl::select('req_id')
            ->where('driver_user_id','=',$user_id)
            ->where('job_id','=',$job_id)
            ->get();
        return $result;
    }
    protected function getJobRequestUsers($job_id){
        $result = DB::table('tbl_job_req')
            ->leftJoin('tbl_users', 'tbl_users.user_id', '=', 'tbl_job_req.driver_user_id')
            ->select('device_id','ios_id','web_token')
            ->where('job_id','=',$job_id)
            ->get();
        return $result;
    }
}
