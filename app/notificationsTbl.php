<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class notificationsTbl extends Model
{
    protected $table = 'tbl_notifications';
    public $timestamps = false;
    protected function insertion($data){
    	$res = notificationsTbl::insertGetId($data);
    	return $res;
    }
    protected function selectById($user_id){
        $res = notificationsTbl::select('notification_id','user_id','notification_type','notification_data','title','body','save_date','status','is_delete')
            ->where('user_id','=', $user_id)
            ->where('status','=', 0)
            ->where('is_delete','=', 0)
            ->whereRaw('DATE_FORMAT(save_date,"%Y-%m-%d") = DATE_FORMAT(NOW(),"%Y-%m-%d")')
            ->orderBy('save_date','DESC')
            ->get();
        return $res;
    }
    protected function selectByIdCustomer(){
        $res = notificationsTbl::select('notification_id','user_id','notification_type','notification_data','title','body','save_date','status','is_delete')
            //->where('user_id','=', $user_id)
            ->where('status','=', 0)
            ->where('is_delete','=', 0)
            ->whereRaw('DATE_FORMAT(save_date,"%Y-%m-%d") BETWEEN DATE_FORMAT(save_date,"%Y-%m-%d") AND DATE_ADD(DATE_FORMAT(save_date,"%Y-%m-%d"),INTERVAL 7 DAY)')
            ->where('notification_type', '=',15)
            ->orderBy('save_date','DESC')
            ->get();
        return $res;
    }
    protected function selectByIdProvider($user_id){
        $res = notificationsTbl::select('notification_id','user_id','notification_type','notification_data','title','body','save_date','status','is_delete')
            ->where('user_id','=', $user_id)
            ->where('status','=', 0)
            ->where('is_delete','=', 0)
            ->where('read_status','=', 0)
            ->whereRaw('save_date >= NOW() - INTERVAL 1 MINUTE')
            //->whereRaw('DATE_FORMAT(save_date,"%Y-%m-%d") = "2018-04-17"')
            ->orderBy('save_date','DESC')
            ->get();
        return $res;
    }
    protected function updateNotiReadStatus($user_id){
        $res = notificationsTbl::where('user_id','=',$user_id)->where('notification_type','=',1)->update(array('read_status'=>1));
        return $res;
    }
}
