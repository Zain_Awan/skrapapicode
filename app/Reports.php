<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reports extends Model
{
    public $timestamps = false;

    protected function GetPaymentReport($user_id,$from_date,$to_date,$report_type){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->leftJoin('tbl_user_info', 'tbl_user_info.user_id','=', 'tbl_driver.user_id')
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.appointment_status',
                'tbl_appointments.cancel_status','tbl_appointments.appointment_type','tbl_job.job_id','tbl_job.transaction_cost','tbl_job.is_paid','tbl_job.job_address')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0);
            if($report_type == 2){
                $ProviderEarnings = $ProviderEarnings->whereBetween(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), [$from_date, $to_date]);
            } elseif ($report_type == 3){
                $ProviderEarnings = $ProviderEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', DB::raw('DATE_FORMAT(NOW(),"%Y-%m-%d")'));
            }
            else{
                $ProviderEarnings = $ProviderEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', $from_date);
            }
        $ProviderEarnings = $ProviderEarnings->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $ProviderEarnings;
    }
    protected function getTotalEarnings($user_id,$from_date,$to_date,$report_type){
        $TotalEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As totalEarnings'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0);
        if($report_type == 2){
            $TotalEarnings = $TotalEarnings->whereBetween(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), [$from_date, $to_date]);
        } elseif ($report_type == 3){
            $TotalEarnings = $TotalEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', DB::raw('DATE_FORMAT(NOW(),"%Y-%m-%d")'));
        }
        else{
            $TotalEarnings = $TotalEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', $from_date);
        }
        $TotalEarnings = $TotalEarnings->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $TotalEarnings;
    }
    protected function getProcessedEarnings($user_id,$from_date,$to_date,$report_type){
        $TotalEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As totalEarnings'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0);
        if($report_type == 2){
            $TotalEarnings = $TotalEarnings->whereBetween(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), [$from_date, $to_date]);
        } elseif ($report_type == 3){
            $TotalEarnings = $TotalEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', DB::raw('DATE_FORMAT(NOW(),"%Y-%m-%d")'));
        }
        else{
            $TotalEarnings = $TotalEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', $from_date);
        }
        $TotalEarnings = $TotalEarnings->whereRaw('tbl_job.is_paid = 3')
            ->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $TotalEarnings;
    }
    protected function getPendingEarnings($user_id,$from_date,$to_date,$report_type){
        $TotalEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As totalEarnings'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0);
        if($report_type == 2){
            $TotalEarnings = $TotalEarnings->whereBetween(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), [$from_date, $to_date]);
        } elseif ($report_type == 3){
            $TotalEarnings = $TotalEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', DB::raw('DATE_FORMAT(NOW(),"%Y-%m-%d")'));
        }
        else{
            $TotalEarnings = $TotalEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', $from_date);
        }
        $TotalEarnings = $TotalEarnings->whereRaw('(tbl_job.is_paid = 0 OR tbl_job.is_paid = 1 OR tbl_job.is_paid = 2)')
            ->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $TotalEarnings;
    }
    protected function getCancelledEarnings($user_id,$from_date,$to_date,$report_type){
        $TotalEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As totalEarnings'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',1)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0);
        if($report_type == 2){
            $TotalEarnings = $TotalEarnings->whereBetween(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), [$from_date, $to_date]);
        } elseif ($report_type == 3){
            $TotalEarnings = $TotalEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', DB::raw('DATE_FORMAT(NOW(),"%Y-%m-%d")'));
        }
        else{
            $TotalEarnings = $TotalEarnings->where(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), '=', $from_date);
        }
        $TotalEarnings = $TotalEarnings->whereRaw('(tbl_job.is_paid = 0 OR tbl_job.is_paid = 1 OR tbl_job.is_paid = 2 OR tbl_job.is_paid = 4)')
            ->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $TotalEarnings;
    }
}
