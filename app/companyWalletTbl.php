<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class companyWalletTbl extends Model
{
    protected $table = 'tbl_company_wallet';
    public $timestamps = false;

    protected function insertion($data){
        $result = companyWalletTbl::insert($data);
        return $result;
    }
    protected function selection(){
        $result = companyWalletTbl::select('company_wallet_id','company_user_id','wallet_id')
            ->where('is_delete',0)
            ->where('status',0)
            ->get();
        return $result;
    }
}
