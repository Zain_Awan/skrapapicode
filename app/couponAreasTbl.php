<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class couponAreasTbl extends Model
{
    protected $table = 'tbl_coupon_area';
    public $timestamps = false;

    protected function insertAreas($data){
        $result = couponAreasTbl::insert($data);
        return $result;
    }
}
