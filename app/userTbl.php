<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class userTbl extends Model
{
    protected $table = 'tbl_users';
    public $timestamps = false;

    protected function insertion($data){
    	userTbl::insert($data);
        $result = userTbl::select('user_id','user_pwd','user_name','otp','referal_id')->orderBy('user_id', 'desc')->first();
     	return $result;
    }
    protected function selection(){

        $result = userTbl::all();
        return $result;

    }
    protected function selectSingle($userName,$password,$user_type){
        if($user_type == 2){
            $result = userTbl::select('user_id','user_type','otp')->where('user_name',$userName)->where('user_pwd',$password)->where('status','=',0)->where('is_delete','=',0)->where('user_type','=',$user_type)->where('provider_verified','=',1)->get();
            return $result;
        }else{
            $result = userTbl::select('user_id','user_type','otp','referal_id')->where('user_name',$userName)->where('user_pwd',$password)->where('status','=',0)->where('is_delete','=',0)->where('user_type','=',$user_type)->get();
            return $result;
        }
    }
    protected function updateDeviceId($id,$data){
        $result = userTbl::where('user_id','=',$id)->update($data);
        return $result;
    }
    protected function changePassword($data, $user_id){
        $result = userTbl::where('user_id',$user_id)->update($data);
        return $result;
    }
    protected function checkPassword($user_id, $password){
        $result = userTbl::select('user_id')->where('user_pwd',$password)->where('user_id',$user_id)->get();
        return $result;
    }
    protected function getPassword($user_id){
        $result = userTbl::select('user_pwd')->where('user_id',$user_id)->get();
        return $result;
    }
    protected function updation(){

    }
    protected function verifyProvider($user_id,$verify){
        $result = userTbl::where('user_id',$user_id)->update(array('provider_verified'=>$verify));
        return $result;
    }
    protected function getIdByReferal($referal){
        $result = userTbl::select('user_id')->where('referal_id',$referal)->where('is_delete',0)->get();
        return $result;
    }
    protected function getCustomersListing(){
        $customerListing = DB::table('tbl_users')
            ->leftJoin('tbl_user_info', 'tbl_user_info.user_id', '=', 'tbl_users.user_id')
            ->select('tbl_users.user_id','tbl_users.user_type','tbl_users.device_type','tbl_user_info.first_name','tbl_user_info.last_name',
                'tbl_user_info.email','tbl_user_info.mobile_number','tbl_user_info.save_date')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_users.user_type','=',1)
            ->get();
        return $customerListing;
    }
    protected function DeleteUser($user_id,$mobile_number){
        $deleteUser = DB::table('tbl_users')
            ->leftJoin('tbl_user_info', 'tbl_user_info.user_id', '=', 'tbl_users.user_id')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_users.user_type','=',1)
            ->where('tbl_users.user_id','=',$user_id)
            ->update(array('tbl_user_info.mobile_number'=>$mobile_number,'tbl_users.user_name'=>$mobile_number,'tbl_user_info.email'=>null));
        return $deleteUser;
    }
    protected function getAllDeviceIds(){
        $result = userTbl::select('device_id')
            ->where('device_id','!=',Null)
            ->get();
        return $result;
    }
    protected function getUsersCount(){
        $result = userTbl::where('status','=',0)
            ->where('is_delete','=',0)
            ->where('user_type','=',1)
            ->count();
        return $result;
    }
    protected function getProvidersCount(){
        $result = DB::table('tbl_users')
            ->leftJoin('tbl_provider','tbl_provider.user_id','=','tbl_users.user_id')
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.user_type','=',2)
            ->count();
        return $result;
    }
    protected function getDriverDeviceIdsByJobId($job_id){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_users','tbl_job.driver_user_id','=','tbl_users.user_id')
            ->select('tbl_users.device_id','tbl_users.ios_id')
            ->where('tbl_job.job_id','=',$job_id)
            ->get();
        return $result;
    }
    protected function getCredentials($userName, $userType){
        $result = userTbl::select('user_name','user_pwd')->where('user_name',$userName)->where('user_type',$userType)->where('status','=',0)->where('is_delete','=',0)->get();
        return $result;
    }
    protected function getMarketingUsers(){
        $result = DB::table('tbl_users')
            ->leftJoin('tbl_user_info','tbl_user_info.user_id','=','tbl_users.user_id')
            ->select('tbl_user_info.mobile_number','tbl_users.ios_id','tbl_users.device_id')
            ->where('tbl_user_info.is_messages','=',0)
            ->where('tbl_users.user_type','=',1)
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->get();
        return $result;
    }
}
