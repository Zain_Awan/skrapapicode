<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cusWalletTbl extends Model
{
    protected $table = 'tbl_cust_wallet_info';
    public $timestamps = false;

    protected function insertion($data){
        $result = cusWalletTbl::insert($data);
        return $result;
    }
    protected function updateWallet($data, $user_id){
        $result = cusWalletTbl::where('user_id',$user_id)->update($data);
        return $result;
    }
    protected function selectCustId($user_id){
        $result = cusWalletTbl::select('customer_id')->where('user_id',$user_id)->get();
        return $result;
    }
    protected function checkUserWalletExists($user_id){
        $result = cusWalletTbl::select('customer_id','wallet_id')->where('user_id',$user_id)->get();
        return $result;
    }
}
