<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class providerServicesTbl extends Model
{
    protected $table = 'tbl_provider_services';
    public $timestamps = false;

    protected function insertion($data){
    	$result = providerServicesTbl::insert($data);
     	return $result;
    }
    protected function DeleteService($service_id, $provider_id){
        $result = providerServicesTbl::where('provider_id', '=', $provider_id)->where('service_id','=', $service_id)->update(array('is_delete'=>1));
        return $result;
    }
    protected function selection(){
    
    }
    protected function selectSingle(){
    	
    }
    protected function updation(){

    }
    protected function deletion(){

    }
}
