<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;

class timeSlotTbl extends Model
{
    protected $table = 'tbl_days_slots';
    public $timestamps = false;

    protected function todaysaSlots($day,$slot){
        if($slot == 0) {
            $timeSlots = DB::table('tbl_days_slots')
                ->join('tbl_time_slots', 'tbl_days_slots.slot_id', '=', 'tbl_time_slots.slot_id')
                ->join('tbl_days', 'tbl_days.day_id', '=', 'tbl_days_slots.day_id')
                ->select('tbl_time_slots.time_slot')
                ->where('tbl_time_slots.status', '=', 0)
                ->where('tbl_time_slots.is_delete', '=', 0)
                ->where('tbl_days_slots.status', '=', 0)
                ->where('tbl_days_slots.is_delete', '=', 0)
                ->where('tbl_days.day_name', '=', $day)
                ->get();
        }else{
            $timeSlots = DB::table('tbl_days_slots')
                ->join('tbl_time_slots', 'tbl_days_slots.slot_id', '=', 'tbl_time_slots.slot_id')
                ->join('tbl_days', 'tbl_days.day_id', '=', 'tbl_days_slots.day_id')
                ->select('tbl_time_slots.time_slot')
                ->where('tbl_time_slots.status', '=', 0)
                ->where('tbl_time_slots.is_delete', '=', 0)
                ->where('tbl_days_slots.status', '=', 0)
                ->where('tbl_days_slots.is_delete', '=', 0)
                ->where('tbl_days.day_name', '=', $day)
                ->where('tbl_time_slots.slot_id','=',$slot)
                ->get();

            }
        return $timeSlots;
    }
    protected function selectSlots(){
        $timeSlots = DB::table('tbl_time_slots')
            ->select('tbl_time_slots.time_slot')
            ->get();
        return $timeSlots;
    }
}
