<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class extentionTbl extends Model
{
    protected $table = 'tbl_skip_extention_cost';
    public $timestamps = false;

    protected function getSkipExtention(){
        $result = extentionTbl::select('extention_id','extention_time','extention_amount')
            ->where('is_delete','=',0)
            ->where('status','=',0)
            ->get();
        return $result;
    }
}
