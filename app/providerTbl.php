<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class providerTbl extends Model
{
    protected $table = 'tbl_provider';
    public $timestamps = false;

    protected function insertion($data){
        providerTbl::insert($data);
    	$result = providerTbl::select('provider_id')->orderBy('provider_id', 'desc')->first();
     	return $result;
    }
    protected function getProviderId($user_id){
        $result = providerTbl::select('provider_id')->where('user_id','=',$user_id)->where('status','=',0)->where('is_delete','=',0)->get();
        return $result;
    }
    protected function selection(){

    }
    protected function selectSingle(){
    	
    }
    protected function updation(){

    }
    protected function deletion(){

    }
    protected function getDriversCount($user_id){
        $result = DB::table('tbl_driver')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_provider.user_id','=',$user_id)
            ->count();
        return $result;
    }
    protected function getVehiclesCount($user_id){
        $result = DB::table('tbl_vehicals')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_vehicals.provider_id')
            ->where('tbl_vehicals.is_delete','=',0)
            ->where('tbl_vehicals.status','=',0)
            ->where('tbl_provider.user_id','=',$user_id)
            ->count();
        return $result;
    }
    protected function getJobsCount($user_id){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_provider.user_id','=',$user_id)
            ->count();
        return $result;
    }
    protected function getCurrentJobsCount($user_id){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_appointments.appointment_status','!=',3)
            ->where('tbl_appointments.cancel_status','!=',1)
            ->where('tbl_provider.user_id','=',$user_id)
            ->count();
        return $result;
    }
    protected function getCompletedJobsCount($user_id){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_appointments.appointment_status','=',3)
            ->where('tbl_appointments.cancel_status','!=',1)
            ->where('tbl_provider.user_id','=',$user_id)
            ->count();
        return $result;
    }
    protected function getCancelledJobsCount($user_id){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',1)
            ->where('tbl_provider.user_id','=',$user_id)
            ->count();
        return $result;
    }
    protected function getTotalEarnings($user_id){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->whereRaw('(tbl_job.is_paid = 0 OR tbl_job.is_paid = 1 OR tbl_job.is_paid = 2 OR tbl_job.is_paid = 3 OR tbl_job.is_paid = 4)')
            ->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $ProviderEarnings;
    }
    protected function getProcessedEarnings($user_id){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_job.is_paid','=',3)
            ->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $ProviderEarnings;
    }
    protected function getPendingEarnings($user_id){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->whereRaw('(tbl_job.is_paid = 0 OR tbl_job.is_paid = 1 OR tbl_job.is_paid = 2)')
            ->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $ProviderEarnings;
    }
    protected function getCancelledEarnings($user_id){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',1)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->whereRaw('(tbl_job.is_paid = 0 OR tbl_job.is_paid = 1 OR tbl_job.is_paid = 2 OR tbl_job.is_paid = 4)')
            ->where('tbl_provider.user_id','=',$user_id)
            ->get();
        return $ProviderEarnings;
    }
}
