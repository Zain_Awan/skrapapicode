<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerCompanyPayTbl extends Model
{
    protected $table = 'tbl_cust_comp_pay';
    public $timestamps = false;

    protected function insertion($data){
        $result = customerCompanyPayTbl::insert($data);
        return $result;
    }
    protected function UpdatePaymentData($data,$job_id){
        $result = customerCompanyPayTbl::where('job_id',$job_id)->update($data);
        return $result;
    }
    protected function getPaymentDetail($job_id){
        $result = customerCompanyPayTbl::select('payIn_id','company_wallet_info_id')->get();
        return $result;
    }
}
