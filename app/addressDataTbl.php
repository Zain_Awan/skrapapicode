<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class addressDataTbl extends Model
{
    protected $table = 'tbl_address_data';
    public $timestamps = false;
    protected function insertion($data){
        $result = addressDataTbl::insert($data);
        return $result;
    }
}
