<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobAndReqFailedTbl extends Model
{
    protected $table = 'tbl_failed_jobs_a_reqs';
    public $timestamps = false;

    protected function insertion($data){
        $result = jobAndReqFailedTbl::insert($data);
        return $result;
    }
    protected function getJobFailedReason($job_id){
        $result = jobAndReqFailedTbl::select('failed_reason')
            ->where('job_id','=',$job_id)
            ->get();
        return $result;
    }
}
