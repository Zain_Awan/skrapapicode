<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class driverTbl extends Model
{
    protected $table = 'tbl_driver';
    public $timestamps = false;

    protected function insertion($data){
        driverTbl::insert($data);
        $result = driverTbl::select('driver_id')->orderBy('driver_id', 'desc')->first();
        return $result;
    }
    protected function selection(){

    }
    protected function selectSingle(){
    	
    }
    protected function updateDriverOnlineStatus($id,$data){
        $result = driverTbl::where('user_id','=',$id)->update($data);
        return $result;
    }
    protected function deletion(){

    }
    protected function getDriverOnlineStatus($user_id){
        $result = driverTbl::select('online_status')->where('user_id',$user_id)->where('status',0)->where('is_delete',0)->get();
        return $result;
    }
    protected function updateDriverInfo($updateDriverDetail,$user_id){
        $result = driverTbl::where('user_id',$user_id)->update($updateDriverDetail);
        return $result;
    }
    protected function updateDriverJobReciveStatus($driver_id, $status){
        $result = driverTbl::where('driver_id',$driver_id)->update(array('driver_job_status'=>$status));
        return $result;
    }
    protected function getProviderDriverIds($provider_id){
        $result = driverTbl::select('driver_id')->where('provider_id',$provider_id)->where('status',0)->where('is_delete',0)->get();
        return $result;
    }
}
