<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class couponUserTbl extends Model
{
    protected $table = 'tbl_coupon_user';
    public $timestamps = false;

    protected function AddCouponUser($couponData){
        $result = couponUserTbl::insert($couponData);
        return $result;
    }
    protected function AddReferalCouponUser($couponData){
        couponUserTbl::insert($couponData);
        $result = couponUserTbl::select('coupon_user_id')->orderBy('coupon_user_id', 'desc')->first();
        return $result;
    }
    protected function deactivateAllCoupons($user_id){
        $result = couponUserTbl::where('user_id','=',$user_id)->update(array('is_active'=>0));
        return $result;
    }
    protected function activateCoupons($coupon_user_id){
        $result = couponUserTbl::where('coupon_user_id','=',$coupon_user_id)->update(array('is_active'=>1));
        return $result;
    }
    protected function updateUses($coupon_user_id, $uses){
        $result = couponUserTbl::where('coupon_user_id','=',$coupon_user_id)->update(array('uses'=>$uses,'is_active'=>0));
        return $result;
    }
    protected function countCoupon($user_id){
        $result = couponUserTbl::select('coupon_user_id')->where('referal_user_id','=',$user_id)->count();
        return $result;
    }
    protected function getReferalCouponId($coupon_user_id){
        $result = couponUserTbl::select('referal_coupon_user_id')->where('coupon_user_id','=',$coupon_user_id)->get();
        return $result;
    }
    protected function updateReferalCouponStatus($coupon_user_id){
        $result = couponUserTbl::where('coupon_user_id','=',$coupon_user_id)->update(array('status'=>0));
        return $result;
    }
}
