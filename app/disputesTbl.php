<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class disputesTbl extends Model
{
    protected $table = "tbl_disputes";
    public $timestamps = false;

    protected function insertion($data){
        $result = disputesTbl::insert($data);
        return $result;
    }
}
