<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobCancelTbl extends Model
{
    protected $table = 'tbl_job_cancel';
    public $timestamps = false;

	protected function insertion($data){
    	$result = jobCancelTbl::insert($data);
    	return $result;     	
    }
}
