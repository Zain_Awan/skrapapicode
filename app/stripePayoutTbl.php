<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stripePayoutTbl extends Model
{
    protected $table = 'tbl_stripe_customer_payout';
    public $timestamps = false;
}
