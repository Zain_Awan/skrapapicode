<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class rawQuery extends Model
{
    protected function getProviderUnAssignedVehicles($provider_user_id){
        $ProviderVehicles = DB::table('tbl_provider')
            ->leftJoin('tbl_vehicals', 'tbl_provider.provider_id', '=', 'tbl_vehicals.provider_id')
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
               $leftjoin->on('tbl_driver_vehicle.vehicle_id', '=', 'tbl_vehicals.vehicle_id')
               ->on('tbl_driver_vehicle.status','=',DB::raw(0))
               ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0));
            })
            ->select('tbl_vehicals.vehicle_id','tbl_vehicals.reg_number')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_vehicals.status','=',0)
            ->where('tbl_vehicals.is_delete','=',0)
            ->WhereNull('tbl_driver_vehicle.vehicle_id')
            ->where('tbl_provider.user_id','=',$provider_user_id)
            ->get();
        return $ProviderVehicles;
    }
    protected function getProviderServices($provider_user_id){
        $providerServices = DB::table('tbl_provider')
            ->leftJoin('tbl_provider_services', 'tbl_provider.provider_id', '=', 'tbl_provider_services.provider_id')
            ->leftJoin('tbl_services', 'tbl_provider_services.service_id', '=', 'tbl_services.service_id')
            ->select('tbl_services.service_id','tbl_services.service_name','tbl_provider_services.provider_id')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_provider_services.status','=',0)
            ->where('tbl_provider_services.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_provider.user_id','=',$provider_user_id)
            ->get();
        return $providerServices;
    }
    protected function  getProviderUnassignedServices($provider_user_id){
        $providerServices1 = DB::table('tbl_provider')
            ->leftJoin('tbl_provider_services',function($leftjoin) use($provider_user_id){
                $leftjoin->on('tbl_provider.provider_id', '=', 'tbl_provider_services.provider_id')
                    ->on('tbl_provider_services.status','=',DB::raw(0))
                    ->on('tbl_provider_services.is_delete','=',DB::raw(0))
                    ->on('tbl_provider.user_id','=', DB::raw($provider_user_id));
            })
            ->rightJoin('tbl_services', 'tbl_provider_services.service_id', '=', 'tbl_services.service_id')
            ->select('tbl_services.service_id','tbl_services.service_name')
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_services.parent_id','!=',0)
            ->WhereNull('tbl_provider_services.service_id');
        $providerServices2 = DB::table('tbl_provider')
            ->leftJoin('tbl_provider_services',function($leftjoin) use($provider_user_id){
                $leftjoin->on('tbl_provider.provider_id', '=', 'tbl_provider_services.provider_id')
                    ->on('tbl_provider_services.status','=',DB::raw(0))
                    ->on('tbl_provider_services.is_delete','=',DB::raw(0))
                    ->on('tbl_provider.user_id','=', DB::raw($provider_user_id));
            })
            ->rightJoin('tbl_services', 'tbl_provider_services.service_id', '=', 'tbl_services.service_id')
            ->select('tbl_services.service_id','tbl_services.service_name')
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_services.sub_cat','=',1)
            ->WhereNull('tbl_provider_services.service_id');
        $providerServices = $providerServices1->union($providerServices2)->get();
        return $providerServices;
    }
    protected function getDriverUnAssignedServices($driver_user_id,$provider_user_id){
        /*$DriverUnAssignedServices = DB::table('tbl_users')
            ->leftJoin('tbl_provider','tbl_users.user_id','=','tbl_provider.provider_id')
            ->leftJoin('tbl_provider_services','tbl_provider_services.provider_id','=','tbl_provider.provider_id')
            ->leftJoin('tbl_services', 'tbl_provider_services.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_driver_services','tbl_driver_services.service_id', '=', DB::raw('tbl_provider_services.service_id AND tbl_driver_services.is_delete = 0 AND tbl_driver_services.status = 0 AND tbl_driver_services.driver_id = '.$driver_user_id))
            ->select('tbl_services.service_id','tbl_services.service_name')
            ->where('tbl_provider_services.status','=',0)
            ->where('tbl_provider_services.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->WhereNull('tbl_driver_services.service_id')
            ->where('tbl_users.user_id',$provider_user_id)
            ->get();
        return $DriverUnAssignedServices;
        ->leftJoin('tbl_provider_services',function($leftjoin) use($provider_user_id) {
                $leftjoin->on('tbl_provider_services.provider_id', '=', 'tbl_provider.provider_id')
                    ->on('tbl_provider.user_id','=',DB::raw($provider_user_id));
            })
        */
        $DriverUnAssignedServices = DB::table('tbl_provider_services')
            ->leftJoin('tbl_services', 'tbl_provider_services.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_provider_services.provider_id')
            ->leftJoin('tbl_driver_services','tbl_driver_services.service_id', '=', DB::raw('tbl_provider_services.service_id AND tbl_provider_services.provider_id = tbl_provider.provider_id AND tbl_driver_services.is_delete = 0 AND tbl_driver_services.status = 0 AND tbl_driver_services.driver_id = '.$driver_user_id))
            ->select('tbl_services.service_id','tbl_services.service_name','tbl_provider_services.provider_id')
            ->where('tbl_provider_services.status','=',0)
            ->where('tbl_provider_services.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->WhereNull('tbl_driver_services.service_id')
            ->where('tbl_provider.user_id','=',$provider_user_id)
            ->get();
        return $DriverUnAssignedServices;
    }

    protected function getDriversJobRequest($service_id,$job_start_time,$job_end_time,$post_code){
        //TODO
        // ADD device id null check
        /*$resultquery = DB::table('tbl_users as users')
            ->leftJoin('tbl_driver', 'users.user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_driver_services', function($leftjoin){
                $leftjoin->on('tbl_driver_services.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_services.status','=',DB::raw(0))
                    ->on('tbl_driver_services.is_delete','=',DB::raw(0));
            })
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0));
            })
            ->select('users.user_id','users.device_id')
            ->where('users.status','=',0)
            ->where('users.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_driver.online_status','=',1)
            ->where('users.device_id','!=',null)
            ->where('tbl_driver_services.service_id','=',$service_id)
            ->whereIn('users.user_id', $ids)
            ->whereNotIN('users.user_id',function($query) use($job_start_time,$job_end_time,$ids){
                $query->from('tbl_users')
                    ->leftJoin('tbl_job', 'tbl_users.user_id', '=', 'tbl_job.driver_user_id')
                    ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
                    ->select('tbl_users.user_id')
                    ->whereBetween('tbl_appointments.appointment_date', [$job_start_time, $job_end_time])
                    ->orWhereBetween(DB::raw('tbl_appointments.appointment_date+1800000'), [$job_start_time, $job_end_time])
                ->where('tbl_users.user_id','=','users.user_id')
                ->where('tbl_appointments.appointment_status','!=',3)
                ->where('tbl_appointments.cancel_status','!=',1)
                    ->groupBy('tbl_users.user_id')
                ->get();
            })
            ->groupBy('users.user_id')
            ->get();*/

        $resultquery = DB::table('tbl_users as users')
            ->leftJoin('tbl_driver','tbl_driver.user_id','=','users.user_id')
            ->leftJoin('tbl_user_info','tbl_user_info.user_id','=','users.user_id')
            ->leftJoin('tbl_driver_service_areas','tbl_driver_service_areas.driver_id','=','tbl_driver.driver_id')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_driver_service_areas.district_code_id')
            ->leftJoin('tbl_driver_services', function($leftjoin){
                $leftjoin->on('tbl_driver_services.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_services.status','=',DB::raw(0))
                    ->on('tbl_driver_services.is_delete','=',DB::raw(0));
            })
            ->join('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0));
            })
            ->select('users.user_id','users.device_id','users.ios_id','tbl_user_info.first_name','tbl_user_info.last_name')
            ->where('users.status','=',0)
            ->where('users.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_driver.online_status','=',1)
            ->where('users.device_id','!=',null)
            ->orwhere('users.ios_id','!=',null)
            ->where('tbl_driver_services.service_id','=',$service_id)
            ->where('tbl_district_code.district_code','=',$post_code)
            ->where('tbl_driver.online_status','=',1)
            ->where('tbl_driver.driver_job_status','=',0)
            ->whereNotIN('users.user_id',function($query) use($job_start_time,$job_end_time){
                $query->from('tbl_users')
                    ->leftJoin('tbl_job', 'tbl_users.user_id', '=', 'tbl_job.driver_user_id')
                    ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
                    ->select('tbl_users.user_id')
                    /*->whereBetween('tbl_appointments.appointment_date', [$job_start_time, $job_end_time])
                    ->orWhereBetween(DB::raw('tbl_appointments.appointment_date+1800000'), [$job_start_time, $job_end_time])*/
                    ->where('tbl_users.user_id','=','users.user_id')
                    ->where('tbl_appointments.appointment_status','!=',3)
                    ->where('tbl_appointments.cancel_status','!=',1)
                    ->groupBy('tbl_users.user_id')
                    ->get();
            })
            ->groupBy('users.user_id')
            ->get();
        return $resultquery;
    }

    protected function getProvidersJobRequest($service_id,$job_start_time,$job_end_time,$post_code){
        $resultquery = DB::table('tbl_users as users')
            ->leftJoin('tbl_provider','tbl_provider.user_id','=','users.user_id')
            ->leftJoin('tbl_provider_service_areas','tbl_provider_service_areas.provider_id','=','tbl_provider.provider_id')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_provider_service_areas.district_code_id')
            ->leftJoin('tbl_provider_services', function($leftjoin){
                $leftjoin->on('tbl_provider_services.provider_id', '=', 'tbl_provider.provider_id')
                    ->on('tbl_provider_services.status','=',DB::raw(0))
                    ->on('tbl_provider_services.is_delete','=',DB::raw(0));
            })
            ->select('users.user_id','users.device_id','users.web_token','users.ios_id')
            ->where('users.status','=',0)
            ->where('users.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_provider.online_status','=',1)
            ->where('users.device_id','!=',null)
            ->orWhere('users.web_token','!=',null)
            ->orwhere('users.ios_id','!=',null)
            ->where('tbl_provider_services.service_id','=',$service_id)
            ->where('tbl_district_code.district_code','=',$post_code)
            ->groupBy('users.user_id')
            ->get();
        return $resultquery;
    }

    protected function getDeviceIdsDriver($job_id,$driver_user_id){
        $resultquery = DB::table('tbl_users')
            ->leftJoin('tbl_job_req', 'tbl_users.user_id', '=', 'tbl_job_req.driver_user_id')
            ->select('tbl_users.user_id','tbl_users.device_id','tbl_users.web_token','tbl_users.ios_id')
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_job_req.status','=',0)
            ->where('tbl_job_req.is_delete','=',0)
            ->where('tbl_job_req.job_id','=',$job_id)
            ->where('tbl_users.user_id','!=',$driver_user_id)
            ->where('tbl_users.web_token','!=',null)
            ->groupBy('tbl_users.user_id')
            ->get();
        return $resultquery;
    }
    protected function getDeviceIdsAssignDriver($driver_user_id){
        $resultquery = DB::table('tbl_users')
            ->select('tbl_users.device_id','tbl_users.ios_id')
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_users.user_id','=',$driver_user_id)
            ->groupBy('tbl_users.user_id')
            ->get();
        return $resultquery;
    }
    protected function getDeviceIdsCustomer($job_id){
        $resultquery = DB::table('tbl_users')
            ->leftJoin('tbl_job', 'tbl_job.customer_user_id', '=', 'tbl_users.user_id')
            ->select('tbl_users.user_id','tbl_users.device_id','tbl_users.ios_id')
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_job.status','!=',2)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_job.job_id','=',$job_id)
            ->groupBy('tbl_users.user_id')
            ->get();
        return $resultquery;
    }
    protected function getJobDetailSingle($user_col,$job_id){
        $resultquery = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.'.$user_col.'', '=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_rating', 'tbl_job.'.$user_col.'', '=', 'tbl_rating.user_id','tbl_rating.status', '=', 0,'tbl_rating.is_delete', '=', 0)
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_driver_vehicle', function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_services.service_id','tbl_services.service_name','tbl_rating.rating',DB::raw('AVG(tbl_rating.rating) As rating'),'tbl_job.job_id','tbl_job.customer_user_id','tbl_job.driver_user_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.job_date as date','tbl_job.status','tbl_job.is_schedule','tbl_job.skip_loc_type','tbl_job.comments','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_job.status','!=',2)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_job.job_id','=',$job_id)
            ->groupBy('tbl_user_info.user_id')
            ->get();
        return $resultquery;
    }
    protected function getProviderJobDetailSingle($appointment_id){
        $resultquery = DB::table('tbl_job')
            ->leftJoin('tbl_failed_jobs_a_reqs', 'tbl_job.job_id', '=', 'tbl_failed_jobs_a_reqs.job_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.driver_user_id', '=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_user_info as customer_info', 'tbl_job.customer_user_id', '=', 'customer_info.user_id')
            ->leftJoin('tbl_rating as driver_rating', 'tbl_job.driver_user_id', '=', 'driver_rating.user_id','driver_rating.status', '=', 0,'driver_rating.is_delete', '=', 0)
            ->leftJoin('tbl_rating as customer_rating', 'tbl_job.driver_user_id', '=', 'customer_rating.user_id','customer_rating.status', '=', 0,'customer_rating.is_delete', '=', 0)
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_driver_vehicle', function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_user_info.profile_pic','customer_info.first_name as cust_fname','customer_info.last_name as cust_lname','customer_info.mobile_number as cust_mobile','customer_info.profile_pic as cust_profile_pic',
                'tbl_services.service_id','tbl_services.service_name',DB::raw('AVG(driver_rating.rating) As driver_rating'),DB::raw('AVG(customer_rating.rating) As customer_rating'),
                'tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_appointments.job_id','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.job_start_time AS job_date','tbl_job.status','tbl_job.is_schedule','tbl_job.skip_req_days','tbl_job.skip_loc_type','tbl_job.is_permit','tbl_job.is_paid','tbl_job.comments',
                'tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.pickup_date','tbl_appointments.cancel_status','tbl_appointments.save_date','tbl_appointments.appointment_status','tbl_appointments.heading_time','tbl_appointments.appoint_start_time','tbl_appointments.appoint_end_time','tbl_appointments.appointment_type','tbl_appointments.is_delete',
                'tbl_vehicals.owner_name','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type',
                'tbl_failed_jobs_a_reqs.failed_id','tbl_failed_jobs_a_reqs.job_id','tbl_failed_jobs_a_reqs.job_request_failed','tbl_failed_jobs_a_reqs.job_accept_failed','tbl_failed_jobs_a_reqs.failed_reason','tbl_failed_jobs_a_reqs.comment','tbl_failed_jobs_a_reqs.save_date','tbl_failed_jobs_a_reqs.job_req_id')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            /*->where('tbl_job.status','=',0)*/
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_appointments.appointment_id','=',$appointment_id)
            ->get();
        return $resultquery;
    }
    protected function getCurrentJobsingle($user_col,$driverCust,$user_id){
        /*$subQuery = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.'.$user_col.'', '=', 'tbl_user_info.user_id')
            ->join('tbl_appointments AS appointmnet', 'tbl_job.job_id', '=', 'appointmnet.job_id')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select(DB::raw('(CASE WHEN appointmnet.appointment_status = 4 THEN FROM_UNIXTIME(appointmnet.pickup_date/1000, "%Y-%m-%d") WHEN appointmnet.appointment_status = 5 THEN FROM_UNIXTIME(appointmnet.pickup_date/1000, "%Y-%m-%d") WHEN appointmnet.appointment_status = 6 THEN FROM_UNIXTIME(appointmnet.pickup_date/1000, "%Y-%m-%d")
WHEN appointmnet.appointment_status = 0 THEN FROM_UNIXTIME(appointmnet.appointment_date/1000, "%Y-%m-%d") WHEN appointmnet.appointment_status = 1 THEN FROM_UNIXTIME(appointmnet.appointment_date/1000, "%Y-%m-%d") WHEN appointmnet.appointment_status = 2 THEN FROM_UNIXTIME(appointmnet.appointment_date/1000, "%Y-%m-%d")END) AS appointmentDate'))
            ->where('appointmnet.cancel_status','=',0)
            ->where('appointmnet.appointment_status','!=',3)
            ->where('appointmnet.is_delete','=',0)
            ->where('appointmnet.appointment_id','=','tbl_appointments.appointment_id');

        $resultquery = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.'.$user_col.'', '=', 'tbl_user_info.user_id')
            ->join('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_user_info.user_id','tbl_services.service_id','tbl_services.service_name','tbl_job.job_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.job_date','tbl_job.status','tbl_job.is_schedule','tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.pickup_date','tbl_appointments.cancel_status','tbl_appointments.save_date','tbl_appointments.appointment_status','tbl_appointments.heading_time','tbl_appointments.appoint_start_time','tbl_appointments.appoint_end_time','tbl_appointments.appointment_type','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->where('tbl_appointments.appointment_status','!=',3)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_job.'.$driverCust.'','=',$user_id)
//->whereRaw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d") =  DATE_FORMAT(NOW(),"%Y-%m-%d") or FROM_UNIXTIME(tbl_appointments.pickup_date/1000, "%Y-%m-%d") =  DATE_FORMAT(NOW(),"%Y-%m-%d")')
            ->where(DB::raw('DATE_FORMAT(NOW(),"%Y-%m-%d")'),'=', DB::raw(' ( ' . $subQuery->toSql() . ' )'))
            ->mergeBindings($subQuery)
            ->orderBy('tbl_job.job_start_tim','ASC')
            ->limit(1)
            ->get();*/
        $resultquery = DB::select("select `tbl_user_info`.`first_name`, `tbl_user_info`.`last_name`, `tbl_user_info`.`mobile_number`, `tbl_user_info`.`user_id`, `tbl_services`.`service_id`, `tbl_services`.`service_name`, `tbl_job`.`job_id`, `tbl_job`.`job_location_lat`, `tbl_job`.`job_location_lng`, `tbl_job`.`job_start_time`, `tbl_job`.`job_end_time`, `tbl_job`.`transaction_cost`, `tbl_job`.`job_address`, `tbl_job`.`job_date` as `date`, `tbl_job`.`status`, `tbl_job`.`is_schedule`,`tbl_job`.`skip_loc_type`, `tbl_job`.`comments`,`tbl_appointments`.`appointment_id`, `tbl_appointments`.`appointment_date`, `tbl_appointments`.`pickup_date`, `tbl_appointments`.`cancel_status`, `tbl_appointments`.`save_date`, `tbl_appointments`.`appointment_status`, `tbl_appointments`.`heading_time`, `tbl_appointments`.`appoint_start_time`, `tbl_appointments`.`appoint_end_time`, `tbl_appointments`.`appointment_type`, `tbl_vehicals`.`reg_number`, `tbl_vehicals`.`vehicle_type` from `tbl_job` left join `tbl_services` on `tbl_job`.`service_id` = `tbl_services`.`service_id` left join `tbl_user_info` on `tbl_job`.".$user_col." = `tbl_user_info`.`user_id` inner join `tbl_appointments` on `tbl_job`.`job_id` = `tbl_appointments`.`job_id` left join `tbl_driver` on `tbl_job`.`driver_user_id` = `tbl_driver`.`user_id` left join `tbl_driver_vehicle` on `tbl_driver_vehicle`.`driver_id` = `tbl_driver`.`driver_id` and `tbl_driver_vehicle`.`is_delete` = 0 and `tbl_driver_vehicle`.`status` = 0 left join `tbl_vehicals` on `tbl_driver_vehicle`.`vehicle_id` = `tbl_vehicals`.`vehicle_id` where `tbl_user_info`.`status` = 0 and `tbl_user_info`.`is_delete` = 0 and `tbl_job`.`status` = 0 and `tbl_job`.`is_delete` = 0 and `tbl_appointments`.`cancel_status` = 0 and `tbl_appointments`.`appointment_status` != 3 and `tbl_appointments`.`status` = 0 and `tbl_appointments`.`is_delete` = 0 and `tbl_services`.`status` = 0 and `tbl_services`.`is_delete` = 0 and `tbl_job`.`".$driverCust."` = ".$user_id." and DATE_FORMAT(NOW(),\"%Y-%m-%d\") = ( select (CASE WHEN appointmnet.appointment_status = 4 THEN FROM_UNIXTIME(appointmnet.pickup_date/1000, \"%Y-%m-%d\") WHEN appointmnet.appointment_status = 5 THEN FROM_UNIXTIME(appointmnet.pickup_date/1000, \"%Y-%m-%d\") WHEN appointmnet.appointment_status = 6 THEN FROM_UNIXTIME(appointmnet.pickup_date/1000, \"%Y-%m-%d\")
WHEN appointmnet.appointment_status = 0 THEN FROM_UNIXTIME(appointmnet.appointment_date/1000, \"%Y-%m-%d\") WHEN appointmnet.appointment_status = 1 THEN FROM_UNIXTIME(appointmnet.appointment_date/1000, \"%Y-%m-%d\") WHEN appointmnet.appointment_status = 2 THEN FROM_UNIXTIME(appointmnet.appointment_date/1000, \"%Y-%m-%d\")END) AS appointmentDate from `tbl_job` left join `tbl_services` on `tbl_job`.`service_id` = `tbl_services`.`service_id` left join `tbl_user_info` on `tbl_job`.".$user_col." = `tbl_user_info`.`user_id` inner join `tbl_appointments` as `appointmnet` on `tbl_job`.`job_id` = `appointmnet`.`job_id` left join `tbl_driver` on `tbl_job`.`driver_user_id` = `tbl_driver`.`user_id` left join `tbl_driver_vehicle` on `tbl_driver_vehicle`.`driver_id` = `tbl_driver`.`driver_id` and `tbl_driver_vehicle`.`is_delete` = 0 and `tbl_driver_vehicle`.`status` = 0 left join `tbl_vehicals` on `tbl_driver_vehicle`.`vehicle_id` = `tbl_vehicals`.`vehicle_id` where `appointmnet`.`cancel_status` = 0 and `appointmnet`.`appointment_status` != 3 and `appointmnet`.`is_delete` = 0 and `appointmnet`.`appointment_id` = tbl_appointments.appointment_id ) order by `tbl_job`.`job_start_time` asc limit 1");
        return $resultquery;
    }
    protected function getCurrentJobListing($user_col,$driverCust,$user_id){
        $resultquery = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.'.$user_col.'', '=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_services.service_id','tbl_services.service_name','tbl_job.job_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.job_date as date','tbl_job.status','tbl_job.is_schedule','tbl_job.skip_loc_type','tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.pickup_date','tbl_appointments.cancel_status','tbl_appointments.save_date','tbl_appointments.appointment_status','tbl_appointments.heading_time','tbl_appointments.appoint_start_time','tbl_appointments.appoint_end_time','tbl_appointments.appointment_type','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_job.status','!=',2)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0);
        $resultquery = $resultquery->where('tbl_appointments.appointment_status','!=',3)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_job.'.$driverCust.'','=',$user_id)
            ->orderBy(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), 'DESC')
            ->get();
        return $resultquery;
    }
    protected function getPendingJobListing($user_id){
        $resultquery = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.customer_user_id','=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_schedule_job', 'tbl_job.job_id','=', 'tbl_schedule_job.job_id')
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_services.service_id','tbl_services.service_name','tbl_job.job_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.status','tbl_job.is_schedule','tbl_schedule_job.schedule_date as job_date')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_job.customer_user_id','=',$user_id)
            ->where('tbl_job.driver_user_id','=',null)
            ->orderBy(DB::raw('FROM_UNIXTIME(tbl_job.job_date/1000, "%Y-%m-%d")'), 'DESC')
            ->get();
        return $resultquery;
    }
    protected function getAutoAcceptJobListing(){
        $resultquery = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.customer_user_id','=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_schedule_job', 'tbl_job.job_id','=', 'tbl_schedule_job.job_id')
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_services.service_id','tbl_services.service_name','tbl_job.job_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.status','tbl_job.is_schedule','tbl_schedule_job.schedule_date as job_date')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_job.driver_user_id','=',null)
            ->orderBy(DB::raw('FROM_UNIXTIME(tbl_job.job_date/1000, "%Y-%m-%d")'), 'DESC')
            ->get();
        return $resultquery;
    }
    protected function getProviderJobListing($user_id,$job_status,$driver_id,$job_type,$cancel_status){
        $resultquery = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.driver_user_id', '=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_user_info as customer_info', 'tbl_job.customer_user_id', '=', 'customer_info.user_id')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','customer_info.first_name as cust_fname','customer_info.last_name as cust_lname','customer_info.mobile_number as cust_mobile','tbl_services.service_id','tbl_services.service_name','tbl_job.job_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.job_date as date','tbl_job.status','tbl_job.is_schedule','tbl_job.skip_loc_type','tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.cancel_status','tbl_appointments.save_date','tbl_appointments.appointment_status','tbl_appointments.heading_time','tbl_appointments.appoint_start_time','tbl_appointments.appoint_end_time','tbl_appointments.appointment_type','tbl_vehicals.owner_name','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_provider.user_id','=',$user_id)
            ->orderBy(DB::raw('FROM_UNIXTIME(tbl_appointments.appointment_date/1000, "%Y-%m-%d")'), 'DESC');
        if($user_col = 'customer_user_id'){
            $resultquery = $resultquery->where('tbl_appointments.appointment_type','!=',3);
        }
        if(!empty($driver_id)){
            $resultquery = $resultquery->where('tbl_job.driver_user_id','=',$driver_id);
        }
        if(!empty($job_type)){
            $resultquery = $resultquery->where('tbl_appointments.appointment_type','=',$job_type);
        }
        if($cancel_status == 1){
            $resultquery = $resultquery->where('tbl_appointments.cancel_status','=',$cancel_status);
        }else{
            $resultquery = $resultquery->where('tbl_appointments.cancel_status','=',$cancel_status);
            if($job_status == 0){
                $resultquery = $resultquery->where('tbl_appointments.appointment_status','!=',3);
            }else{
                $resultquery = $resultquery->where('tbl_appointments.appointment_status','=',3);
            }

        }

        $resultquery = $resultquery->get();
        return $resultquery;
    }
    protected function getPreviousJobListing($user_col,$driverCust,$user_id){
    $resultquery = DB::table('tbl_job')
        ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
        ->leftJoin('tbl_user_info', 'tbl_job.'.$user_col.'', '=', 'tbl_user_info.user_id')
        ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
        ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
        ->leftJoin('tbl_driver_vehicle',function($leftjoin){
            $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                ->on('tbl_driver_vehicle.status','=',DB::raw(0));
        })
        ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
        ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_services.service_id','tbl_services.service_name','tbl_job.job_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.job_date as date','tbl_job.status','tbl_job.is_schedule','tbl_job.skip_loc_type','tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.pickup_date','tbl_appointments.cancel_status','tbl_appointments.save_date','tbl_appointments.appointment_status','tbl_appointments.heading_time','tbl_appointments.appoint_start_time','tbl_appointments.appoint_end_time','tbl_appointments.appointment_type','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
        ->where('tbl_user_info.status','=',0)
        ->where('tbl_user_info.is_delete','=',0)
        ->where('tbl_job.status','!=',2)
        ->where('tbl_job.is_delete','=',0)
        ->where('tbl_appointments.status','=',0)
        ->where('tbl_appointments.is_delete','=',0)
        ->where('tbl_services.status','=',0)
        ->where('tbl_services.is_delete','=',0)
        ->where('tbl_job.'.$driverCust.'','=',$user_id)
        ->where(function($q) {
            $q->where('tbl_appointments.appointment_status','=',3)
                ->orWhere('tbl_appointments.cancel_status','=',1);
        })
        ->orderBy('tbl_appointments.appointment_date', 'desc')
        ->get();
    return $resultquery;
    }
    protected function getAppointmentByJob($user_col,$job_id){
        $resultquery = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.'.$user_col.'', '=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_users', 'tbl_job.'.$user_col.'', '=', 'tbl_users.user_id')
            ->leftJoin('tbl_rating', 'tbl_job.'.$user_col.'', '=', 'tbl_rating.user_id','tbl_rating.status', '=', 0,'tbl_rating.is_delete', '=', 0)
            ->leftJoin('tbl_rating As jobRating', function($leftjoin) use($user_col){
                $leftjoin->on('jobRating.user_id', '=', 'tbl_job.'.$user_col.'')
                    ->on('jobRating.appointment_id','=','tbl_appointments.appointment_id');
            })
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_users.user_pwd','tbl_user_info.email','tbl_services.service_id','tbl_services.service_name','tbl_rating.rating',DB::raw('ROUND(AVG(tbl_rating.rating)) As rating'),'jobRating.rating As job_rating','tbl_job.job_id','tbl_job.customer_user_id','tbl_job.driver_user_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.is_paid','tbl_job.service_rate','tbl_job.permit_cost','tbl_job.discount','tbl_job.discount_rate','tbl_job.extention_id','tbl_job.extention_amount','tbl_job.is_coupon','tbl_job.coupon_id','tbl_job.job_address','tbl_job.job_date as date','tbl_job.status','tbl_job.is_schedule','tbl_job.skip_loc_type','tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.pickup_date','tbl_appointments.cancel_status','tbl_appointments.save_date','tbl_appointments.appointment_status','tbl_appointments.heading_time','tbl_appointments.appoint_start_time','tbl_appointments.appoint_end_time','tbl_appointments.appointment_type','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_job.status','!=',2)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_job.job_id','=',$job_id)
            ->get();
        return $resultquery;
    }
    protected function getAppointments($user_col,$appointment_id){
        $resultquery = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info', 'tbl_job.'.$user_col.'', '=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_rating', 'tbl_job.'.$user_col.'', '=', 'tbl_rating.user_id','tbl_rating.status', '=', 0,'tbl_rating.is_delete', '=', 0)
            ->leftJoin('tbl_rating As jobRating', function($leftjoin) use($user_col,$appointment_id){
                $leftjoin->on('jobRating.user_id', '=', 'tbl_job.'.$user_col.'')
                    ->on('jobRating.appointment_id','=',DB::raw($appointment_id));
            })
           ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select('tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_user_info.email','tbl_user_info.profile_pic','tbl_services.service_id','tbl_services.service_name','tbl_rating.rating',DB::raw('ROUND(AVG(tbl_rating.rating)) As rating'),'jobRating.rating As job_rating','tbl_job.job_id','tbl_job.customer_user_id','tbl_job.driver_user_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.service_rate','tbl_job.permit_cost','tbl_job.discount','tbl_job.discount_rate','tbl_job.extention_id','tbl_job.extention_amount','tbl_job.is_coupon','tbl_job.coupon_id','tbl_job.job_address','tbl_job.job_date as date','tbl_job.status','tbl_job.is_paid','tbl_job.is_schedule','tbl_job.skip_loc_type','tbl_job.comments','tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.pickup_date','tbl_appointments.cancel_status','tbl_appointments.save_date','tbl_appointments.appointment_status','tbl_appointments.heading_time','tbl_appointments.appoint_start_time','tbl_appointments.appoint_end_time','tbl_appointments.appointment_type','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_job.status','!=',2)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_appointments.appointment_id','=',$appointment_id)
            ->get();
        return $resultquery;
    }
    protected function getDriversListing($provider_id){
        $resultquery = DB::table('tbl_provider')
            ->leftJoin('tbl_driver', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->leftJoin('tbl_user_info', 'tbl_driver.user_id', '=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_driver_vehicle', function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftJoin('tbl_vehicals', function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.vehicle_id', '=', 'tbl_vehicals.vehicle_id')
                    ->on('tbl_vehicals.status','=',DB::raw(0))
                    ->on('tbl_vehicals.is_delete','=',DB::raw(0));
            })
            ->select('tbl_user_info.user_info_id','tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.email','tbl_user_info.mobile_number','tbl_driver.driver_id','tbl_driver.driver_job_status','tbl_driver.user_id','tbl_driver.online_status','tbl_driver.waste_carier_license','tbl_driver.driving_license','tbl_driver.insurance_cert','tbl_driver.recent_pic','tbl_driver.save_date','tbl_vehicals.vehicle_id','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_provider.user_id','=',$provider_id)
            ->groupBy('tbl_driver.user_id')
            ->get();
        return $resultquery;
    }
    protected function getDriver($driver_id){
        $resultquery = DB::table('tbl_driver')
            ->leftJoin('tbl_user_info', 'tbl_driver.user_id', '=', 'tbl_user_info.user_id')
            ->leftJoin('tbl_driver_vehicle', function($leftJoin)
            {
                $leftJoin->on('tbl_driver.driver_id', '=', 'tbl_driver_vehicle.driver_id')
                    ->on(DB::raw('tbl_driver_vehicle.status'), '=',DB::raw(0))
                    ->on(DB::raw('tbl_driver_vehicle.is_delete'), '=',DB::raw(0));
            })
            ->leftJoin('tbl_vehicals', function($leftJoin)
            {
                $leftJoin->on('tbl_driver_vehicle.vehicle_id', '=', 'tbl_vehicals.vehicle_id')
                    ->on(DB::raw('tbl_vehicals.status'), '=',DB::raw(0))
                    ->on(DB::raw('tbl_vehicals.is_delete'), '=',DB::raw(0));
            })
            ->select('tbl_user_info.user_info_id','tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.email','tbl_user_info.mobile_number','tbl_user_info.dob','tbl_user_info.building','tbl_user_info.street','tbl_user_info.town','tbl_user_info.country','tbl_user_info.post_code','tbl_driver.driver_id','tbl_driver.user_id','tbl_driver.online_status','tbl_driver.waste_carier_license','tbl_driver.driving_license','tbl_driver.insurance_cert','tbl_driver.recent_pic','tbl_driver.save_date','tbl_vehicals.vehicle_id','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type','tbl_vehicals.owner_name','tbl_vehicals.model','tbl_vehicals.make','tbl_driver_vehicle.is_delete')
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_driver.driver_id','=',$driver_id)
            ->get();
        return $resultquery;
    }
    protected function getDriverServices($driver_id){
        $resultquery = DB::table('tbl_driver')
            ->leftJoin('tbl_driver_services', 'tbl_driver.driver_id', '=', 'tbl_driver_services.driver_id')
            ->leftJoin('tbl_services', 'tbl_driver_services.service_id', '=', 'tbl_services.service_id')
            ->select('tbl_services.service_id','tbl_services.service_name')
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where('tbl_driver_services.status','=',0)
            ->where('tbl_driver_services.is_delete','=',0)
            ->where('tbl_driver.driver_id','=',$driver_id)
            ->get();
        return $resultquery;
    }
    protected  function getVehiclelisting($provider_id){
        $resultquery = DB::table('tbl_provider')
            ->leftJoin('tbl_vehicals', 'tbl_provider.provider_id', '=', 'tbl_vehicals.provider_id')
            ->select('tbl_vehicals.vehicle_id','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type','tbl_vehicals.owner_name','tbl_vehicals.make','tbl_vehicals.model','tbl_vehicals.doc_image','tbl_vehicals.save_date')
            ->where('tbl_vehicals.status','=',0)
            ->where('tbl_vehicals.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_provider.user_id','=',$provider_id)
            ->get();
        return $resultquery;
    }
    protected function getDeviceAndUserIds($appointment_id){
        $resultquery = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_users', 'tbl_job.customer_user_id', '=', 'tbl_users.user_id')
            ->select('tbl_users.device_id','tbl_users.ios_id','tbl_job.driver_user_id','tbl_job.customer_user_id')
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.appointment_id','=',$appointment_id)
            ->get();
        return $resultquery;
    }
    protected function getDeviceIdsByAppId($appointment_id,$user_type){
        $resultquery = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_users', $user_type, '=', 'tbl_users.user_id')
            ->select('tbl_users.device_id','tbl_users.ios_id',$user_type.' as noti_user_id','tbl_job.job_id')
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.appointment_id','=',$appointment_id)
            ->get();
        return $resultquery;
    }

    protected function getUserProfile($user_id){
        $user_profile = DB::table('tbl_users')
            ->leftJoin('tbl_user_info', 'tbl_users.user_id', '=', 'tbl_user_info.user_id')
            ->select('tbl_users.user_name','tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.email','tbl_user_info.mobile_number','tbl_user_info.dob','tbl_user_info.building','tbl_user_info.street','tbl_user_info.town','tbl_user_info.country','tbl_user_info.post_code','tbl_user_info.profile_pic','is_messages')
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_user_info.user_id','=',$user_id)
            ->get();
        return $user_profile;
    }
    protected function findDrivers($post_code){
        $drivers = DB::table('tbl_driver_service_areas')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_driver_service_areas.district_code_id')
            ->leftJoin('tbl_driver','tbl_driver.driver_id','=','tbl_driver_service_areas.driver_id')
            ->select('tbl_driver_service_areas.district_code_id','tbl_driver.user_id')
            ->where('tbl_district_code.district_code','=',$post_code)
            ->where('tbl_driver.online_status','=',1)
            ->get();
        return $drivers;
    }
    protected function getServiceRates($town,$post_code,$service,$service_type){
            if(empty($post_code) && !empty($town)){
                $skipRate = DB::table('tbl_skip_rates')
                    ->select('tbl_skip_rates.service_rate_id','tbl_skip_rates.'.$service.' as service_rate')
                    ->where('tbl_skip_rates.Borough','=',$town)
                    ->get();
            }else if(!empty($post_code) && empty($town)){
                $skipRate = DB::table('tbl_skip_rates')
                    ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_skip_rates.district_id')
                    ->select('tbl_skip_rates.service_rate_id','tbl_skip_rates.'.$service.' as service_rate','tbl_district_code.district_id')
                    ->where('tbl_district_code.district_code','=',$post_code)
                    ->get();
            }
        return $skipRate;
    }
    protected function getSkipPermitRate($town,$post_code){
        /*if(empty($post_code) && !empty($town)){
            $skipRate = DB::table('tbl_permit_rates')
                ->leftJoin('tbl_towns','tbl_towns.town_id','=','tbl_permit_rates.town_id')
                ->select('tbl_permit_rates.permit_rate_id','tbl_permit_rates.permit_rate')
                ->where('tbl_towns.town_name','=',$town)
                ->get();
        }else if(!empty($post_code) && empty($town)){
            $skipRate = DB::table('tbl_permit_rates')
                ->leftJoin('tbl_towns','tbl_towns.town_id','=','tbl_permit_rates.town_id')
                ->leftJoin('tbl_postcodes','tbl_postcodes.town_id','=','tbl_towns.town_id')
                ->select('tbl_permit_rates.permit_rate_id','tbl_permit_rates.permit_rate')
                ->where('tbl_postcodes.post_code','=',$post_code)
                ->get();
        }*/
        $skipRate = array('permit_rate_id'=>1,'permit_rate'=>50);
        return $skipRate;
    }

    protected function getCities(){
        $cities = DB::table('tbl_ukcities')
            ->select('tbl_ukcities.city_id','tbl_ukcities.city_name')
            ->get();
        return $cities;
    }
    protected function getTowns($cities){
        $towns = DB::table('tbl_uktowns')
            ->leftJoin('tbl_ukcities','tbl_ukcities.city_id','=','tbl_uktowns.city_id')
            ->select('tbl_uktowns.town_id','tbl_uktowns.town_name')
            ->whereIn('tbl_uktowns.city_id', $cities)
            ->get();
        return $towns;
    }
    protected function getdistrictCodes($town_ids){
        $district_codes = DB::table('tbl_uktowns')
            ->leftJoin('tbl_district_code','tbl_district_code.town_id','=','tbl_uktowns.town_id')
            ->Join('tbl_skip_rates','tbl_district_code.district_id','=','tbl_skip_rates.district_id')
            ->select('tbl_district_code.district_id','tbl_district_code.district_code')
            ->whereIn('tbl_district_code.town_id', $town_ids)
            ->get();
        return $district_codes;
    }
    protected function getUserIdAndWalletId($card_id){
        $mangoPayData = DB::table('tbl_cust_cards')
            ->leftJoin('tbl_cust_wallet_info','tbl_cust_wallet_info.customer_id','=','tbl_cust_cards.customer_id')
            ->select('tbl_cust_wallet_info.cust_wallet_id','tbl_cust_wallet_info.customer_id','tbl_cust_wallet_info.wallet_id','tbl_cust_cards.cust_card_id')
            ->where('tbl_cust_wallet_info.status', 0)
            ->where('tbl_cust_wallet_info.is_delete', 0)
            ->where('tbl_cust_cards.card_id', $card_id)
            ->get();
        return $mangoPayData;
    }
    protected function getPaymentDetailByAppId($appointment_id){
        $jobPaymentData = DB::table('tbl_appointments')
            ->leftJoin('tbl_job','tbl_job.job_id','=','tbl_appointments.job_id')
            ->leftJoin('tbl_stripe_customer_charge','tbl_stripe_customer_charge.job_id','=','tbl_appointments.job_id')
            ->select('tbl_stripe_customer_charge.stripe_charge_id','tbl_stripe_customer_charge.charge_id','tbl_stripe_customer_charge.source_id','tbl_stripe_customer_charge.customer_id','tbl_job.job_id','tbl_job.transaction_cost')
            ->where('tbl_appointments.appointment_id', $appointment_id)
            ->get();
        return $jobPaymentData;
    }
    protected function checkUser($mobile, $user_type, $email){
        $userExists = DB::table('tbl_users')
            ->leftJoin('tbl_user_info','tbl_user_info.user_id','=','tbl_users.user_id')
            ->select('tbl_users.user_id')
            ->where('tbl_users.user_type','=',$user_type)
            ->where('tbl_user_info.email','=',$email)
            ->orWhere('tbl_users.user_type','=',$user_type)
            ->where('tbl_user_info.mobile_number','=',$mobile)
            ->get();
        return $userExists;
    }

    protected function getProviderServiceAreas($provider_user_id){
        $serviceAreas = DB::table('tbl_provider_service_areas')
            ->leftJoin('tbl_provider','tbl_provider.provider_id', '=', 'tbl_provider_service_areas.provider_id')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_provider_service_areas.district_code_id')
            ->leftJoin('tbl_uktowns','tbl_district_code.town_id','=','tbl_uktowns.town_id')
            ->select('tbl_uktowns.town_id','tbl_uktowns.town_name')
            ->distinct()
            ->where('tbl_provider.user_id', '=', $provider_user_id)
            ->get();
        return $serviceAreas;
    }

    protected function getProviderdistrictAreas($provider_user_id){
        $serviceAreas = DB::table('tbl_provider_service_areas')
            ->leftJoin('tbl_provider','tbl_provider.provider_id', '=', 'tbl_provider_service_areas.provider_id')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_provider_service_areas.district_code_id')
            ->select('tbl_district_code.district_id')
            ->distinct()
            ->where('tbl_provider.user_id', '=', $provider_user_id)
            ->get();
        return $serviceAreas;
    }

    protected function getDriverServiceAreas($driver_id){
        $serviceAreas = DB::table('tbl_driver_service_areas')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_driver_service_areas.district_code_id')
            ->leftJoin('tbl_uktowns','tbl_district_code.town_id','=','tbl_uktowns.town_id')
            ->select('tbl_uktowns.town_id','tbl_uktowns.town_name')
            ->distinct()
            ->where('tbl_driver_service_areas.driver_id', '=', $driver_id)
            ->get();
        return $serviceAreas;
    }

    protected function getDriverUnAssignServiceAreas($driver_id){
        $serviceAreas = DB::table('tbl_provider_service_areas')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_provider_service_areas.district_code_id')
            ->leftJoin('tbl_uktowns','tbl_district_code.town_id','=','tbl_uktowns.town_id')
            ->join('tbl_driver',function($leftJoin) use($driver_id) {
                $leftJoin->on('tbl_driver.provider_id', '=', 'tbl_provider_service_areas.provider_id')
                    ->on('tbl_driver.driver_id', '=', DB::raw($driver_id));
            })
            ->leftJoin('tbl_driver_service_areas',function($leftJoin){
                $leftJoin->on('tbl_driver.driver_id','=','tbl_driver_service_areas.driver_id')
                    ->on('tbl_provider_service_areas.district_code_id','=','tbl_driver_service_areas.district_code_id');
            })
            ->select('tbl_uktowns.town_id','tbl_uktowns.town_name')
            ->distinct()
            ->whereNull('tbl_driver_service_areas.district_code_id')
            ->get();
        return $serviceAreas;
    }

    protected function getProviderUassignedServiceAreas($provider_id){
        $serviceAreas = DB::table('tbl_skip_rates')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_skip_rates.district_id')
            ->leftJoin('tbl_uktowns','tbl_district_code.town_id','=','tbl_uktowns.town_id')
            ->leftJoin('tbl_provider_service_areas',function($leftJoin) use($provider_id){
                $leftJoin->on('tbl_district_code.district_id', '=', 'tbl_provider_service_areas.district_code_id')
                    ->on('tbl_provider_service_areas.provider_id', '=', DB::raw($provider_id));
            })
            ->select('tbl_uktowns.town_id','tbl_uktowns.town_name')
            ->distinct()
            ->whereNull('tbl_provider_service_areas.provider_id')
            ->get();
        return $serviceAreas;
    }

    protected function getAllJobs($type, $manual){
        if($type == 'current'){
            $is_delete = 0;
            $appointment_status = 3;
            $operator = '!=';
            $cancel_status = 0;
            $operator2 = '=';
        }else if($type == 'completed'){
            $is_delete = 0;
            $appointment_status = 3;
            $operator = '=';
            $cancel_status = 0;
            $operator2 = '=';
        }else if($type == 'failed'){
            $is_delete = 1;
            $appointment_status = 3;
            $operator = '!=';
            $cancel_status = 0;
            $operator2 = '=';
        }else if($type == 'cancelled'){
            $is_delete = 0;
            $appointment_status = 3;
            $operator = '!=';
            $cancel_status = 1;
            $operator2 = '=';
        }
        $resultquery = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_job.service_id', '=', 'tbl_services.service_id')
            ->leftJoin('tbl_user_info As customerInfo', 'tbl_job.customer_user_id', '=', 'customerInfo.user_id')
            ->leftJoin('tbl_user_info As driverInfo', 'tbl_job.driver_user_id', '=', 'driverInfo.user_id')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->leftJoin('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0));
            })
            ->leftjoin('tbl_vehicals','tbl_driver_vehicle.vehicle_id','tbl_vehicals.vehicle_id','tbl_vehicals.status','=',0,'tbl_vehicals.is_delete','=',0)
            ->select('customerInfo.first_name As cust_first_name','customerInfo.last_name As cust_last_name','customerInfo.mobile_number As custmobile_number','driverInfo.first_name','driverInfo.last_name','driverInfo.mobile_number','tbl_services.service_id','tbl_services.service_name','tbl_job.job_id','tbl_job.job_location_lat','tbl_job.job_location_lng','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.job_address','tbl_job.job_date as date','tbl_job.status','tbl_job.is_schedule','tbl_job.is_permit','tbl_job.is_paid','tbl_job.skip_loc_type','tbl_appointments.appointment_id','tbl_appointments.appointment_date','tbl_appointments.cancel_status','tbl_appointments.save_date','tbl_appointments.appointment_status','tbl_appointments.heading_time','tbl_appointments.appoint_start_time','tbl_appointments.appoint_end_time','tbl_appointments.appointment_type','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('tbl_job.is_delete','=',0);
            if($manual == 1 && $type == 'current'){
                $resultquery = $resultquery->where('tbl_job.status','=',1);
            } elseif ($type == 'current'){
                $resultquery = $resultquery->where('tbl_job.status','=',0);
            }
        $resultquery = $resultquery->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',$is_delete)
            ->where('tbl_services.status','=',0)
            ->where('tbl_services.is_delete','=',0)
            ->where(function($q) use($appointment_status, $cancel_status, $operator, $operator2) {
                $q->where('tbl_appointments.appointment_status',$operator, $appointment_status)
                    ->Where('tbl_appointments.cancel_status',$operator2, $cancel_status);
            })
            ->orderBy('tbl_appointments.appointment_date', 'desc')
            ->get();
        return $resultquery;
    }
    protected function getProviderListing($verified_status){
        $providerListing = DB::table('tbl_users')
            ->leftJoin('tbl_user_info', 'tbl_user_info.user_id', '=', 'tbl_users.user_id')
            ->leftJoin('tbl_provider', 'tbl_users.user_id', '=', 'tbl_provider.user_id')
            ->leftJoin('tbl_company', 'tbl_users.user_id', '=', 'tbl_company.provider_user_id')
            ->select('tbl_provider.provider_id','tbl_users.user_id','tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.email','tbl_user_info.mobile_number','tbl_user_info.dob','tbl_user_info.building','tbl_user_info.street','tbl_user_info.town','tbl_user_info.country','tbl_user_info.post_code','tbl_user_info.profile_pic',
                'tbl_company.Registration_number','tbl_company.Business_name','tbl_company.Registered_as','tbl_company.tier','tbl_company.Applicant_type','tbl_company.Registration_date','tbl_company.Address','tbl_company.waste_license_url','tbl_provider.vat_status')
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_company.status','=',0)
            ->where('tbl_company.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_users.user_type','=',2)
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.provider_verified','=',$verified_status)
            ->get();
        return $providerListing;
    }
    protected function getProviderDetail($provider_user_id){
        $providerDetail = DB::table('tbl_users')
            ->leftJoin('tbl_user_info', 'tbl_user_info.user_id', '=', 'tbl_users.user_id')
            ->leftJoin('tbl_provider', 'tbl_users.user_id', '=', 'tbl_provider.user_id')
            ->leftJoin('tbl_company', 'tbl_users.user_id', '=', 'tbl_company.provider_user_id')
            ->leftJoin('tbl_vat_detail', 'tbl_provider.provider_id', '=', 'tbl_vat_detail.provider_id')
            ->leftJoin('tbl_provider_bank_manual', 'tbl_provider.provider_id', '=', 'tbl_provider_bank_manual.provider_id')
            ->select('tbl_provider.vat_status','tbl_users.user_id','tbl_users.provider_verified','tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.email','tbl_user_info.mobile_number','tbl_user_info.dob','tbl_user_info.building','tbl_user_info.street','tbl_user_info.town','tbl_user_info.country','tbl_user_info.post_code','tbl_user_info.profile_pic',
                'tbl_company.Registration_number','tbl_company.Business_name','tbl_company.Registered_as','tbl_company.tier','tbl_company.Applicant_type','tbl_company.Registration_date','tbl_company.Address','tbl_company.waste_license_url',
                'tbl_vat_detail.valid','tbl_vat_detail.database','tbl_vat_detail.format_valid','tbl_vat_detail.query','tbl_vat_detail.country_code','tbl_vat_detail.vat_number','tbl_vat_detail.company_name','tbl_vat_detail.company_address',
                'tbl_provider_bank_manual.account_name','tbl_provider_bank_manual.account_number','tbl_provider_bank_manual.sort_code','tbl_provider_bank_manual.account_type')
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_company.status','=',0)
            ->where('tbl_company.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_users.user_type','=',2)
            ->where('tbl_users.user_id','=',$provider_user_id)
            ->get();
        return $providerDetail;
    }
    protected function getProviderEarnings($provider_id, $is_paid){
        $ProviderEarnings = DB::table('tbl_provider')
            ->leftJoin('tbl_driver', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->leftJoin('tbl_job', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->select('tbl_job.job_id','tbl_job.job_address','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.is_permit','tbl_job.is_paid','tbl_job.transaction_cost')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_job.is_paid','=',$is_paid)
            ->where('tbl_provider.provider_id','=',$provider_id)
            ->get();
        return $ProviderEarnings;
    }
    protected function getDiscountCoupon($user_id){
        $serviceAreas = DB::table('tbl_coupon')
            ->leftJoin('tbl_coupon_user','tbl_coupon_user.coupon_id','=','tbl_coupon.coupon_id')
            ->leftJoin('tbl_coupon_service','tbl_coupon_service.coupon_id','=','tbl_coupon.coupon_id')
            ->leftJoin('tbl_coupon_area','tbl_coupon_area.coupon_id','=','tbl_coupon.coupon_id')
            ->select('tbl_coupon.coupon_id','tbl_coupon.discount','tbl_coupon.max_discount','tbl_coupon.is_fixed','tbl_coupon.is_service','tbl_coupon.is_service_area')
            ->where('tbl_coupon_user.user_id', '=', $user_id)
            ->get();
        return $serviceAreas;
    }
    protected function getProviderTotalEarnings($provider_id, $type){
        $ProviderEarnings = DB::table('tbl_provider')
            ->leftJoin('tbl_driver', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->leftJoin('tbl_job', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0);
            if($type == 'total'){
                $ProviderEarnings = $ProviderEarnings->whereRaw('(tbl_job.is_paid = 1 or tbl_job.is_paid = 3)');
            }
            if($type == 'pending'){
                $ProviderEarnings = $ProviderEarnings->where('tbl_job.is_paid','=',1);
            }
            if($type == 'processed'){
                $ProviderEarnings = $ProviderEarnings->where('tbl_job.is_paid','=',3);
            }
            $ProviderEarnings = $ProviderEarnings->where('tbl_provider.provider_id','=',$provider_id)
            ->get();
        return $ProviderEarnings;
    }
    protected function findProviderDrivers($service_id,$job_start_time,$job_end_time,$post_code,$provider_user_id){
        $resultquery = DB::table('tbl_driver_service_areas')
            ->leftJoin('tbl_district_code','tbl_district_code.district_id','=','tbl_driver_service_areas.district_code_id')
            ->leftJoin('tbl_driver','tbl_driver.driver_id','=','tbl_driver_service_areas.driver_id')
            ->leftJoin('tbl_users as users','tbl_driver.user_id','=','users.user_id')
            ->leftJoin('tbl_user_info','tbl_user_info.user_id','=','users.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_users as provider_user','provider_user.user_id','=','tbl_provider.user_id')
            ->leftJoin('tbl_driver_services', function($leftjoin){
                $leftjoin->on('tbl_driver_services.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_services.status','=',DB::raw(0))
                    ->on('tbl_driver_services.is_delete','=',DB::raw(0));
            })
            ->join('tbl_driver_vehicle',function($leftjoin){
                $leftjoin->on('tbl_driver_vehicle.driver_id', '=', 'tbl_driver.driver_id')
                    ->on('tbl_driver_vehicle.status','=',DB::raw(0))
                    ->on('tbl_driver_vehicle.is_delete','=',DB::raw(0));
            })
            ->leftJoin('tbl_vehicals','tbl_vehicals.vehicle_id' ,'=', 'tbl_driver_vehicle.vehicle_id')
            ->select('users.user_id','users.device_id','tbl_user_info.first_name','tbl_user_info.last_name','tbl_user_info.mobile_number','tbl_vehicals.reg_number','tbl_vehicals.vehicle_type')
            ->where('users.status','=',0)
            ->where('users.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_driver.online_status','=',1)
            ->where('users.device_id','!=',null)
            ->orwhere('users.ios_id','!=',null)
            ->where('tbl_driver_services.service_id','=',$service_id)
            ->where('tbl_district_code.district_code','=',$post_code)
            ->where('tbl_driver.online_status','=',1)
            ->where('provider_user.user_id','=',$provider_user_id)
            ->whereNotIN('users.user_id',function($query) use($job_start_time,$job_end_time){
                $query->from('tbl_users')
                    ->leftJoin('tbl_job', 'tbl_users.user_id', '=', 'tbl_job.driver_user_id')
                    ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
                    ->select('tbl_users.user_id')
                    /*->whereBetween('tbl_appointments.appointment_date', [$job_start_time, $job_end_time])
                    ->orWhereBetween(DB::raw('tbl_appointments.appointment_date+1800000'), [$job_start_time, $job_end_time])
                    */->where('tbl_users.user_id','=','users.user_id')
                    ->where('tbl_appointments.appointment_status','!=',3)
                    ->where('tbl_appointments.cancel_status','!=',1)
                    ->groupBy('tbl_users.user_id')
                    ->get();
            })
            ->groupBy('users.user_id')
            ->get();
        return $resultquery;
    }
    public function cronJobQuery(){

    }
    protected function getMatchedTowns($towns_ids){
        $district_codes = DB::table('tbl_district_code')
            ->Join('tbl_skip_rates','tbl_skip_rates.district_id','=','tbl_district_code.district_id')
            ->Join('tbl_uktowns','tbl_district_code.town_id','=','tbl_uktowns.town_id')
            ->select('tbl_uktowns.town_id','tbl_uktowns.town_name')
            ->distinct()
            ->get();
        return $district_codes;
    }
}