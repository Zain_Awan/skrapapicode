<?php

namespace App\Http\Controllers;

use App\driverTbl;
use App\postCodeTbl;
use App\providerTbl;
use Illuminate\Http\Request;
use App\rawQuery;
use App\servicesTbl;
use App\providerServicesTbl;
use App\driverServicesTbl;
use App\driverServicesAreasTbl;
use App\providerServiceAreasTbl;
use App\disputesTbl;
use App\couponTbl;
use App\couponUserTbl;
use App\userInfoTbl;
use App\skipRatesTbl;
use Mail;

class ServicesMethods extends Controller
{
///////////////////// constructor Method to include other controllers ///////////////////////////////////
    protected $HelperMethods;
    public function __construct(HelperMethods $HelperMethods)
    {
        $this->HelperMethods = $HelperMethods;
    }
///////////////////////////////// GET SERVICES LIST SERVICE METHOD ////////////////////////////
    public function getServices($request){
        $service_type = $request->service_type;
        // selecting all services from serviceTbl model
        $res = servicesTbl::selection($service_type);
        if(count($res)>0){
            $response = $this->HelperMethods->getArrayResponse(0,'Services found successfuly',$res);
            // return response of the request
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(1,'Error finding services','');
            // return response of the request
            return $response;
        }
    }
///////////////////////////////// GET PROVIDER SERVICES LIST SERVICE METHOD ////////////////////////////
    public function getProviderServices($request){
        ///////////////////////////////// Request Parameters ////////////////////////////
        $provider_user_id = $request->provider_user_id;
        ///////////////////////////////// calling get Provider Services Method ////////////////////////////
        $providerServices = rawQuery::getProviderServices($provider_user_id);
        ///////////////////////////////// if response is not empty ////////////////////////////
        if(count($providerServices)>0){
            ///////////////////////////////// Return Response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'services found successfully',$providerServices);
            return $response;
        }else{
            ///////////////////////////////// Return Response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Error finding services','');
            return $response;
        }
    }
    ///////////////////////////////// GET PROVIDER SERVICES LIST SERVICE METHOD ////////////////////////////
    public function getProviderUnassignedServices($request){
        ///////////////////////////////// Request Parameters ////////////////////////////
        $provider_user_id = $request->provider_user_id;
        ///////////////////////////////// calling get Provider Services Method ////////////////////////////
        $providerServices = rawQuery::getProviderUnassignedServices($provider_user_id);
        ///////////////////////////////// if response is not empty ////////////////////////////
        if(count($providerServices)>0){
            ///////////////////////////////// Return Response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'services found successfully',$providerServices);
            return $response;
        }else{
            ///////////////////////////////// Return Response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Error finding services','');
            return $response;
        }
    }
///////////////////////////////// GET DRIVER UNASSIGNED SERVICES LIST SERVICE METHOD ////////////////////////////
    public function getDriverUnAssignedServices($request){
        ///////////////////////////////// Request Parameters ////////////////////////////
        $driver_user_id = $request->user_id;
        $provider_user_id = $request->provider_user_id;
        ///////////////////////////////// calling rawQuery MOdel Method to get valus from DB ////////////////////////////
        $DriverUnAssignedServices = rawQuery::getDriverUnAssignedServices($driver_user_id,$provider_user_id);
        ///////////////////////////////// if response is not empty ////////////////////////////
        if(count($DriverUnAssignedServices)>0){
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'services found successfully',$DriverUnAssignedServices);
            return $response;
        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Error finding services','');
            return $response;
        }
    }
///////////////////////////////// ADD NEW DRIVER SERVICE METHOD ////////////////////////////
    public function AddDriverService($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $service_id = $request->service_id;
        $driver_id = $request->driver_id;
        ///////////////////////////////// creating array of the request ////////////////////////////
        $data = array('driver_id'=>$driver_id,'service_id'=>$service_id);
        ///////////////////////////////// Inserting data into DB using driver Services Model ////////////////////////////
        $res = driverServicesTbl::insertion($data);
        ///////////////////////////////// IF data inserted successfully ////////////////////////////
        if($res == 1){
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Service added successfully','');
            return $response;
        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error adding service','');
            return $response;
        }
    }

///////////////////////////////// ADD NEW PROVIDER SERVICE METHOD ////////////////////////////
    public function AddProviderService($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $service_id = $request->service_id;
        $provider_user_id = $request->provider_id;
        ///////////////////////////////// creating array of the request ////////////////////////////
        $provider = providerTbl::getProviderId($provider_user_id);
        $provider_id = $provider[0]->provider_id;
        $data = array('provider_id'=>$provider_id,'service_id'=>$service_id);
        ///////////////////////////////// Inserting data into DB using driver Services Model ////////////////////////////
        $res = providerServicesTbl::insertion($data);
        ///////////////////////////////// IF data inserted successfully ////////////////////////////
        if($res == 1){
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Service added successfully','');
            return $response;
        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error adding service','');
            return $response;
        }
    }

///////////////////////////////// ADD NEW PROVIDER SERVICE METHOD ////////////////////////////
    public function AddProviderServiceAreas($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $town_id = $request->town_id;
        $provider_user_id = $request->provider_id;
        ///////////////////////////////// creating array of the request ////////////////////////////
        $provider = providerTbl::getProviderId($provider_user_id);
        $provider_id = $provider[0]->provider_id;
        $districtAreas = $this->getdistrictCodes($town_id);
        $Distarray = [];
        foreach ($districtAreas['result'] as $districs)
        {
            $districs->district_id;
            array_push($Distarray,$districs->district_id);
        }
        ///////////////////////////////// Inserting data into DB using driver Services Model ////////////////////////////
        $res = $this->HelperMethods->saveProviderServicesArea($Distarray,$provider_id);
        ///////////////////////////////// IF data inserted successfully ////////////////////////////
        if($res == 1){
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Service Area added successfully','');
            return $response;
        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error adding service Area','');
            return $response;
        }
    }
    public function AddDriverServiceAreas($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $town_id = $request->town_id;
        $driver_id = $request->driver_id;
        ///////////////////////////////// creating array of the request ////////////////////////////
        $districtAreas = $this->getdistrictCodes($town_id);
        ///////////////////////////////// Inserting data into DB using driver Services Model ////////////////////////////
        $res = $this->saveDriverServiceAreas($districtAreas['result'],$driver_id);
        return $res;
    }
    public function DeleteDriverServiceAreas($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $town_id = $request->town_id;
        $driver_id = $request->driver_id;
        ///////////////////////////////// creating array of the request ////////////////////////////
        $districtAreas = $this->getdistrictCodes($town_id);
        $Distarray = [];
        foreach ($districtAreas['result'] as $districs)
        {
            $districs->district_id;
            array_push($Distarray,$districs->district_id);
        }
        $res = driverServicesAreasTbl::deleteServiceAreasSingle($driver_id, $Distarray);
        ///////////////////////////////// Inserting data into DB using driver Services Model ////////////////////////////
        if($res == 1){
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Service Area Deleted successfully','');
            return $response;
        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error deleting service Area','');
            return $response;
        }
    }
    ///////////////////////////////// ADD NEW PROVIDER SERVICE METHOD ////////////////////////////
    public function getDriverServiceAreas($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $driver_id = $request->driver_id;
        ///////////////////////////////// creating array of the request ////////////////////////////
        $provider = rawQuery::getDriverServiceAreas($driver_id);
        ///////////////////////////////// IF data inserted successfully ////////////////////////////
        if(count($provider)>0){
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Service Area found successfully',$provider);
            return $response;
        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Error finding service Area',$provider);
            return $response;
        }
    }
    public function getDriverUnAssignServiceAreas($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $driver_id = $request->driver_id;
        ///////////////////////////////// creating array of the request ////////////////////////////
        $provider = rawQuery::getDriverUnAssignServiceAreas($driver_id);
        ///////////////////////////////// IF data inserted successfully ////////////////////////////
        if(count($provider)>0){
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Service Area found successfully',$provider);
            return $response;
        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Error finding service Area',$provider);
            return $response;
        }
    }
    ///////////////////////////////// ADD NEW PROVIDER SERVICE METHOD ////////////////////////////
    public function DeleteProviderServiceAreas($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $town_id = $request->town_id;
        $provider_user_id = $request->provider_id;
        ///////////////////////////////// creating array of the request ////////////////////////////
        $provider = providerTbl::getProviderId($provider_user_id);
        $provider_id = $provider[0]->provider_id;
        $districtAreas = $this->getdistrictCodes($town_id);
        $Distarray = [];
        foreach ($districtAreas['result'] as $districs)
        {
            $districs->district_id;
            array_push($Distarray,$districs->district_id);
        }
        ///////////////////////////////// Inserting data into DB using driver Services Model ////////////////////////////
        $res = providerServiceAreasTbl::deleteServiceArea($Distarray,$provider_id);
        ///////////////////////////////// IF data inserted successfully ////////////////////////////
        if($res == 1){
            $drivers = driverTbl::getProviderDriverIds($provider_id);
            $driverIds = [];
            foreach ($drivers as $driver)
            {
                array_push($driverIds,$driver->driver_id);
            }
            $resp = driverServicesAreasTbl::deleteServiceAreas($driverIds,$Distarray);
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Service Area deleted successfully','');
            return $response;
        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error deleting service Area','');
            return $response;
        }
    }
///////////////////////////////// DELETE DRIVER SERVICES SERVICE METHOD ////////////////////////////
    public function DeleteDriverService($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $service_id = $request->service_id;
        $driver_id = $request->driver_id;
        $data = array('is_delete'=>1);
        ///////////////////////////////// delete service using driverServicesTbl MOdel ////////////////////////////
        $res = driverServicesTbl::DeleteService($service_id, $driver_id, $data);
        ///////////////////////////////// if service deleted ////////////////////////////
        if($res == 1){
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Service deleted successfully','');
            return $response;
        }else{
    ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error deleting service','');
            return $response;
        }
    }

///////////////////////////////// DELETE DRIVER SERVICES SERVICE METHOD ////////////////////////////
    public function DeleteProviderService($request){
        ///////////////////////////////// Request parameters ////////////////////////////
        $service_id = $request->service_id;
        $provider_user_id = $request->provider_id;
        $provider = providerTbl::getProviderId($provider_user_id);
        $provider_id = $provider[0]->provider_id;
        $drivers = driverTbl::getProviderDriverIds($provider_id);
        $driverIds = [];
        foreach ($drivers as $driver)
        {
            array_push($driverIds,$driver->driver_id);
        }
        ///////////////////////////////// delete service using driverServicesTbl MOdel ////////////////////////////
        $res = providerServicesTbl::DeleteService($service_id, $provider_id);
        ///////////////////////////////// if service deleted ////////////////////////////
        if($res == 1){
            $resp = driverServicesTbl::deleteServices($driverIds,$service_id);
            if($resp > 0){
                ///////////////////////////////// Return response to service ////////////////////////////
                $response = $this->HelperMethods->getEmptyResponse(0,'Service deleted successfully','');
                return $response;
            }else{
                ///////////////////////////////// Return response to service ////////////////////////////
                $response = $this->HelperMethods->getEmptyResponse(2,'provider Service deleted but not driver','');
                return $response;
            }

        }else{
            ///////////////////////////////// Return response to service ////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error deleting service','');
            return $response;
        }
    }
///////////////////////////////// GET SERVICE RATES SERVICE METHOD ////////////////////////////
    public function getServiceRates($request){
        ///////////////////////////////// Request Parameters ////////////////////////////
        $town = $request->town_name;
        $post_code = $request->post_code;
        $service_id = $request->service_id;
        $service_type = $request->service_type;
        $user_id = $request->user_id;
        $service = 'service'.$service_id;
        ///////////////////////////////// requesting data using rawQuery Model Method ////////////////////////////
        $res = rawQuery::getServiceRates($town,$post_code,$service,$service_type);
        ///////////////////////////////// If response has data ////////////////////////////
        if(count($res)>0){
            $area_id = $res[0]->district_id;
            $coupon = $this->HelperMethods->findActiveCoupon($user_id, $service_id, $area_id);
            if(count($coupon)>0){
                if($coupon[0]->is_fixed == 0){
                    $service_rate = $res[0]->service_rate;
                    $discount = $coupon[0]->discount/100;
                    $discount = round($service_rate * $discount);
                    $afterDiscount = $service_rate - $discount;
                    $res[0]->discount_rate = $afterDiscount;
                    $res[0]->discount = $discount;
                    $res[0]->coupon_code = $coupon[0]->coupon_code;
                    $res[0]->coupon_id = $coupon[0]->coupon_user_id;
                    $res[0]->is_coupon = 1;
                }else{
                    $service_rate = $res[0]->service_rate;
                    $afterDiscount = $service_rate - $coupon[0]->discount;
                    $res[0]->discount_rate = $afterDiscount;
                    $res[0]->discount = $coupon[0]->discount;
                    $res[0]->coupon_code = $coupon[0]->coupon_code;
                    $res[0]->coupon_id = $coupon[0]->coupon_user_id;
                    $res[0]->is_coupon = 1;
                }
            }else{
                $res[0]->is_coupon = 0;
            }
            ///////////////////////////////// Return response of the request ////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0, 'service rate found successfully', $res[0]);
            return $response;
        }else{
            $cust_Info =  userInfoTbl::getcustomerEmailPhone($user_id);
            $mail = $_ENV['ADMINEMAIL'];
            Mail::send('emails.serviceRate', ['data' => $cust_Info[0],'post_code'=>$post_code], function ($message) use ($mail) {
                $message->from($_ENV['INFOEMAIL'], 'Skrap');
                $message->to($mail)->subject('Service rate not found');
            });
            ///////////////////////////////// Return response of the request ////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(1, 'Unfortunately we are currently unable to accept your order via the app, to book your order please call 0330 133 1561', '');
            return $response;
        }
    }
///////////////////////////////// GET SKIP PERMIT RATE SERVICE METHOD ////////////////////////////
    public function getSkipPermitRate($request){
        ///////////////////////////////// Request Parameters ////////////////////////////
        $town = $request->town_name;
        $post_code = $request->post_code;
        ///////////////////////////////// Requesting data using rawQuery Model Method ////////////////////////////
        $res = rawQuery::getSkipPermitRate($town,$post_code);
        ///////////////////////////////// if esponse has data ////////////////////////////
        if(count($res)>0){
            ///////////////////////////////// Return response of the request ////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0, 'skip permit rate found successfully', $res);
            return $response;
        }else{
            ///////////////////////////////// Return response of the request ////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(1, 'Skip permit rate not found', '');
            return $response;
        }
    }

    public function getCities(){
        $res = rawQuery::getCities();
        $response = $this->HelperMethods->getArrayResponse(0, 'cities found', $res);
        return $response;
    }
    public function getTowns($request){
        $cities = $request->cities;
        $res = rawQuery::getMatchedTowns($cities);
        $response = $this->HelperMethods->getArrayResponse(0, 'Towns found', $res);
        return $response;
    }
    public function getdistrictCodes($towns_ids){
        $res = rawQuery::getdistrictCodes($towns_ids);
        $response = $this->HelperMethods->getArrayResponse(0, 'Districts found', $res);
        return $response;
    }
    public function getProviderServiceAreas($provider_user_id){
        $res = rawQuery::getProviderServiceAreas($provider_user_id);
        if(count($res)>0){
            $response = $this->HelperMethods->getArrayResponse(0, 'Districts found', $res);
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(1, 'Error finding districts','');
            return $response;
        }
    }
    public function getProviderdistrictAreas($provider_user_id){
        $res = rawQuery::getProviderdistrictAreas($provider_user_id);
        if(count($res)>0){
            $response = $this->HelperMethods->getArrayResponse(0, 'Districts found', $res);
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(1, 'Error finding districts','');
            return $response;
        }
    }
    public function getProviderUnassignedServiceAreas($provider_user_id){
        $provider = providerTbl::getProviderId($provider_user_id);
        $provider_id = $provider[0]->provider_id;
        $res = rawQuery::getProviderUassignedServiceAreas($provider_id);
        if(count($res)>0){
            $response = $this->HelperMethods->getArrayResponse(0, 'Districts found', $res);
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(1, 'Error finding districts','');
            return $response;
        }
    }
    public function saveDriverServiceAreas($ServiceAreas,$driver_id){
        $bulkCodes = array();
        foreach($ServiceAreas as $districtAreas){
            $data = array('driver_id'=>$driver_id,'district_code_id'=>$districtAreas->district_id);
            array_push($bulkCodes,$data);
        }
        $res = driverServicesAreasTbl::insertion($bulkCodes);
        if($res == 1){
            $response = $this->HelperMethods->getEmptyResponse(0, 'Service Areas Inserted Successfully','');
            return $response;
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1, 'Error Inserting Service Areas','');
            return $response;
        }
    }
    public function reportAnIssue($request){
        $raised_by = $request->raised_by_id;
        $description = $request->description;
        $appointment_id = $request->appointment_id;

        $dispute = array('appointment_id'=>$appointment_id,'description'=>$description,'raised_by_id'=>$raised_by);

        $resp = disputesTbl::insertion($dispute);
        if($resp == 1){
            $response = $this->HelperMethods->getEmptyResponse(0, 'Issue reported successfully','');
            return $response;
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1, 'Error reporting issue','');
            return $response;
        }
    }
    public function AddServiceRates($request){
        $postCodes = $request->post_codes;
        $serviceRates = $request->service_rates;
        $insertArr = [];
        foreach ($postCodes as $postCode){
            $serviceRates['district_id'] = $postCode['district_id'];
            $serviceRates['district_code'] = $postCode['district_code'];
            array_push($insertArr, $serviceRates);
        }
        $res = skipRatesTbl::insertRates($insertArr);
        if($res == 1){
            $ids = array();
            for($itr = 6; $itr<26; $itr++){
                array_push($ids,$itr);
            }
            $service_names = servicesTbl::getServiceNames($ids);
            $title = 'Service Cost for some new service areas has been added';
            $mail = $_ENV['ADMINEMAIL'];
            Mail::send('emails.serviceRatesEmail', ['service_rates' => $insertArr,'service_names'=>$service_names,'title'=>$title, 'district_code'=>''], function ($message) use ($mail) {
                $message->from($_ENV['INFOEMAIL'], 'Skrap');
                $message->to($mail)->subject('Skrap Service Cost');
            });
            $response = $this->HelperMethods->getEmptyResponse(0,'Services rates saved successfully','');
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1,'Error cannot save service rates','');
        }
        return $response;
    }
    public function updateServiceRates($request){
        $serviceRateId = $request->service_rate_id;
        $serviceRates = $request->service_rates;
        $resp = skipRatesTbl::updateServiceRate($serviceRateId, $serviceRates);
        if($resp === 1){
            $ids = array();
            for($itr = 6; $itr<26; $itr++){
                array_push($ids,$itr);
            }
            $service_names = servicesTbl::getServiceNames($ids);
            $title = 'The service cost for the service area has been updated';
            $mail = $_ENV['ADMINEMAIL'];
            Mail::send('emails.serviceRatesEmail', ['service_rates' => [$serviceRates],'service_names'=>$service_names,'title'=>$title, 'district_code'=>''], function ($message) use ($mail) {
                $message->from($_ENV['INFOEMAIL'], 'Skrap');
                $message->to($mail)->subject('Skrap Service Cost');
            });
            $response = $this->HelperMethods->getEmptyResponse(0,'Service rates updated successfully','');
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1,'unable to update service rates','');
        }
        return $response;
    }
    public function deleteServiceRate($request){
        $serviceRateId = $request->service_rate_id;
        $district_code = $request->area_code;
        $resp = skipRatesTbl::deleteServiceRate($serviceRateId);
        if($resp === 1){
            $title = 'The service cost for the service area has been deleted';
            $mail = $_ENV['ADMINEMAIL'];
            Mail::send('emails.serviceRatesEmail', ['district_code'=>$district_code,'title'=>$title, 'service_names'=>'',], function ($message) use ($mail) {
                $message->from($_ENV['INFOEMAIL'], 'Skrap');
                $message->to($mail)->subject('Skrap Service Cost');
            });
            $response = $this->HelperMethods->getEmptyResponse(0,'Service rates deleted successfully','');
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1,'unable to delete service rates','');
        }
        return $response;
    }
    public function getAllServiceRates(){
        $resp = skipRatesTbl::getAll();
        if(count($resp)>0){
            $ids = array();
            for($itr = 6; $itr<26; $itr++){
                array_push($ids,$itr);
            }
            $service_names = servicesTbl::getServiceNames($ids);
            $resultArr = array('service_name'=>$service_names, 'service_rates'=>$resp);
            $response = $this->HelperMethods->getObjectResponse(0,'Services rates found successfully',$resultArr);
        }else{
            $response = $this->HelperMethods->getObjectResponse(1,'Cannot find service rates', '');
        }
        return $response;
    }
    public function getAllAreaCodes(){
        $resp = postCodeTbl::getAllPriceLess();
        if(count($resp)>0){
            $ids = array();
            for($itr = 6; $itr<26; $itr++){
                array_push($ids,$itr);
            }
            $service_names = servicesTbl::getServiceNames($ids);
            $resultArr = array('service_name'=>$service_names, 'area_codes'=>$resp);
            $response = $this->HelperMethods->getObjectResponse(0,'Area codes found successfully',$resultArr);
        } else {
            $response = $this->HelperMethods->getObjectResponse(1,'Cannot find Area codes', '');
        }
        return $response;
    }
}
