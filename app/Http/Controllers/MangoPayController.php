<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MangoPayController extends Controller
{
    private $mangopay;
    protected $HelperMethods;
    // MangoPay instantiating Api
    public function __construct(\MangoPay\MangoPayApi $mangopay, HelperMethods $HelperMethods) {
        $this->mangopay = $mangopay;
        $this->HelperMethods = $HelperMethods;
    }

    /////////////////////// CREATE MANGO PAY USER METHOD /////////////////////////

    public function createUser($userInfo){
        $street = $userInfo['street'];
        $building = $userInfo['building'];
        $address = $building." ".$street;
        $dob = strtotime($userInfo['dob']);
        try{
        //////////////////// creating mango pay Natural user object ////////////////////////
            $UserNatural = new \MangoPay\UserNatural();
            $UserNatural->Tag = "customer";
            $UserNatural->FirstName = $userInfo['first_name'];
            $UserNatural->LastName = $userInfo['last_name'];
            $UserNatural->Address = new \MangoPay\Address();
            $UserNatural->Address->AddressLine1 = $address;
            $UserNatural->Address->City = $userInfo['town'];
            $UserNatural->Address->PostalCode = $userInfo['post_code'];
            $UserNatural->Address->Country = $userInfo['country'];
            $UserNatural->Birthday = $dob;
            $UserNatural->Nationality = $userInfo['country'];
            $UserNatural->CountryOfResidence = $userInfo['country'];
            $UserNatural->Email = $userInfo['email'];
            //////////////////////// Send the request ///////////////////////////////////
            $userObj = $this->mangopay->Users->Create($UserNatural);
            if($userObj->Id != '') {
                return array('code'=>0,'description'=>'user created','result'=>$userObj);
            }
        }
        catch (\MangoPay\Libraries\ResponseException $e) {
            return array('code'=>$e->getCode(),'description'=>$e->getMessage(),'result'=>$e->GetErrorDetails());
        }
    }

////////////////////// Create Wallet ///////////////////////////

    public function CreateWallet($first_name,$mangoUserId){
        try {
            $Wallet = new \MangoPay\Wallet();
            $Wallet->Tag = $first_name . "'s Wallet";
            $Wallet->Owners = array($mangoUserId);
            $Wallet->Description = "Skrap App Wallet";
            $Wallet->Currency = "GBP";
            $WalletObj = $this->mangopay->Wallets->Create($Wallet);
            if($WalletObj->Id != '') {
                return array('code'=>0,'description'=>'wallet created','result'=>$WalletObj);
            }
        }catch (\MangoPay\Libraries\ResponseException $e){
            return array('code'=>$e->getCode(),'description'=>$e->getMessage(),'result'=>$e->GetErrorDetails());
        }
    }

    public function createCard($mangoCustId){
        ///////////////////////// creating mango pay card registration object ///////////////
        $cardRegister = new \MangoPay\CardRegistration();
        $cardRegister->UserId = $mangoCustId;
        $cardRegister->Currency = "GBP";
        //////////////////// creating card /////////////////////////////
        $result = $this->mangopay->CardRegistrations->Create($cardRegister);
        return $result;
    }
    public function cardFinal($create_id){
        header('Access-Control-Allow-Origin: *');
        $cardRegisterPut = $this->mangopay->CardRegistrations->Get($create_id);
        $cardRegisterPut->RegistrationData = isset($_GET['data']) ? 'data=' . $_GET['data'] : 'errorCode=' . $_GET['errorCode'];
        $result = $this->mangopay->CardRegistrations->Update($cardRegisterPut);
        return json_encode($result);
    }

    public function cardPayIn($customer_id,$customer_wallet,$card_id,$amount){
        try{
            // MangoPay payin object
            $pay = new \MangoPay\PayIn();
            $pay->CreditedWalletId = $customer_wallet;
            $pay->AuthorId = $customer_id;
            $pay->CreditedUserId = $customer_id;
            $pay->PaymentType = "CARD";
            $pay->Type = "PAYIN";
            $pay->Nature = "REGULAR";
            $pay->ExecutionType = "DIRECT";
            $pay->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
            $pay->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
            $pay->PaymentDetails->CardId = $card_id;
            $pay->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
            $pay->ExecutionDetails->SecureModeNeeded = "FALSE";
            $pay->ExecutionDetails->SecureModeReturnURL = 'http://wasteapi.dev/scrapapi/home';
            $pay->DebitedFunds = new \MangoPay\Money();
            $pay->DebitedFunds->Currency = "GBP";
            $pay->DebitedFunds->Amount = $amount;
            $pay->CreditedFunds = new \MangoPay\Money();
            $pay->CreditedFunds->Currency = "GBP";
            $pay->CreditedFunds->Amount = $amount;
            $pay->Fees = new \MangoPay\Money();
            $pay->Fees->Currency = "GBP";
            $pay->Fees->Amount = 0;
            $createdPayIn = $this->mangopay->PayIns->Create($pay);
            return array('code'=>$createdPayIn->ResultCode,'description'=>$createdPayIn->Status,'result'=>$createdPayIn);
        }
        catch (\MangoPay\Libraries\ResponseException $e) {
            return array('code'=>$e->getCode(),'description'=>$e->getMessage(),'result'=>$e->GetErrorDetails());
        }
    }

    public function viewWallet($WalletId){
        try {
            $Wallet = $this->mangopay->Wallets->Get($WalletId);

            return array('code'=>0,'description'=>'wallet data found successfully','result'=>$Wallet);
        }catch (\MangoPay\Libraries\ResponseException $e){
            return array('code'=>$e->getCode(),'description'=>$e->getMessage(),'result'=>$e->GetErrorDetails());
        }
    }

    public function getBillingAddress($request){
        $user_id = $request->user_id;
        $res = userInfoTbl::getBillingAddress($user_id);
        if(count($res)>0){
            $response = $this->HelperMethods->getObjectResponse(0,'Billing address found successfully',$res[0]);
            return $response;
        }else{
            $response = $this->HelperMethods->getObjectResponse(1,'Billing address not found','');
            return $response;
        }
    }
    public function clientCardList($customer_id){
            $user = $this->mangopay->Users->Get($customer_id);
            $pagination = new \MangoPay\Pagination(1, 100);
            $Sorting = new \MangoPay\Sorting();
            $Sorting->AddField("CreationDate","DESC");
            $cards = $this->mangopay->Users->GetCards($user->Id, $pagination, $Sorting);
            $response = $this->HelperMethods->getArrayResponse(0,'Cards data found successfully',$cards);
            return $response;

    }
    public function LeagalUser(){

        $UserLegal = new \MangoPay\UserLegal();
        $UserLegal->Tag = "custom meta";
        $UserLegal->HeadquartersAddress = new \MangoPay\Address();
        $UserLegal->HeadquartersAddress->AddressLine1 = "1 Mangopay Street";
        $UserLegal->HeadquartersAddress->City = "Paris";
        $UserLegal->HeadquartersAddress->Region = "Ile de France";
        $UserLegal->HeadquartersAddress->PostalCode = "75001";
        $UserLegal->HeadquartersAddress->Country = "FR";
        $UserLegal->LegalPersonType = "BUSINESS";
        $UserLegal->Name = "Mangopay Ltd";
        $UserLegal->LegalRepresentativeAddress = new \MangoPay\Address();
        $UserLegal->LegalRepresentativeAddress->AddressLine1 = "1 Mangopay Street";
        $UserLegal->LegalRepresentativeAddress->City = "Paris";
        $UserLegal->LegalRepresentativeAddress->Region = "Ile de France";
        $UserLegal->LegalRepresentativeAddress->PostalCode = "75001";
        $UserLegal->LegalRepresentativeAddress->Country = "FR";
        $UserLegal->LegalRepresentativeBirthday = 1463496101;
        $UserLegal->LegalRepresentativeCountryOfResidence = "ES";
        $UserLegal->LegalRepresentativeNationality = "FR";
        $UserLegal->LegalRepresentativeFirstName = "Joe";
        $UserLegal->LegalRepresentativeLastName = "Blogs";
        $UserLegal->Email = "support@mangopay.com";

        $Result = $this->mangopay->Users->Create($UserLegal);

        print_r($Result);

    }
    public function testTrans(){
        $WalletId = 39867583;
        $pagination = new \MangoPay\Pagination(1, 2);
        $Wallet = $this->mangopay->Wallets->GetTransactions($WalletId,$pagination);
        print_r($Wallet);
        print_r($pagination);
    }
    public function Transfer($author,$credited_wallet,$debited_wallet,$amount){
        try {
            $Transfer = new \MangoPay\Transfer();
            $Transfer->AuthorId = $author;
            $Transfer->DebitedFunds = new \MangoPay\Money();
            $Transfer->DebitedFunds->Currency = "GBP";
            $Transfer->DebitedFunds->Amount = $amount;
            $Transfer->Fees = new \MangoPay\Money();
            $Transfer->Fees->Currency = "GBP";
            $Transfer->Fees->Amount = 0;
            $Transfer->DebitedWalletId = $debited_wallet;
            $Transfer->CreditedWalletId = $credited_wallet;
            $createdTransfer = $this->mangopay->Transfers->Create($Transfer);
            return array('code'=>$createdTransfer->ResultCode,'description'=>$createdTransfer->Status,'result'=>$createdTransfer);
        }
        catch (\MangoPay\Libraries\ResponseException $e) {
            return array('code'=>$e->getCode(),'description'=>$e->getMessage(),'result'=>$e->GetErrorDetails());
        }
    }
    public function PayInRefund($PayInId,$author,$amount)
    {
        try {
            $Refund = new \MangoPay\Refund();
            $Refund->Tag = "custom meta";
            $Refund->AuthorId = $author;
            $Refund->DebitedFunds = new \MangoPay\Money();
            $Refund->DebitedFunds->Currency = "GBP";
            $Refund->DebitedFunds->Amount = $amount;
            $Refund->Fees = new \MangoPay\Money();
            $Refund->Fees->Currency = "GBP";
            $Refund->Fees->Amount = 0;
            $reFundResult = $this->mangopay->PayIns->CreateRefund($PayInId, $Refund);
            return array('code' => $reFundResult->ResultCode, 'description' => $reFundResult->Status, 'result' => $reFundResult);
        } catch (\MangoPay\Libraries\ResponseException $e) {
            return array('code' => $e->getCode(), 'description' => $e->getMessage(), 'result' => $e->GetErrorDetails());
        }
    }
    public function deactivateCard($card_id)
    {
        try {

            $Card = new \MangoPay\Card();
            $Card->Id = $card_id;
            $Card->Active = false;

            $Result = $this->mangopay->Cards->Update($Card);
            return array('code' => 0, 'description' => 'successfull', 'result' => $Result);
        } catch (\MangoPay\Libraries\ResponseException $e) {
        // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return array('code' => $e->getCode(), 'description' => $e->getMessage(), 'result' => $e->GetErrorDetails());
        }
    }
    public function getCard($CardId){
        try {
            $Card =  $this->mangopay->Cards->Get($CardId);
            return array('code' => 0, 'description' => 'successfull', 'result' => $Card);
        } catch(\MangoPay\Libraries\ResponseException $e) {
// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return array('code' => $e->getCode(), 'description' => $e->getMessage(), 'result' => $e->GetErrorDetails());
        }
    }

    public function cardPayInWeb($customer_id,$customer_wallet,$card_id,$amount){
        try{
            // MangoPay payin object
            $pay = new \MangoPay\PayIn();
            $pay->CreditedWalletId = $customer_wallet;
            $pay->AuthorId = $customer_id;
            $pay->CreditedUserId = $customer_id;
            $pay->PaymentType = "CARD";
            $pay->Type = "PAYIN";
            $pay->Nature = "REGULAR";
            $pay->ExecutionType = "DIRECT";
            $pay->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
            $pay->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
            $pay->PaymentDetails->CardId = $card_id;
            $pay->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
            $pay->ExecutionDetails->SecureModeNeeded = "FALSE";
            $pay->ExecutionDetails->SecureModeReturnURL = 'http://wasteapi.dev/scrapapi/home';
            $pay->DebitedFunds = new \MangoPay\Money();
            $pay->DebitedFunds->Currency = "GBP";
            $pay->DebitedFunds->Amount = $amount;
            $pay->CreditedFunds = new \MangoPay\Money();
            $pay->CreditedFunds->Currency = "GBP";
            $pay->CreditedFunds->Amount = $amount;
            $pay->Fees = new \MangoPay\Money();
            $pay->Fees->Currency = "GBP";
            $pay->Fees->Amount = 0;
            $createdPayIn = $this->mangopay->PayIns->Create($pay);
            return array('code'=>$createdPayIn->ResultCode,'description'=>$createdPayIn->Status,'result'=>$createdPayIn);
        }
        catch (\MangoPay\Libraries\ResponseException $e) {
            return array('code'=>$e->getCode(),'description'=>$e->getMessage(),'result'=>$e->GetErrorDetails());
        }
    }
}
