<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Response\DownstreamResponse;
use LaravelFCM\Response\DownstreamResponseContract;
use FCM;

class FCMcontroller extends Controller
{
    /////////////////////////// FCM Notification Method to sen notification on Android devices ////////////////////
    public function FCMSendNotifications($deviceIds,$jobData,$title,$body,$type){
        ////////////////// calling OptionsBuilder function //////////////////
        $milliseconds = round(microtime(true) * 1000);
        $jobData['expiration_timestamp']=$milliseconds;
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);
        ////////////////// calling PayloadNotificationBuilder function //////////////////
        $notificationBuilder = new PayloadNotificationBuilder($title);
        ///////////////// calling setBody function //////////////////
        $notificationBuilder->setBody($body)
            ->setSound('default');
        ////////////////// calling PayloadDataBuilder function //////////////////
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['data'=>$jobData,'type'=>$type,'title'=>$title,'body'=>$body]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $deviceIds;

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

        $response = $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        $downstreamResponse;
        return $response;
        // return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }
    public function FCMSendNotificationsIos($deviceIds,$jobData,$title,$body,$type){
        ////////////////// calling OptionsBuilder function //////////////////
        $milliseconds = round(microtime(true) * 1000);
        $jobData['expiration_timestamp']=$milliseconds;
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);
        ////////////////// calling PayloadNotificationBuilder function //////////////////
        $notificationBuilder = new PayloadNotificationBuilder($title);
        ///////////////// calling setBody function //////////////////
        $notificationBuilder->setBody($body)
            ->setSound('default');
        ////////////////// calling PayloadDataBuilder function //////////////////
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['data'=>$jobData,'type'=>$type,'title'=>$title,'body'=>$body]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $deviceIds;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        $response = $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        $downstreamResponse;
        return $response;
        // return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }
    public function FCMtest(){
        $jobData = array("job_address"=>"celvas blue area islamabad",
            "job_id"=>40,
            "customer_id"=>4,
            "is_schedule"=>1,
            "job_end_time"=>1511289018390,
            "job_location_lat"=>12.972814,
            "job_location_lng"=>77.6204740000001,
            "job_start_time"=>1511289018390,
            "service_id"=>1,
            "service_name"=>"skip",
            "transaction_cost"=>100,
            "job_dates"=>[
                1511289018390,
                1511289018390,
                1511289018390,
                1511289018390,
                1511289018390,
                1511289018390,
                1511289018390
            ]);

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Job Request');
        ///////////////// calling setBody function //////////////////
        $notificationBuilder->setBody('You have recieved new job request')
            ->setSound('default');
        ////////////////// calling PayloadDataBuilder function //////////////////
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['data'=>$jobData,'type'=>1,'title'=>'Job Request','body'=>'You have recieved new job request','url'=>'http://localhost:4200']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = "e92_VJyP_UM:APA91bGIBS86KKIxwuQI-nJYaV17-OFL1FwlI-RlLO-G3sj0Nn0SECfJta2dbp5GJonKBtEpRk_8TYYVAArxhyRL4_oLsxbjWouslWwrLFyIcWYe5KtlUKmdRhUYKVfwBpVATPEAV1iH";

        $downstreamResponse = FCM::sendTo($token, $option, null, $data );

        //print_r($jobData);
        echo $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        $downstreamResponse;

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens


    }
    public function sendGenNotification($tokens){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Skrap app update');
        ///////////////// calling setBody function //////////////////
        $notificationBuilder->setBody('Please update your app or uninstall and install new one , we have implemented new data security mechanism, you have to update app to continue using skrap')
            ->setSound('default');
        ////////////////// calling PayloadDataBuilder function //////////////////
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['data'=>'data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, null );
        echo $downstreamResponse->numberSuccess();
    }
}
