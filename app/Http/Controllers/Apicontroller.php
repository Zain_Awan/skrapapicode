<?php

namespace App\Http\Controllers;

use App\rawQuery;
use Illuminate\Http\Request;
use Mail;
use File;
use Carbon\Carbon;
class Apicontroller extends Controller
{
    protected $HelperMethods;
    protected $UserMethods;
    protected $JobMethods;
    protected $ServicesMethods;
    protected $VehicleMethods;
    protected $PaymentController;
    protected $ReportsController;
    protected $cronJobController;
    public function __construct(HelperMethods $HelperMethods, UserMethods $UserMethods, JobMethods $JobMethods, ServicesMethods $ServicesMethods, VehicleMethods $VehicleMethods, PaymentController $PaymentController, ReportsController $ReportsController, cronJobController $cronJobController)
    {
     $this->HelperMethods = $HelperMethods;
     $this->UserMethods = $UserMethods;
     $this->JobMethods = $JobMethods;
     $this->ServicesMethods = $ServicesMethods;
     $this->VehicleMethods = $VehicleMethods;
     $this->PaymentController = $PaymentController;
     $this->ReportsController = $ReportsController;
     $this->cronJobController = $cronJobController;
    }

/////////////////////////////////////////// METHODS CALLED DIRECTLY BY SERVICES /////////////////////////////////////////

    public function home(){
    	$message = "welcome to byoot waste Api testing";
        //$password = base64_encode('VERUeVBJVUQ=');
       return $message;
    }

//////////////////////////////////////// PROVIDER REGISTRATION SERVICE METHOD ///////////////////////////////////////////////

    public function registerProvider(Request $request){
        $response = $this->UserMethods->registerProvider($request);
        return $response;
    }

//////////////////////////////////////// USER RATING SERVICE METHOD ///////////////////////////////////////////////

    public function giveUserRating(Request $request){
        $response = $this->UserMethods->giveUserRating($request);
        return $response;
    }



//////////////////////////////////// CUSTOMER REGISTRATION SERVICE METHOD /////////////////////////////////////////////

    public function registerCustomer(Request $request){
        $response =$this->UserMethods->registerCustomer($request);
        return $response;
    }
///////////////////////////////////////////////// LOGIN SERVICE METHOD ////////////////////////////////////////////////////////

    public function login(Request $request){
        $response = $this->UserMethods->login($request);
        return $response;
    }

///////////////////////////////////// CHANGE PASSWORD SERVICE METHOD ////////////////////////////////////////////////////////

    public function changePassword(Request $request){
        $response = $this->UserMethods->changePassword($request);
        return $response;
    }

///////////////////////////////////// FORGOT PASSWORD SERVICE METHOD ////////////////////////////////////////////////////////

    public function forgotPassword(Request $request){
        $response = $this->UserMethods->forgotPassword($request);
        return $response;
    }

///////////////////////////////////////////// GET SERVICES SERVICE METHOD ////////////////////////////////////////////////////////

    public function getServices(Request $request){
        $response = $this->ServicesMethods->getServices($request);
        return $response;
    }

/////////////////////////////////////////// JOB REQUEST SERVICE METHOD ///////////////////////////////////////////////////////////

    public function jobRequest(Request $request){
        $response = $this->JobMethods->jobRequest($request);
        return $response;
    }

//////////////////////// JOB ACCEPTED SERVICE METHOD ////////////////////////////

    public function jobAccepted(Request $request){
        $response = $this->JobMethods->jobAccepted($request);
        return $response;
    }

//////////////////////// GET JOB DETAIL SERVICE METHOD ////////////////////////////

    public function getJobDetail(Request $request){
        $response = $this->JobMethods->getJobDetail($request);
        return $response;
    }
//////////////////////// GET CURRENT JOB SERVICE METHOD ////////////////////////////

    public function getCurrentJob(Request $request){
        $response = $this->JobMethods->getCurrentJob($request);
        return $response;
    }

//////////////////////// GET CURRENT JOB LISTING SERVICE METHOD ////////////////////////////

    public function getCurrentJobListing(Request $request){
        $response = $this->JobMethods->getCurrentJobListing($request);
        return $response;
    }

//////////////////////// GET PENDING JOB LISTING SERVICE METHOD ////////////////////////////

    public function getPendingJobListing(Request $request){
        $response = $this->JobMethods->getPendingJobListing($request);
        return $response;
    }

//////////////////////// GET PREVIOUS JOB LISTING SERVICE METHOD ////////////////////////////

    public function getPreviousJobListing(Request $request){
        $response = $this->JobMethods->getPreviousJobListing($request);
        return $response;
    }

//////////////////////// GET APPOINTMENT SERVICE METHOD ////////////////////////////

    public function getAppointment(Request $request){
        $response = $this->JobMethods->getAppointment($request);
        return $response;
    }

//////////////////////// UPDATE DEVICE ID SERVICE METHOD ////////////////////////////

    public function updateDeviceId(Request $request){
        $response = $this->UserMethods->updateDeviceId($request);
        return $response;
    }

//////////////////////// GET DRIVER LISTING SERVICE METHOD ////////////////////////////

    public function getDrversListing(Request $request){
        $response = $this->UserMethods->getDrversListing($request);
        return $response;
    }

///////////////////////// GET DRIVER SERVICE METHOD ////////////////////////////

    public function getDriver(Request $request){
        $response = $this->UserMethods->getDriver($request);
        return $response;
    }

//////////////////////// GET VEHICLE LISTING SERVICE METHOD ////////////////////////////

    public function getVehiclesListing(Request $request){
        $response = $this->VehicleMethods->getVehiclesListing($request);
        return $response;
    }

//////////////////////// UPDATE DRIVER ONLINE STATUS SERVICE METHOD ////////////////////////////

    public function updateDriverOnlineStatus(Request $request){
        $response = $this->UserMethods->updateDriverOnlineStatus($request);
        return $response;
    }

//////////////////////// UPDATE JOB STATUS SERVICE METHOD ////////////////////////////

    public function updateJobStatus(Request $request){
        $response = $this->JobMethods->updateJobStatus($request);
        return $response;
    }

//////////////////////// CANCEL APPOINTMENT SERVICE METHOD ////////////////////////////

    public function cancelAppointment(Request $request){
        $response = $this->JobMethods->cancelAppointment($request);
        return $response;
    }

////////////////////// ADD DRIVER SERVICE METHOD ////////////////////////////////////

    public function addDriver(Request $request){
        $response = $this->UserMethods->addDriver($request);
        return $response;
    }
////////////////////// ADD VEHICLE SERVICE METHOD ////////////////////////////////////

    public function addVehicle(Request $request){
        $response = $this->VehicleMethods->addVehicle($request);
        return $response;
    }

////////////////////// ASSIGN VEHICLE TO DRIVER SERVICE METHOD ////////////////////////////////////

    public function assingDriverToVehicle(Request $request){
        $response = $this->VehicleMethods->assingDriverToVehicle($request);
        return $response;
    }

////////////////////// GET PROVIDER SERVICES SERVICE METHOD ////////////////////////////////////

    public function getProviderServices(Request $request){
        $response = $this->ServicesMethods->getProviderServices($request);
        return $response;
    }

////////////////////// GET PROVIDER SERVICES SERVICE METHOD ////////////////////////////////////

    public function getProviderAccountDetail(Request $request){
        $response = $this->PaymentController->getProviderAccountDetail($request);
        return $response;
    }

////////////////////// GET PROVIDER SERVICES SERVICE METHOD ////////////////////////////////////

    public function updateProviderAccountDetail(Request $request){
        $response = $this->PaymentController->updateProviderAccountDetail($request);
        return $response;
    }

////////////////////// GET PROVIDER SERVICES SERVICE METHOD ////////////////////////////////////

    public function getProviderUnassignedServices(Request $request){
        $response = $this->ServicesMethods->getProviderUnassignedServices($request);
        return $response;
    }

////////////////////// GET DRIVER UNASSIGNED SERVICES SERVICE METHOD ////////////////////////////////////

    public function getDriverUnAssignedServices(Request $request){
        $response = $this->ServicesMethods->getDriverUnAssignedServices($request);
        return $response;
    }

////////////////////// UN ASSIGN DRIVER VEHICLES SERVICE METHOD ////////////////////////////////////

    public function UnAssignDriverVehicle(Request $request){
        $response = $this->VehicleMethods->UnAssignDriverVehicle($request);
        return $response;
    }

////////////////////// GET PROVIDER UN ASSIGNED VEHICLES SERVICE METHOD ////////////////////////////////////

    public function getProviderUnAssignedVehicles(Request $request){
        $response = $this->VehicleMethods->getProviderUnAssignedVehicles($request);
        return $response;
    }
////////////////////// CHECK USER EMAIL SERVICE METHOD ////////////////////////////////////

    public function checkUserEmail(Request $request){
        $exists = $this->HelperMethods->userExists($request->email,$request->user_type);
        return $exists;
    }

////////////////////// CHECK USER Mobile Number SERVICE METHOD ////////////////////////////////////

    public function checkUserMobile(Request $request){
        $exists = $this->HelperMethods->userMobileExists($request->mobile,$request->user_type);
        return $exists;
    }

////////////////////// CHECK COMPANY NUMBER SERVICE METHOD ////////////////////////////////////

    public function checkCompNumber(Request $request){
        $exists = $this->HelperMethods->companyExists($request->CompNum);
        return $exists;
    }

////////////////////// CHECK VAT NUMBER SERVICE METHOD ////////////////////////////////////

    public function checkVatNumber(Request $request){
        $exists = $this->HelperMethods->vatExists($request->VatNum);
        return $exists;
    }

////////////////////// GET DRIVER ONLINE STATUS SERVICE METHOD ////////////////////////////////////

    public function getDriverOnlineStatus($user_id){
        $response = $this->UserMethods->getDriverOnlineStatus($user_id);
        return $response;
    }

////////////////////// CREATE MANGOPAY USER SERVICE METHOD ////////////////////////////////////

    public function createMangoUser(Request $request){
        $response = $this->PaymentController->createUser($request);
        return $response;
    }

    ////////////////////// CREATE MANGOPAY USER SERVICE METHOD ////////////////////////////////////

    public function createCompUserWallet(Request $request){
        $response = $this->PaymentController->createCompUserWallet($request);
        return $response;
    }

/////////////////////////// CREATE USER CARD SERVICE METHOD ////////////////////////////////////

    public function createCard(Request $request){
        $response = $this->PaymentController->createCustomerCardSource($request);
        return $response;
    }

/////////////////////////// GET USER CARD LIST SERVICE METHOD ////////////////////////////////////

    public function getCardList(Request $request){
        $response = $this->PaymentController->getSourceList($request->user_id);
        return $response;
    }

/////////////////////////// GET USER PROFILE SERVICE METHOD ////////////////////////////////////

    public function getUserProfile(Request $request){
        $response = $this->UserMethods->getUserProfile($request);
        return $response;
    }

////////////////////////////// UPDATE USER PROFILE SERVICE METHOD //////////////////////////////////

    public function updateUserProfile(Request $request){
        $response = $this->UserMethods->updateUserProfile($request);
        return $response;
    }

////////////////////////////// GET BILLING INFO SERVICE METHOD //////////////////////////////////

    public function getBillingAddress(Request $request){
        $response = $this->PaymentController->getBillingAddress($request);
        return $response;
    }

//////////////////////////////////////////// ADD DRIVER SERVICE /////////////////////////////////////////

    public function AddDriverService(Request $request){
        $response = $this->ServicesMethods->AddDriverService($request);
        return $response;
    }

//////////////////////////////////////////// ADD PROVIDER SERVICE /////////////////////////////////////////

    public function AddProviderService(Request $request){
        $response = $this->ServicesMethods->AddProviderService($request);
        return $response;
    }

//////////////////////////////////////////// ADD PROVIDER SERVICE /////////////////////////////////////////

    public function AddProviderServiceAreas(Request $request){
        $response = $this->ServicesMethods->AddProviderServiceAreas($request);
        return $response;
    }

//////////////////////////////////////////// ADD PROVIDER SERVICE /////////////////////////////////////////

    public function AddDriverServiceAreas(Request $request){
        $response = $this->ServicesMethods->AddDriverServiceAreas($request);
        return $response;
    }

//////////////////////////////////////////// ADD PROVIDER SERVICE /////////////////////////////////////////

    public function getDriverServiceAreas(Request $request){
        $response = $this->ServicesMethods->getDriverServiceAreas($request);
        return $response;
    }

//////////////////////////////////////////// ADD PROVIDER SERVICE /////////////////////////////////////////

    public function getDriverUnAssignServiceAreas(Request $request){
        $response = $this->ServicesMethods->getDriverUnAssignServiceAreas($request);
        return $response;
    }

//////////////////////////////////////////// ADD PROVIDER SERVICE /////////////////////////////////////////

    public function DeleteProviderServiceAreas(Request $request){
        $response = $this->ServicesMethods->DeleteProviderServiceAreas($request);
        return $response;
    }

//////////////////////////////////////////// ADD PROVIDER SERVICE /////////////////////////////////////////

    public function DeleteDriverServiceAreas(Request $request){
        $response = $this->ServicesMethods->DeleteDriverServiceAreas($request);
        return $response;
    }

//////////////////////////////////////////// ADD DRIVER JOB AREA SERVICE /////////////////////////////////////////

    public function addDriverJobArea(Request $request){
        $response = $this->UserMethods->addDriverJobArea($request);
        return $response;
    }

//////////////////////////////////////////// GET COMPANY INFO SERVICE /////////////////////////////////////////

    public function getCompnyInfo(Request $request){
        $response = $this->UserMethods->getCompnyInfo($request);
        return $response;
    }

////////////////////////////////// GET PROVIDER CURRENT JOB LISTING SERVICE /////////////////////////////////////

    public function getProviderJobListing(Request $request){
        $response = $this->JobMethods->getProviderJobListing($request);
        return $response;
    }

////////////////////////////////// GET PROVIDER CURRENT SINGLE JOB SERVICE /////////////////////////////////////

    public function getProviderJobDetail(Request $request){
        $response = $this->JobMethods->getProviderJobDetail($request);
        return $response;
    }

////////////////////////////////// GET SERVICE RATE SERVICE METHOD /////////////////////////////////////

    public function getServiceRates(Request $request)
    {
        $response = $this->ServicesMethods->getServiceRates($request);
        return $response;
    }

////////////////////////////////// GET PERMIT RATE SERVICE METHOD /////////////////////////////////////

    public function getSkipPermitRate(Request $request){
        $response = $this->ServicesMethods->getSkipPermitRate($request);
        return $response;
    }

////////////////////////////////// GET DRIVER INFO SERVICE METHOD /////////////////////////////////////

    public function updateDriverInfo(Request $request){
        $response = $this->UserMethods->updateDriverInfo($request);
        return $response;
    }

////////////////////////////////// GET VEHICLE DETAIL SERVICE METHOD /////////////////////////////////////

    public function getVehicle(Request $request){
        $response = $this->VehicleMethods->getVehicleDetail($request);
        return $response;
    }

////////////////////////////////// UPDATE VEHICLE DETAIL SERVICE METHOD /////////////////////////////////////

    public function updateVehicleInfo(Request $request){
        $response = $this->VehicleMethods->updateVehicleInfo($request);
        return $response;
    }
////////////////////////////////////////// DEACTIVATE USER CARDS SERVICE METHOD /////////////////////////////////////////////////////////

    public function deactivateCard(Request $request){
        $response = $this->PaymentController->deactivateCard($request->user_id, $request->source_id);
        return $response;
    }
////////////////////////////////////////// DEACTIVATE USER CARDS SERVICE METHOD /////////////////////////////////////////////////////////

    public function reportAnIssue(Request $request){
        $response = $this->ServicesMethods->reportAnIssue($request);
        return $response;
    }
////////////////////////////////////////// VIEW USER WALLET SERVICE METHOD /////////////////////////////////////////////////////////

    public function viewWallet(Request $request){
        $response = $this->PaymentController->viewWallet($request);
        return $response;
    }

////////////////////////////////////////// VIEW USER WALLET SERVICE METHOD /////////////////////////////////////////////////////////

    public function getNotificationList(Request $request){
        $response = $this->JobMethods->getNotificationList($request);
        return $response;
    }

////////////////////////////////////////// VIEW USER WALLET SERVICE METHOD /////////////////////////////////////////////////////////

    public function updateNotiReadStatus(Request $request){
        $response = $this->JobMethods->updateNotiReadStatus($request);
        return $response;
    }

////////////////////////////////////////// SET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////
    public function setDefaultCard(Request $request){
        $response = $this->PaymentController->setDefaultSource($request);
        return $response;
    }

////////////////////////////////////////// GET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////

    public function getDefaultCard(Request $request){
        $response = $this->PaymentController->getDefaultSource($request);
        return $response;
    }

////////////////////////////////////////// GET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////

    public function changeDriverJobReceiveStatus(Request $request){
        $response = $this->UserMethods->changeDriverJobReceiveStatus($request);
        return $response;
    }

////////////////////////////////////////// GET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////

    public function findProviderDrivers(Request $request){
        $response = $this->JobMethods->findProviderDrivers($request);
        return $response;
    }

////////////////////////////////////////// GET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////

    public function ActivateCoupon(Request $request){
        $response = $this->HelperMethods->ActivateCoupon($request);
        return $response;
    }

////////////////////////////////////////// GET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////

    public function getCoupons(Request $request){
        $response = $this->HelperMethods->getCoupons($request);
        return $response;
    }

////////////////////////////////////////// GET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////

    public function getProviderInstances(Request $request){
        $response = $this->UserMethods->getProviderInstances($request);
        return $response;
    }

////////////////////////////////////////// GET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////

    public function getProviderPaymentReport(Request $request){
        $response = $this->ReportsController->getProviderPaymentReport($request);
        return $response;
    }
////////////////////////////////////////// GET DEFAULT CARD SERVICE METHOD /////////////////////////////////////////////////////////

    public function cronJobs(){
        $response = $this->cronJobController->cronJobAppointments();
        return $response;
    }
///////////////////////////////////////////////// get REF COUPON SERVICE METHOD ////////////////////////////////////////////////////////

    public function getRefCoupon(Request $request){
        $response = $this->HelperMethods->getRefCoupon($request);
        return $response;
    }
////////////////////////////////////////// JOB FAILED CHECK /////////////////////////////////////////////////////////

    public function jobfailedCheck(Request $request){
        $response = $this->JobMethods->jobRequestEmail($request->customer_id,$request->job_id);
        return $response;
    }

////////////////////////////////////////// GET TIME SLOTS SERVICE METHOD /////////////////////////////////////////////////////////

    public function getTimeSlots(Request $request){
        $response = $this->JobMethods->getTimeSlots($request);
        return $response;
    }

    ////////////////////////////////////////// CANCEL JOB REQUEST /////////////////////////////////////////////////////////

    public function cancelJobRequest(Request $request){
        $response = $this->JobMethods->cancelJobRequest($request);
        return $response;
    }
    ////////////////////////////////////////// SKIP EXTENSION REQUEST /////////////////////////////////////////////////////////

    public function getExtentions(Request $request){
        $response = $this->JobMethods->getExtentions($request);
        return $response;
    }
    ////////////////////////////////////////// SKIP EXTENSION REQUEST /////////////////////////////////////////////////////////

    public function addExtention(Request $request){
        $response = $this->JobMethods->addExtention($request);
        return $response;
    }
    ////////////////////////////////////////// SKIP EXTENSION REQUEST /////////////////////////////////////////////////////////

    public function requestCollection(Request $request){
        $response = $this->JobMethods->requestCollection($request);
        return $response;
    }
//////////////////////////////////////////// DELETE SERVICES /////////////////////////////////////////

//////////////////////////////////////////// DELETE DRIVER SERVICE METHOD /////////////////////////////////////////

    public function DeleteDriverService(Request $request){
        $response = $this->ServicesMethods->DeleteDriverService($request);
        return $response;
    }
    public function DeleteProviderService(Request $request){
        $response = $this->ServicesMethods->DeleteProviderService($request);
        return $response;
    }
    public function checkOldPassword(Request $request){
        $response = $this->HelperMethods->checkOldPassword($request->user_id, base64_encode($request->old_password));
        return $response;
    }
    public function cities()
    {
       $response = $this->UserMethods->cities();
        return $response;
    }
    public function getCities(){
        $response = $this->ServicesMethods->getCities();
        return $response;
    }
    public function getTowns(Request $request){
        $response = $this->ServicesMethods->getTowns($request);
        return $response;
    }
    public function getdistrictCodes(Request $request){
        $response = $this->ServicesMethods->getdistrictCodes($request->town_ids);
        return $response;
    }
    public function getProviderServiceAreas(Request $request){
        $response = $this->ServicesMethods->getProviderServiceAreas($request->provider_id);
        return $response;
    }
    public function getProviderUnassignedServiceAreas(Request $request){
        $response = $this->ServicesMethods->getProviderUnassignedServiceAreas($request->provider_id);
        return $response;
    }
    public function emailTest(){
        $mail = 'zain@celvas.com.pk';
        $userRes = array('user_name'=>'hello','user_pwd'=>'hello');
        Mail::send('emails.welcome', ['data' => $userRes], function ($message) use ($mail) {
            $message->from($_ENV['INFOEMAIL'], 'Skrap');
            $message->to($mail)->subject('Skrap registration email');
        });
    }
    public function getHttpLogs()
    {
        $todays = date("d-m-Y");
        $logs = File::get(storage_path('logs/http'.$todays.'.log'));
        print_r($logs);
    }
    public function cronJob(){
        $response = $this->JobMethods->cronJob();
        return $response;
    }
    public function getMatchedDistrics(){
        $response = rawQuery::getMatchedDistrics();
        return $response;
    }
    public function createReferal(Request $request){
        return $referal = $this->HelperMethods->createReferalId($request->first_name);
    }
    public function AssignCoupon(Request $request){
        return $referal = $this->HelperMethods->AssignCoupon($request->user_id,1,null,null);
    }
    public function testcron(){
        Mail::send('emails.test',['data'=>'testCronJob'],function ($message){
            $message->from($_ENV['INFOEMAIL'], 'Skrap');
            $message->to('zain@celvas.com.pk')->subject('cronJob testing email');
        });
    }
    public function deleteUser(Request $request){
        $user_id = $request->user_id;
        $mobile_number = $request->mobile_number;
        $response = $this->UserMethods->DeleteUser($user_id,$mobile_number);
        return $response;
    }
    public function extraCharge(Request $request){
        $response = $this->PaymentController->extrachargeCustomer($request);
        return $response;
    }
    public function createCoupon(Request $request){
        $response = $this->HelperMethods->createCoupon($request);
        return $response;
    }
    public function getCouponList(){
        $response = $this->HelperMethods->getCouponList();
        return $response;
    }
    public function cancelJobCheck(Request $request){
        $response = $this->JobMethods->cancelJobCheck($request);
        return $response;
    }
    public function updateJobData(Request $request)
    {
        $response = $this->JobMethods->updateJobData($request);
        return $response;
    }
    public function sendGenNoti(){
        $response = $this->JobMethods->sendGenNotification();
        return $response;
    }
    public function chargeCustomer(Request $request){
        $response = $this->PaymentController->chargeCustomer($request);
        return $response;
    }
}
