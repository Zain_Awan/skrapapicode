<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function getToken($email,$password){
        if(Auth::attempt(['email' => $email, 'password' => $password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $resp = ['success' => $success];
            return $resp;
        }
        else{
            $resp = ['success' => 1];
            return $resp;
        }
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register($email,$password)
    {
        $input['email'] = $email;
        $input['c_password'] = $password;
        $input['name'] = 'skrapUser';
        $input['password'] = bcrypt($password);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        $resp = ['success' => $success];
        return $resp;
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this-> successStatus);
    }
}