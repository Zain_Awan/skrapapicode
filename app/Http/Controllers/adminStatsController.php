<?php

namespace App\Http\Controllers;

use App\adminTbl;
use Illuminate\Http\Request;
use App\userTbl;
use Carbon\Carbon;

class adminStatsController extends Controller
{
    protected $HelperMethods;
    public function __construct(HelperMethods $HelperMethods)
    {
        $this->HelperMethods = $HelperMethods;
    }

    public function getAdminInstances(){
        $usersCount = userTbl::getUsersCount();
        $providersCount = userTbl::getProvidersCount();
        $jobCounts = adminTbl::getJobsCount();
        $currentJobsCount = adminTbl::getCurrentJobsCount();
        $completedJobsCount = adminTbl::getCompletedJobsCount();
        $cancelledJobsCount = adminTbl::getCancelledJobsCount();
        $failedJobsCount = adminTbl::getFailedJobsCount();
        $totalEarnings = adminTbl::getTotalEarnings();
        $pendingEarnings = adminTbl::getPendingEarnings();
        $processedEarnings = adminTbl::getProcessedEarnings();
        $cancelledEarnings = adminTbl::getCancelledEarnings();
        $instances = array('usersCount'=>$usersCount,'providersCount'=>$providersCount,'jobCount'=>$jobCounts,
            'currentJobsCount'=>$currentJobsCount,'completedJobsCount'=>$completedJobsCount,'cancelledJobsCount'=>$cancelledJobsCount,
            'failedJobsCount'=>$failedJobsCount,'totalEarnings'=>$totalEarnings[0]->transactionCost,'pendingEarnings'=>$pendingEarnings[0]->transactionCost,
            'processedEarnings'=>$processedEarnings[0]->transactionCost, 'cancelledEarnings'=>$cancelledEarnings[0]->transactionCost);
        $response = $this->HelperMethods->getObjectResponse(0,'Admin Instance found Successfully',$instances);
        return $response;
    }
    public function getUsersChartData(){
        $current = Carbon::now()->format('Y-m-d');
        $oneMonth = Carbon::parse($current)->subDay(60)->format('Y-m-d');
        $resp = adminTbl::getOneMonthUsers($oneMonth);
        $dates = $resp->pluck('Date')->values()->toArray();
        $users = $resp->pluck('Users')->values()->toArray();
        $res = array('Dates'=>$dates,'Users'=>$users);
        $response = $this->HelperMethods->getObjectResponse(0,'Users chart data found Successfully',$res);
        return $response;
    }
    public function getJobsChartData(){
        $current = Carbon::now()->format('Y-m-d');
        $oneMonth = Carbon::parse($current)->subDay(30)->format('Y-m-d');
        $resp = adminTbl::getOneMonthJobs($oneMonth);
        $dates = $resp->pluck('Date')->values()->toArray();
        $jobs = $resp->pluck('Jobs')->values()->toArray();
        $res = array('Dates'=>$dates,'Jobs'=>$jobs);
        $response = $this->HelperMethods->getObjectResponse(0,'Job chart data found Successfully',$res);
        return $response;
    }
    public function getOneMonthEarningsData(){
        $current = Carbon::now()->format('Y-m-d');
        $oneMonth = Carbon::parse($current)->subDay(30)->format('Y-m-d');
        $resp = adminTbl::getOneMonthEarnings($oneMonth);
        $dates = $resp->pluck('Date')->values()->toArray();
        $jobs = $resp->pluck('Earnings')->values()->toArray();
        $res = array('Dates'=>$dates,'Earnings'=>$jobs);
        $response = $this->HelperMethods->getObjectResponse(0,'Earning chart data found Successfully',$res);
        return $response;
    }
}
