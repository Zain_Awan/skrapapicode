<?php

namespace App\Http\Controllers;

use App\jobImagesTbl;
use App\scheduleJobTbl;
use App\userTbl;
use Illuminate\Http\Request;
use App\rawQuery;
use App\appointmentsTbl;
use App\jobCancelTbl;
use App\jobRequestTbl;
use App\jobTbl;
use App\jobAndReqFailedTbl;
use App\notificationsTbl;
use App\couponUserTbl;
use App\timeSlotTbl;
use Carbon\Carbon;
use App\userInfoTbl;
use App\extentionTbl;
use Mockery\Exception;
use Mail;

class JobMethods extends Controller
{
    protected $HelperMethods;
    protected $FCMcontroller;
    protected $nodeApiController;
    protected $PaymentController;
    public function __construct(HelperMethods $HelperMethods, FCMcontroller $FCMcontroller, nodeApiController $nodeApiController, PaymentController $PaymentController)
    {
        $this->HelperMethods = $HelperMethods;
        $this->FCMcontroller = $FCMcontroller;
        $this->nodeApiController = $nodeApiController;
        $this->PaymentController = $PaymentController;
    }
    ///////////////////////// UPDATE JOB STATUS METHOD ////////////////////////////
    public function updateJobStatus($request){
        ///////////////////////// getting request data //////////////////////////////////////////////////////
        $appointment_id = $request->appointment_id;
        $job_status = $request->job_status;
        ////////////////////////// current time to milli's ///////////////////////////////////////////////
        $datetime = round(microtime(true) * 1000);
        /////////////////////////////// get device and user ids using rawQuery Model ///////////////////////////////
        $resultquery = rawQuery::getDeviceAndUserIds($appointment_id);
        $deviceid = $resultquery[0]->device_id;
        $Iosid = $resultquery[0]->ios_id;
        $driver_user_id = $resultquery[0]->driver_user_id;
        $customer_user_id = $resultquery[0]->customer_user_id;
        /////////////////// if job status in 1 /////////////////////////////////////////////
        if($job_status==1){
            $customer_title = 'Heading';
            $customer_body = 'Driver is heading towards job location';
            $status = array('appointment_status'=>$job_status,'heading_time'=>$datetime);
            ///////////// updating job status //////////////////////////////
            appointmentsTbl::updateAppointmentStatus($appointment_id,$status);
        }
        elseif($job_status==2){
            $customer_title = 'Ongoing';
            $customer_body = 'You job has been started please click to view';
            $status = array('appointment_status'=>$job_status,'appoint_start_time'=>$datetime);
            ///////////// updating job status //////////////////////////////
            appointmentsTbl::updateAppointmentStatus($appointment_id,$status);
        }
        elseif($job_status==3){
            $customer_title = 'Completed';
            $customer_body = 'Your job is completed successfully';
            $status = array('appointment_status'=>$job_status,'appoint_end_time'=>$datetime);
            ///////////// updating job status //////////////////////////////
            $resComplete = appointmentsTbl::updateAppointmentStatus($appointment_id,$status);
            if($resComplete == 1){
                $user_col = 'customer_user_id';
                $resultquery = rawQuery::getAppointments($user_col,$appointment_id);
                $array = json_decode(json_encode($resultquery[0]), True);
                $personal_detail = array_slice($array,0,5);
                $service_detail = array_slice($array,5,2);
                $rating_detail = array_slice($array,7,2);
                $job_detail = array_slice($array,8,23);
                $appointment_detail = array_slice($array,32,10);
                $vehicle_detail = array_slice($array,42,2);
                $mail = $personal_detail['email'];
                if($job_detail['is_paid'] == 0){
                    $user_pwd = userTbl::getPassword($customer_user_id);
                    $personal_detail['user_pwd'] = $user_pwd[0]->user_pwd;
                }
                Mail::send('emails.jobRecipt', ['data' => $personal_detail,'vehicle_detail'=>$vehicle_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'appointment_detail'=>$appointment_detail], function ($message) use ($mail) {
                    $message->from($_ENV['INFOEMAIL'], 'Skrap');
                    $message->to($mail)->subject('Skrap order receipt');
                });
            }
        }
        elseif($job_status==4){
            $customer_title = 'Delivered';
            $customer_body = 'Your required skip is delivered';
            $status = array('appointment_status'=>$job_status,'appoint_end_time'=>$datetime);
            ///////////// updating job status //////////////////////////////
            $resComplete = appointmentsTbl::updateAppointmentStatus($appointment_id,$status);
        }
        elseif($job_status==5){
            $customer_title = 'Pickup Heading';
            $customer_body = 'Driver is heading for skip pickup';
            $status = array('appointment_status'=>$job_status,'appoint_end_time'=>$datetime);
            ///////////// updating job status //////////////////////////////
            $resComplete = appointmentsTbl::updateAppointmentStatus($appointment_id,$status);
        }
        elseif($job_status==6){
            $customer_title = 'Pickup Ongoing';
            $customer_body = 'Skip pickup job has been started';
            $status = array('appointment_status'=>$job_status,'appoint_end_time'=>$datetime);
            ///////////// updating job status //////////////////////////////
            $resComplete = appointmentsTbl::updateAppointmentStatus($appointment_id,$status);
        }
        ////////////// check for device ids and job status ///////////////////////
        if($job_status != 0){
            $customer_data = array('appointment_id'=>$appointment_id,'driver_user_id'=>$driver_user_id);
            $type = 3;
            ///////////// sending notifications using FCM controller //////////////////////////////
            if($deviceid != null){
                $pushresp = $this->FCMcontroller->FCMSendNotifications($deviceid,$customer_data,$customer_title,$customer_body,$type);
            }else if($Iosid != null){
                $pushresp = $this->FCMcontroller->FCMSendNotificationsIos($Iosid,$customer_data,$customer_title,$customer_body,$type);
            }
            $notificationDetails = array('user_id'=>$customer_user_id,'notification_type'=>$type,'notification_data'=>json_encode($customer_data),'title'=>$customer_title,'body'=>$customer_body);
            /////////////// inserting notifications data into Db using notificationsTbl Model //////////////////
            notificationsTbl::insertion($notificationDetails);
            ///////////////////////// return response ///////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Job status updated successfully','');
            return $response;
        }else{
            ///////////////////////// return response ///////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(2,'Error updating job status','');
            return $response;
        }
    }
    ///////////////////////// JOB REQUEST METHOD ////////////////////////////
    public function jobRequest($request){
        // Request data
        $comments = $request->comments;
        $customer_id = $request->customer_user_id;
        $job_id = $request->job_id;
        $cardId = $request->card_id;
        $service_id = $request->service_id;
        $service_type = $request->service_type;
        $skip_location = $request->skip_loc_type;
        $job_area = $request->job_area;
        $skip_req_days = $request->skip_req_days;
        $is_permit = $request->is_permit;
        $service_name = $request->service_name;
        $job_location_lat = $request->job_location_lat;
        $job_location_lng = $request->job_location_lng;
        $destination_lat = $request->destination_lat;
        $destination_lng = $request->destination_lng;
        $job_start_time = $request->job_start_time;
        $job_end_time = $request->job_end_time;
        $status = $request->status;
        $is_coupon = $request->coupon_detail['is_coupon'];
        $junk_image_front = $request->junk_image_front;
        $junk_image_back = $request->junk_image_back;
        $junk_image_side = $request->junk_image_side;
        $service_rate = $request->coupon_detail['service_rate'];
        if($is_coupon == 1){
            $discount = $request->coupon_detail['discount'];
            $discount_rate = $request->coupon_detail['discount_rate'];
            $coupon_id = $request->coupon_detail['coupon_id'];
            $transaction_cost = $discount_rate;
        }else{
            $discount = 0;
            $discount_rate = 0;
            $coupon_id = null;
            $transaction_cost = $request->transaction_cost;
        }
        if($status == 1){
            $job_status = 1;
        }else{
            $job_status = 0;
        }
        $permit_cost = $request->permit_cost;
        $job_dates = $request->job_dates;
        $job_address = str_replace("،", "",$request->job_address);
        $is_schedule = $request->is_schedule;
        // creating array of the required data
        $job_data = array('customer_user_id'=>$customer_id,'service_id'=>$service_id,'job_location_lat'=>$job_location_lat,
            'job_location_lng'=>$job_location_lng,'destination_lat'=>$destination_lat,'destination_lng'=>$destination_lng,
            'job_start_time'=>$job_start_time,'job_end_time'=>$job_end_time,'job_address'=>$job_address,'transaction_cost'=>$transaction_cost,
            'is_schedule'=>$is_schedule,'skip_loc_type'=>$skip_location,'is_permit'=>$is_permit,'skip_req_days'=>$skip_req_days,
            'card_id'=>$cardId,'is_coupon'=>$is_coupon,'service_rate'=>$service_rate,'discount'=>$discount,'discount_rate'=>$discount_rate,
            'permit_cost'=>$permit_cost,'coupon_id'=>$coupon_id,'status'=>$job_status,'comments'=>$comments);
        // saving job in the Db using saveJob method
        if($job_id == ''){
            $data = $this->HelperMethods->saveJob($job_data,$job_dates,$service_type);
            $job_id = $data['job_id'];
            $result = array('job_id'=>$job_id);
        }
        if(!empty($job_id)){
            $this->HelperMethods->updateJob($job_data,$job_dates,$job_id,$service_type);
            $result = array('job_id'=>$job_id);
            $job_images_arr = array($junk_image_front,$junk_image_back,$junk_image_side);
            $imgResp = $this->uploadJobImages($job_id, $job_images_arr);
            $job_data['jobImages'] = $imgResp;
            ////////// calling raw query model to get device ids and driver checks ////////////////////
            $drivers = rawQuery::getDriversJobRequest($service_id,$job_start_time,$job_end_time,$job_area);
            $providers = rawQuery::getProvidersJobRequest($service_id,$job_start_time,$job_end_time,$job_area);
            $combiledArray = array_merge($providers->toArray(),$drivers->toArray());
            $resultquery = collect($combiledArray);
            $mobileTokens = $resultquery->pluck('device_id')->values()->toArray();
            $webTokents = $resultquery->pluck('web_token')->values()->toArray();
            $ios_ids = $resultquery->pluck('ios_id')->values()->toArray();
            $deviceIds = array_merge($mobileTokens,$webTokents);
            ////////////// creating notification body variables//////////////////
            $title = 'Job Request';
            $body = 'You have received new job request';
            $job_dates = array('job_dates'=>$job_dates);
            $service = ['service_name'=>$service_name];
            $jobId = ['job_id'=>$job_id];
            $job_data = array_merge($job_data,$service);
            $job_data = array_merge($job_data, $job_dates);
            $job_data = array_merge($job_data, $jobId);
            $job_data['job_area'] = $job_area;
            $type = 1;
            ///////////////// check if device ids are not empty ////////////////////
            if(!empty($deviceIds) || !empty($ios_ids)){
                //////////////////// send notifications by calling notification method of FCM controller /////////
                try{
                    $pushresp = $this->FCMcontroller->FCMSendNotifications($deviceIds,$job_data,$title,$body,$type);
                    $pushresp2 = $this->FCMcontroller->FCMSendNotificationsIos($ios_ids,$job_data,$title,$body,$type);
                }
                catch (Exception $e){

                }
                //////////////////// if response is not empty //////////////////
                if($pushresp>0 || $pushresp2>0){
                    foreach($resultquery as $querydata){
                        $user_id = $querydata->user_id;
                        $requestDetails = array('job_id'=>$job_id,'driver_user_id'=>$user_id);
                        //////////// inserting data in job request tbl by calling JobRequestTbl model /////////////////////
                        jobRequestTbl::insertion($requestDetails);
                        /////////// creating array of notification data /////////////////////////
                        $notificationDetails = array('user_id'=>$user_id,'notification_type'=>1,'notification_data'=>json_encode($job_data),'title'=>$title,'body'=>$body);
                        ////////////// inserting notification data in notifications tale by calling notificationTbl model //////////////////
                        notificationsTbl::insertion($notificationDetails);
                    }
                    ////////// return response of the request ////////////////////////
                    return $response = $this->HelperMethods->getObjectResponse(0,'Request Sent',$result);
                }else{
                    $this->jobRequestEmail($customer_id,0);
                    ////////// return response of the request ////////////////////////
                    return $response = $this->HelperMethods->getObjectResponse(1,'Unable to find drivers please try later',$result);
                }
            }else{
                $this->jobRequestEmail($customer_id,0);
                ////////// return response of the request ////////////////////////
                return $response = $this->HelperMethods->getObjectResponse(2,'Unable to find drivers please try later',$result);
            }
        }else{
            ////////// return response of the request ////////////////////////
            return $response = $this->HelperMethods->getObjectResponse(3,'cannot create job',$result);
        }
    }
    ///////////////////////// JOB ACCEPTED METHOD ////////////////////////////
    public function jobAccepted($request){
        //////////////////////////////////// request data //////////////////////////////////
        $driver_user_id = $request->driver_user_id;
        $job_id = $request->job_id;
        $provider_user_id = $request->user_id;
        $appointment_date = $request->appointment_date;
        ////////////////////// check if the job is already accepted by another driver //////////////////
        $checkAccepted = jobTbl::checkAccepted($job_id);
        /////////////////////////// if not accepted ///////////////////////////
        if($checkAccepted[0]->driver_user_id == null && $checkAccepted[0]->status != 2){
            /////////////// getting job data ///////////////////////
            $jobdata = jobTbl::getJobData($job_id);
            $card_id = $jobdata[0]->card_id;
            $amount = $jobdata[0]->transaction_cost;
            $cust_user_id = $jobdata[0]->customer_user_id;
            ////////////// getting device ids of the drivers /////////////////////
            $resultquery = rawQuery::getDeviceIdsDriver($job_id,$driver_user_id);
            ////////////// getting device id of customer ///////////////////////////
            $customer_device_id = rawQuery::getDeviceIdsCustomer($job_id);
            //////////////// created an array of driver_user_id //////////////////////////////
            $driver = array('driver_user_id'=>$driver_user_id);
            //////////////// update driver id jobTbl to assign driver to the job ///////////////////////
            jobTbl::updateDriveId($job_id,$driver);

            //////////////// selecting job request data against driver from jobRequestTbl ////////////////
            if($provider_user_id != '' || $provider_user_id != 0){
                $job_req_data = jobRequestTbl::selectSingle($job_id,$provider_user_id);
                $driver_assign_id = rawQuery::getDeviceIdsAssignDriver($driver_user_id);
            }else{
                $job_req_data = jobRequestTbl::selectSingle($job_id,$driver_user_id);
            }
            //////////////// check if data exists or not ////////////////////////////////////////////////
            if(count($job_req_data)>0) {
                ////////////////////////// selecting schedule job data from scheduleJobTbl model /////////////
                $scheduleJob = scheduleJobTbl::selectionById($job_id);
                if ($scheduleJob->count() > 0) {
                    $req_id = $job_req_data[0]->req_id;
                    $checkarray = array();
                    $skip_req_days = $jobdata[0]->skip_req_days;
                    $skip_loc_type = $jobdata[0]->skip_loc_type;
                    foreach ($scheduleJob as $scheduleDate) {
                        $job_date = $scheduleDate->schedule_date;
                        $job_type = $scheduleDate->job_type;
                        if ($job_type == 2) {
                            if((!empty($appointment_date)) && ($skip_loc_type == 0)){
                                $days = $skip_req_days * 86400000;
                                $skip_pickup_date = $appointment_date + $days;
                                $appointmentdata = array('job_id' => $job_id, 'job_req_id' => $req_id, 'appointment_date' => $appointment_date,'pickup_date'=>$skip_pickup_date, 'appointment_type' => $job_type);
                                $res = appointmentsTbl::insertion($appointmentdata);
                                if ($res == 1) {
                                    array_push($checkarray, $res);
                                }
                                else{
                                    appointmentsTbl::deleteAppointment($job_id);
                                    $failedData = array('job_id' => $job_id, 'job_req_id' => $req_id,'failed_reason'=>$res,'job_accept_failed'=>1,'comment'=>'appointment insertion failed');
                                    $failedResp = jobAndReqFailedTbl::insertion($failedData);
                                    return $response = $this->HelperMethods->getEmptyResponse(5,'Error job failed to accept','');
                                    exit;
                                }
                            } else {
                                $days = $skip_req_days * 86400000;
                                $skip_pickup_date = $job_date + $days;
                                $appointmentdata = array('job_id' => $job_id, 'job_req_id' => $req_id, 'appointment_date' => $job_date,'pickup_date'=>$skip_pickup_date, 'appointment_type' => $job_type);
                                $res = appointmentsTbl::insertion($appointmentdata);
                                if ($res == 1) {
                                    array_push($checkarray, $res);
                                }
                                else{
                                    appointmentsTbl::deleteAppointment($job_id);
                                    $failedData = array('job_id' => $job_id, 'job_req_id' => $req_id,'failed_reason'=>$res,'job_accept_failed'=>1,'comment'=>'appointment insertion failed');
                                    $failedResp = jobAndReqFailedTbl::insertion($failedData);
                                    return $response = $this->HelperMethods->getEmptyResponse(6,'Error job failed to accept','');
                                    exit;
                                }
                            }
                        } else {
                            //////////////// forloop for inserting multiple data ////////////////////////
                            $appointmentdata = array('job_id' => $job_id, 'job_req_id' => $req_id, 'appointment_date' => $job_date, 'appointment_type' => $job_type);
                            /////////////////// inserting appointments using appointmentsTbl  Model/////////////////////
                            $res = appointmentsTbl::insertion($appointmentdata);
                            /////////////// check if the data inserted correctly //////////////////
                            if ($res == 1) {
                                ////////////// pushing response in checkarray ////////////////////
                                array_push($checkarray, $res);
                            }
                            else{
                                appointmentsTbl::deleteAppointment($job_id);
                                $failedData = array('job_id' => $job_id, 'job_req_id' => $req_id,'failed_reason'=>$res,'job_accept_failed'=>1,'comment'=>'appointment insertion failed');
                                $failedResp = jobAndReqFailedTbl::insertion($failedData);
                                return $response = $this->HelperMethods->getEmptyResponse(7,'Error job failed to accept','');
                                exit;
                            }
                        }
                    }
                    if (count($checkarray) >= 1) {
                        ///////////////// setting body data for notification ///////////////////////////
                        $mobileTokens = $resultquery->pluck('device_id')->values()->toArray();
                        $webTokents = $resultquery->pluck('web_token')->values()->toArray();
                        $ios_ids = $resultquery->pluck('ios_id')->values()->toArray();
                        $deviceIds = array_merge($mobileTokens,$webTokents);
                        $driver_title = 'Job Request Accepted';
                        $driver_body = 'Another driver Accepted the Job';
                        $driver_data = array('job_id' => $job_id);
                        $deviceid = $customer_device_id[0]->device_id;
                        $deviceidIos = $customer_device_id[0]->ios_id;
                        $customer_user_id = $customer_device_id[0]->user_id;
                        $customer_title = 'Job Request Accepted';
                        $customer_body = 'Your Job Request is Accepted';
                        $customer_data = array('job_id' => $job_id);
                        $type = 2;
                        if($card_id == null || $card_id == ''){
                            $resCode = 0;
                        }else{
                            $payinRes = $this->PaymentController->createCharge($job_id,$card_id,$amount,$cust_user_id);
                            $resCode = $payinRes['code'];
                        }
                        if($resCode == 1 || $resCode == 2){
                            appointmentsTbl::deleteAppointment($job_id);
                            $failedData = array('job_id' => $job_id, 'job_req_id' => $req_id,'failed_reason'=>json_encode($payinRes),'job_accept_failed'=>1,'comment'=>'appointment insertion failed');
                            $failedResps = array('code'=>$payinRes['code'],'description'=>$payinRes['description'],);
                            $failedResp = jobAndReqFailedTbl::insertion($failedData);
                            return $response = $this->HelperMethods->getEmptyResponse(8,$payinRes['description'],'');
                            exit;
                        }
                        //////////////////// if device ids are greater then 1 ////////////////////////
                        if (count($deviceIds) > 1 || count($ios_ids) > 1) {
                            ////////////////////////// sending notifications to drivers using FCM controller method //////////////
                            $pushresp = $this->FCMcontroller->FCMSendNotifications($deviceIds, $driver_data, $driver_title, $driver_body, $type);
                            $pushresp2 = $this->FCMcontroller->FCMSendNotificationsIos($ios_ids, $driver_data, $driver_title, $driver_body, $type);
                        } else {
                            $pushresp = 1;
                        }
                        if($provider_user_id != '' || $provider_user_id != 0){
                            $driver_assign_token = $driver_assign_id->pluck('device_id')->values()->toArray();
                            $driver_assign_ios_token = $driver_assign_id->pluck('ios_id')->values()->toArray();
                            $driver_assign_title = 'Job Assigned';
                            $driver_assign_body = 'Your provider assigned you a new job';
                            $asssign_type = 5;
                        if($driver_assign_token[0] != null){
                            $this->FCMcontroller->FCMSendNotifications($driver_assign_token, $driver_data, $driver_assign_title, $driver_assign_body, $asssign_type);
                        }else if($driver_assign_ios_token[0] != null){
                            $this->FCMcontroller->FCMSendNotificationsIos($driver_assign_ios_token, $driver_data, $driver_assign_title, $driver_assign_body, $asssign_type);
                        }
                        }
                        ////////////////////////// sending notifications to customers using FCM controller method //////////////
                        try{
                            if($deviceid != null){
                                $customernotification = $this->FCMcontroller->FCMSendNotifications($deviceid, $customer_data, $customer_title, $customer_body, $type);
                            }else if($deviceidIos != null){
                                $customernotification = $this->FCMcontroller->FCMSendNotificationsIos($deviceidIos, $customer_data, $customer_title, $customer_body, $type);
                            }
                        }catch(Exception $exception ){

                        }
                        ///////////////// inserting notification details using notificationsTbl model /////////////////////
                        $notificationDetailscus = array('user_id' => $customer_user_id, 'notification_type' => 2, 'notification_data' => json_encode($customer_data), 'title' => $customer_title, 'body' => $customer_body);
                        notificationsTbl::insertion($notificationDetailscus);
                        if($provider_user_id != '' || $provider_user_id != 0){
                            if(count($driver_assign_token)>0 || count($driver_assign_ios_token>0)) {
                                $notificationAssgnDriver = array('user_id' => $driver_user_id, 'notification_type' => 5, 'notification_data' => json_encode($driver_data), 'title' => $driver_assign_title, 'body' => $driver_assign_body);
                                notificationsTbl::insertion($notificationAssgnDriver);
                            }
                        }
                        foreach ($resultquery as $driver_ids) {
                            $user_id = $driver_ids->user_id;
                            ///////////////// inserting notification details using notificationsTbl model /////////////////////
                            $notificationDetails = array('user_id' => $user_id, 'notification_type' => 2, 'notification_data' => json_encode($driver_data), 'title' => $driver_title, 'body' => $driver_body);
                            notificationsTbl::insertion($notificationDetails);
                        }
                        $coupon = $this->HelperMethods->CouponUses($cust_user_id);
                        $this->HelperMethods->referalCouponActivate($job_id);

                        $user_col = 'customer_user_id';
                        $resultquery = rawQuery::getAppointmentByJob($user_col,$job_id);
                        $array = json_decode(json_encode($resultquery[0]), True);
                        $personal_detail = array_slice($array,0,5);
                        $service_detail = array_slice($array,5,2);
                        $rating_detail = array_slice($array,7,2);
                        $job_detail = array_slice($array,9,22);
                        $appointment_detail = array_slice($array,31,10);
                        $vehicle_detail = array_slice($array,41,2);
                        $jobDetails = array('data' => $personal_detail,'vehicle_detail'=>$vehicle_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'appointment_detail'=>$appointment_detail);
                        $this->nodeApiController->sendJobSms($jobDetails);
                        $mail = $personal_detail['email'];
                        Mail::send('emails.jobRecipt', $jobDetails, function ($message) use ($mail) {
                            $message->from($_ENV['INFOEMAIL'], 'Skrap');
                            $message->to($mail)->subject('Skrap order receipt');
                        });
                        ////////////////////// Return Response ///////////////////////////////////////////
                        return $response = $this->HelperMethods->getObjectResponse(0, 'You have accepted job request, please view detail when to start job',array('appointment_id' => $appointment_detail['appointment_id'],'appointment_type'=>$scheduleJob[0]->job_type));
                    } else {
                        ////////////////////// Return Response ///////////////////////////////////////////
                        return $response = $this->HelperMethods->getObjectResponse(1, 'System Error, unable to assing job', array('appointment_type'=>$scheduleJob[0]->job_type));
                    }
                } else {
                    ////////////////////// Return Response ///////////////////////////////////////////
                    return $response = $this->HelperMethods->getEmptyResponse(2, 'System Error, schedule job data not found', '');
                }
            }else{
                ////////////////////// Return Response ///////////////////////////////////////////
                return $response = $this->HelperMethods->getEmptyResponse(3, 'System Error, job Request data not found', '');
            }
        }else{
            if($checkAccepted[0]->status == 2){
                ////////////////////// Return Response ///////////////////////////////////////////
                return $response = $this->HelperMethods->getEmptyResponse(4,'Job canceled by the customer','');
            }else{
                ////////////////////// Return Response ///////////////////////////////////////////
                return $response = $this->HelperMethods->getEmptyResponse(4,'Sorry you are late, job already accepted by another driver','');
            }
        }
    }
    ///////////////////////// GET JOB DETAIL METHOD ////////////////////////////
    public function getJobDetail($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $job_id = $request->job_id;
        $type = $request->type;
        /////////////////////////// if request type is 2 ///////////////////////////////////////
        if($type == 2) {
            $user_col = 'customer_user_id';
        }
        /////////////////////////// if request type is 1 ///////////////////////////////////////
        else if($type == 1) {
            $user_col = 'driver_user_id';
        }
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getJobDetailSingle($user_col,$job_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            $array = json_decode(json_encode($resultquery[0]), True);
            $personal_detail = array_slice($array,0,3);
            $service_detail = array_slice($array,3,2);
            $rating_detail = array_slice($array,5,1);
            $job_detail = array_slice($array,6,14);
            $jobImages = jobImagesTbl::getJobImages($resultquery[0]->job_id);
            if($type == 1){
                $vehicle_detail = array_slice($array,20,2);
            }
            /////////////// selecting single appointment //////////////////////
            $appointment = appointmentsTbl::selectSingle($job_id);
            if($type == 1){
                $data = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'rating_detail'=>$rating_detail,'job_detail'=>$job_detail,'vehicle_detail'=>$vehicle_detail,'appointments'=>$appointment,'jobImages'=>$jobImages);
            }else if($type == 2){
                $data = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'rating_detail'=>$rating_detail,'job_detail'=>$job_detail,'appointments'=>$appointment,'jobImages'=>$jobImages);
            }
            //////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0,'Job detail found successfully',$data);
            return $response;
        }else{
            //////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(1,'Job detail not found','');
            return $response;
        }
    }

    ///////////////////////// GET JOB DETAIL METHOD ////////////////////////////
    public function getProviderJobDetail($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $appointment_id = $request->appointment_id;
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getProviderJobDetailSingle($appointment_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            $array = json_decode(json_encode($resultquery[0]), True);
            $driver_detail = array_slice($array,0,4);
            $customer_detail = array_slice($array,4,4);
            $service_detail = array_slice($array,8,2);
            $driver_rating = array_slice($array,10,1);
            $customer_rating = array_slice($array,11,1);
            $job_detail = array_slice($array,12,15);
            $appointment = array_slice($array,27,11);
            $vehicle_detail = array_slice($array,38,3);
            $failed_detail = array_slice($array,41,8);
            $jobImages = jobImagesTbl::getJobImages($resultquery[0]->job_id);
            /////////////// selecting single appointment //////////////////////
            //$appointment = appointmentsTbl::selectSingle($job_id);
            $data = array('driver_detail'=>$driver_detail,'driver_rating'=>$driver_rating,'customer_detail'=>$customer_detail,'customer_rating'=>$customer_rating,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'vehicle_detail'=>$vehicle_detail,'appointments'=>$appointment, 'failed_detail'=>$failed_detail,'jobImages'=>$jobImages);
            //////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0,'Job detail found successfully',$data);
            return $response;
        }else{
            //////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(1,'Job detail not found','');
            return $response;
        }
    }

    ///////////////////////// GET CURRENT JOB METHOD ////////////////////////////
    public function getCurrentJob($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $user_id = $request->user_id;
        $type = $request->type;
        /////////////////////////// if request type is 2 ///////////////////////////////////////
        if($type == 2) {
            $user_col = 'customer_user_id';
            $driverCust = 'driver_user_id';
        }
        /////////////////////////// if request type is 1 ///////////////////////////////////////
        else if($type == 1) {
            $user_col = 'driver_user_id';
            $driverCust = 'customer_user_id';
        }
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getCurrentJobsingle($user_col,$driverCust,$user_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if(count($resultquery)>0){
            $finalIndex = 0;
            $index = 0;
            foreach($resultquery as $currentApp){
                if($currentApp->appointment_status == 2){
                    $finalIndex = $index;
                    break;
                }else if($currentApp->appointment_status == 1){
                    $finalIndex = $index;
                    break;
                }else if($currentApp->appointment_status == 0){
                    $finalIndex = $index;
                }
                $index++;
            }
            $array = json_decode(json_encode($resultquery[$finalIndex]), True);
            $personal_detail = array_slice($array,0,4);
            $service_detail = array_slice($array,4,2);
            $job_detail = array_slice($array,6,12);
            $appointment_detail = array_slice($array,18,10);
            if($type == 1){
                $vehicle_detail = array_slice($array,27,2);
                $data = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'vehicle_detail'=>$vehicle_detail,'appointments'=>$appointment_detail);
            }
            else if($type == 2){
                $data = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'appointments'=>$appointment_detail);
            }
            //////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0,'Job detail found successfully',$data);
            return $response;
        }else{
            //////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(1,'Job detail not found','');
            return $response;
        }
    }
    ///////////////////////// GET CURRENT JOB LISTINGS METHOD ////////////////////////////
    public function getCurrentJobListing($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $user_id = $request->user_id;
        $type = $request->type;
        /////////////////////////// if request type is 2 ///////////////////////////////////////
        if($type == 2) {
            $user_col = 'customer_user_id';
            $driverCust = 'driver_user_id';
        }
        /////////////////////////// if request type is 1 ///////////////////////////////////////
        else if($type == 1) {
            $user_col = 'driver_user_id';
            $driverCust = 'customer_user_id';
        }
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getCurrentJobListing($user_col,$driverCust,$user_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            $CurrentJobList = array();
            foreach($resultquery as $appointmentData){
                $array = json_decode(json_encode($appointmentData), True);
                $personal_detail = array_slice($array,0,3);
                $service_detail = array_slice($array,3,2);
                $job_detail = array_slice($array,5,11);
                $appointment_detail = array_slice($array,16,10);
                if($type == 1){
                    $vehicle_detail = array_slice($array,26,2);
                    $job_appointment = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'vehicle_detail'=>$vehicle_detail,'appointments'=>$appointment_detail);
                }else if($type == 2){
                    $job_appointment = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'appointments'=>$appointment_detail);
                }
                array_push($CurrentJobList,$job_appointment);
            }
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Job detail found successfully',$CurrentJobList);
            return $response;
        }else{
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Job detail not found','');
            return $response;
        }
    }
    ///////////////////////// GET PREVIOUS JOB LISTING METHOD ////////////////////////////
    public function getPreviousJobListing($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $user_id = $request->user_id;
        $type = $request->type;
        /////////////////////////// if request type is 2 ///////////////////////////////////////
        if($type == 2) {
            $user_col = 'customer_user_id';
            $driverCust = 'driver_user_id';
        }
        /////////////////////////// if request type is 1 ///////////////////////////////////////
        else if($type == 1) {
            $user_col = 'driver_user_id';
            $driverCust = 'customer_user_id';
        }
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getPreviousJobListing($user_col,$driverCust,$user_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            $PreviousJobList = array();
            foreach($resultquery as $appointmentData){
                $array = json_decode(json_encode($appointmentData), True);
                $personal_detail = array_slice($array,0,3);
                $service_detail = array_slice($array,3,2);
                $job_detail = array_slice($array,5,11);
                $appointment_detail = array_slice($array,16,10);
                if($type == 1){
                    $vehicle_detail = array_slice($array,26,2);
                    $job_appointment = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'vehicle_detail'=>$vehicle_detail,'appointments'=>$appointment_detail);
                }else if($type == 2){
                    $job_appointment = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'appointments'=>$appointment_detail);
                }
                array_push($PreviousJobList,$job_appointment);
            }
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Job detail found successfully',$PreviousJobList);
            return $response;
        }else{
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Job detail not found','');
            return $response;
        }
    }
///////////////////////// GET Pendding JOB LISTINGS METHOD ////////////////////////////
    public function getPendingJobListing($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $user_id = $request->user_id;
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getPendingJobListing($user_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            $CurrentJobList = array();
            foreach($resultquery as $appointmentData){
                $array = json_decode(json_encode($appointmentData), True);
                $personal_detail = array_slice($array,0,3);
                $service_detail = array_slice($array,3,2);
                $job_detail = array_slice($array,5,10);
                $job_appointment = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail);
                array_push($CurrentJobList,$job_appointment);
            }
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Job detail found successfully',$CurrentJobList);
            return $response;
        }else{
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Job detail not found','');
            return $response;
        }
    }
    ///////////////////////// GET PENDDING JOB LISTINGS ADMIN METHOD ////////////////////////////
    public function getAutoAcceptJobListing(){
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getAutoAcceptJobListing();
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            $CurrentJobList = array();
            foreach($resultquery as $appointmentData){
                $array = json_decode(json_encode($appointmentData), True);
                $personal_detail = array_slice($array,0,3);
                $service_detail = array_slice($array,3,2);
                $job_detail = array_slice($array,5,10);
                $job_appointment = array('personal_detail'=>$personal_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail);
                array_push($CurrentJobList,$job_appointment);
            }
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Job detail found successfully',$CurrentJobList);
            return $response;
        }else{
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Job detail not found','');
            return $response;
        }
    }
    ///////////////////////// GET APPOINTMENT METHOD ////////////////////////////
    public function getAppointment($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $appointment_id = $request->appointment_id;
        $type = $request->type;
        /////////////////////////// if request type is 2 ///////////////////////////////////////
        if($type == 2) {
            $user_col = 'customer_user_id';
        }
        /////////////////////////// if request type is 1 ///////////////////////////////////////
        else if($type == 1) {
            $user_col = 'driver_user_id';
        }
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getAppointments($user_col,$appointment_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if(count($resultquery)>0){
            $array = json_decode(json_encode($resultquery[0]), True);
            $personal_detail = array_slice($array,0,5);
            $service_detail = array_slice($array,5,2);
            $rating_detail = array_slice($array,7,2);
            $job_detail = array_slice($array,9,23);
            $appointment_detail = array_slice($array,32,10);
            $jobImages = jobImagesTbl::getJobImages($resultquery[0]->job_id);
            /////////////////////////// if request type is 1 ///////////////////////////////////////
            if($type == 1) {
                $vehicle_detail = array_slice($array, 42, 2);
                $data = array('personal_detail' => $personal_detail, 'vehicle_detail' => $vehicle_detail, 'service_detail' => $service_detail,'rating_detail'=>$rating_detail, 'job_detail' => $job_detail, 'appointments' => $appointment_detail, 'jobImages'=>$jobImages);
            }
            /////////////////////////// if request type is 2 ///////////////////////////////////////
            else if($type==2){
                $data = array('personal_detail' => $personal_detail,'service_detail' => $service_detail,'rating_detail'=>$rating_detail, 'job_detail' => $job_detail, 'appointments' => $appointment_detail,'jobImages'=>$jobImages);
            }
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0,'Job detail found successfully',$data);
            return $response;
        }else{
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Job detail not found','');
            return $response;
        }
    }

    public function cancelAppointment($request){
        /////////////////////// request parameters ///////////////////////////////////////////
        $appointment_id = $request->appointment_id;
        $user_id = $request->user_id;
        $cancel_reason = $request->cancel_reason;
        $type = $request->type;
        $is_fee = $request->is_fee;
        ///////////////////// if type is 1 /////////////////////////
        if($type==1){
            $user_type = 'tbl_job.driver_user_id';
        }
        /////////////// if type is 2 ///////////////////////////////
        else if($type==2){
            $user_type = 'tbl_job.customer_user_id';
        }
        ///////////////////// get device ids using rawQuery Model ///////////////////////////
        $resultquery = rawQuery::getDeviceIdsByAppId($appointment_id,$user_type);
        $deviceid =$resultquery[0]->device_id;
        $ios_id =$resultquery[0]->ios_id;
        $noti_user_id = $resultquery[0]->noti_user_id;
        $job_id= $resultquery[0]->job_id;
        ////////////////////////// setting notification params //////////////////////////////
        $title = 'Job canceled';
        $body = 'Your job has been canceled';
        $noti_data = array('appointment_id'=>$appointment_id);
        $type = 4;
        $data = array('cancel_status'=>1);
        /////////////////// updating Appointment cancel status ////////////////////////
        $res = appointmentsTbl::cancelAppointment($job_id,$data);
        /////////////////////////// checking response ///////////////////////////////////
        if($res > 0){
            $reFundResp = $this->PaymentController->refundCharge($appointment_id,$is_fee);
            $cancel_data = array('appointment_id'=>$appointment_id,'cancel_by_id'=>$user_id,'cancel_reason'=>$cancel_reason);
            ///////////////////// inserting data using JobCancelTbl Model ///////////////////
            $cancelRes = jobCancelTbl::insertion($cancel_data);
            if($cancelRes==1){
                $couponData = jobTbl::getjobCoupon($appointment_id);
                if(count($couponData)>0){
                    $coupon_user_id = $couponData[0]->coupon_user_id;
                    $uses = $couponData[0]->uses-1;
                    couponUserTbl::updateUses($coupon_user_id,$uses);
                }
                ///////////////////////////// sending notifications using FCMcontroller ////////////////////
                if($deviceid == null){
                    $pushresp = $this->FCMcontroller->FCMSendNotificationsIos($ios_id,$noti_data,$title,$body,$type);
                }else{
                    $pushresp = $this->FCMcontroller->FCMSendNotifications($deviceid,$noti_data,$title,$body,$type);
                }
                $notificationDetails = array('user_id'=>$noti_user_id,'notification_type'=>$type,'notification_data'=>json_encode($noti_data),'title'=>$title,'body'=>$body);
                //////////////////// inserting data using notificationsTbl model //////////////////////////
                notificationsTbl::insertion($notificationDetails);
                ////////////////// return Response ///////////////////////////
                $response = $this->HelperMethods->getEmptyResponse(0,'Appointment canceled saved successfully','');
                return $response;
            }else{
                /////////////////// return Response ///////////////////////////
                $response = $this->HelperMethods->getEmptyResponse(1,'Appointment canceled but not saved','');
                return $response;
            }
        }else{
            ////////////////// return Response ///////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(2,'Appointment is already canceled','');
            return $response;
        }
    }

    ///////////////////////// GET PROVIDER CURRENT JOB LISTINGS METHOD ////////////////////////////
    public function getProviderJobListing($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $user_id = $request->user_id;
        $job_status = $request->job_status;
        $driver_id = $request->driver_id;
        $job_type = $request->job_type;
        $cancel_status = $request->cancel_status;
        ////////////////////////// selecting job data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getProviderJobListing($user_id,$job_status,$driver_id,$job_type,$cancel_status);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            $CurrentJobList = array();
            foreach($resultquery as $appointmentData){
                $array = json_decode(json_encode($appointmentData), True);
                $driver_detail = array_slice($array,0,3);
                $customer_detail = array_slice($array,3,3);
                $service_detail = array_slice($array,6,2);
                $job_detail = array_slice($array,8,11);
                $appointment_detail = array_slice($array,19,9);
                $vehicle_detail = array_slice($array,28,3);
                $job_appointment = array('driver_detail'=>$driver_detail,'customer_detail'=>$customer_detail,'service_detail'=>$service_detail,'job_detail'=>$job_detail,'vehicle_detail'=>$vehicle_detail,'appointments'=>$appointment_detail);
                array_push($CurrentJobList,$job_appointment);
            }
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Job detail found successfully',$CurrentJobList);
            return $response;
        }else{
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Job detail not found','');
            return $response;
        }
    }

    ///////////////////// CREATE SKIP PICKUP JOB SERVICE METHOD //////////////////////////////////

    public function createSkipPickupJob($job_id,$req_id,$appointment_date,$skip_req_days){
        ///////////////////// Request Parameters //////////////////////////////////
        $days = $skip_req_days * 86400000;
        $skip_pickup_date = $appointment_date + $days;
        ///////////////////// array to insert data in table //////////////////////////////////
        $skip_pickUp_job = array('job_id' => $job_id, 'job_req_id' => $req_id, 'appointment_date' => $skip_pickup_date,'appointment_type'=>3);
        /////////////////////Inserting data in database using appointmentsTbl Model //////////////////////////////////
        $res = appointmentsTbl::insertion($skip_pickUp_job);
        if($res==1){
            ///////////////////// return response //////////////////////////////////
            $response = array('code'=>0,'description'=>'job created');
            return $response;
        }else{
            ///////////////////// return response //////////////////////////////////
            $response = array('code'=>1,'description'=>'error creating job');
            return $response;
        }
    }

    public function getAllJobs($request)
    {
        $type = $request->type;
        $manual = $request->manual;
        $resultquery = rawQuery::getAllJobs($type, $manual);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if (count($resultquery) > 0) {
            $CurrentJobList = array();
            foreach ($resultquery as $appointmentData) {
                $array = json_decode(json_encode($appointmentData), True);
                $customer_detail = array_slice($array, 0, 3);
                $driver_detail = array_slice($array, 3, 3);
                $service_detail = array_slice($array, 6, 2);
                $job_detail = array_slice($array, 8, 13);
                $appointment_detail = array_slice($array, 21, 9);
                $vehicle_detail = array_slice($array, 30, 2);
                /////////////////////////// if request type is 1 ///////////////////////////////////////
                $job_appointment = array('customer_detail' => $customer_detail, 'driver_detail' => $driver_detail, 'vehicle_detail' => $vehicle_detail, 'service_detail' => $service_detail,'job_detail' => $job_detail, 'appointments' => $appointment_detail);
                ///////////////////// return response //////////////////////////////////
                array_push($CurrentJobList, $job_appointment);
            }
            //$pagination = array('currentPage'=>$resultquery->currentPage(), 'firstItem'=>$resultquery->firstItem(),'hasMorePages'=>$resultquery->hasMorePages(),'lastItem'=>$resultquery->lastItem(),'lastPage'=>$resultquery->lastPage(),'nextPageUrl'=>$resultquery->nextPageUrl(),'onFirstPage'=>$resultquery->onFirstPage(),'perPage'=>$resultquery->perPage(),'previousPageUrl'=>$resultquery->previousPageUrl(),'total'=>$resultquery->total());
            $response = $this->HelperMethods->getArrayResponse(0, 'Job detail found successfully', $CurrentJobList);
            return $response;
        } else {
            ///////////////////// return response //////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1, 'Job detail not found', '');
            return $response;
        }
    }

    public function getNotificationList($request){
        $user_id = $request->user_id;
        $user_type = $request->user_type;
        if($user_type == 2){
            $notifications = notificationsTbl::selectByIdProvider($user_id);
        }else if($user_type == 1){
            $notifications = notificationsTbl::selectByIdCustomer();
        }
        else{
            $notifications = notificationsTbl::selectById($user_id);
        }
        foreach($notifications as $notification){
            $notification['notification_data'] = json_decode($notification->notification_data);
        }
        if(count($notifications)>0){
            $response = $this->HelperMethods->getObjectResponse(0, 'Notifications found successfully', $notifications);
            return $response;
        }else{
            $response = $this->HelperMethods->getObjectResponse(1, 'Error finding Notifications', '');
            return $response;
        }
    }

    public function updateNotiReadStatus($request){
        $user_id = $request->user_id;
        notificationsTbl::updateNotiReadStatus($user_id);
        $response = $this->HelperMethods->getEmptyResponse(0, 'Notification status updated successfully', '');
        return $response;
    }

    public function getProviderEarnings($request){
        $provider_id = $request->provider_id;
        $is_paid = $request->is_paid;
        $Earnings = rawQuery::getProviderEarnings($provider_id, $is_paid);
        if(count($Earnings) > 0){
            $response = $this->HelperMethods->getArrayResponse(0, 'Provider earnings found successfully', $Earnings);
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(0, 'Provider earnings found successfully', '');
            return $response;
        }
    }
    public function getProviderTotalEarnings($request){
        $provider_id = $request->provider_id;
        $type = $request->type;
        $Earnings = rawQuery::getProviderTotalEarnings($provider_id,$type);
        if(count($Earnings) > 0){
            $response = $this->HelperMethods->getObjectResponse(0, 'Provider earnings found successfully', $Earnings[0]);
            return $response;
        }else{
            $response = $this->HelperMethods->getObjectResponse(0, 'Provider earnings found successfully', '');
            return $response;
        }
    }
    public function cronJob(){
        $today = Carbon::now()->addDays(1);
        $myvar = round(strtotime($today) * 1000);
        return $myvar;
    }
    public function findProviderDrivers($request){
        $provider_user_id = $request->user_id;
        $service_id = $request->service_id;
        $job_start_time = $request->job_start_time;
        $job_end_time = $request->job_end_time;
        $job_area = $request->job_area;
        $drivers = rawQuery::findProviderDrivers($service_id,$job_start_time,$job_end_time,$job_area,$provider_user_id);
        if(count($drivers) > 0){
            $response = $this->HelperMethods->getArrayResponse(0, 'Drivers found successfully', $drivers);
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(1, 'No driver is available at this time', '');
            return $response;
        }
    }
    public function cancelJobCheck($request){
        $appointment_id = $request->appointment_id;
        $appointment_date = appointmentsTbl::getAppointmentTime($appointment_id);
        if(count($appointment_date) > 0) {
            $now = Carbon::now(+1);
            $job_date = $appointment_date[0]->save_date;
            $days = $now->diffInDays($job_date);
            if ($days == 0) {
                $time = $now->diffInMinutes($job_date);
                if ($time >= 30) {
                    $res = array('is_fee' => 1, 'description' => 'You are cancelling order after 30 minutes £50 cancellation fee will be charged to you');
                    $response = $this->HelperMethods->getObjectResponse(0, 'Successful', $res);
                    return $response;
                } else {
                    $res = array('is_fee' => 0, 'description' => '');
                    $response = $this->HelperMethods->getObjectResponse(0, 'Successful', $res);
                    return $response;
                }
            } else {
                $res = array('is_fee' => 1, 'description' => 'You are cancelling order after 30 minutes £50 cancellation fee will be charged to you');
                $response = $this->HelperMethods->getObjectResponse(0, 'Successful', $res);
                return $response;
            }
        }else{
            $response = $this->HelperMethods->getObjectResponse(1, 'Cannot find appointment to cancel', '');
            return $response;
        }
    }
    ///////////////////////// UPDATE JOB DATA METHOD ////////////////////////////
    public function updateJobData($request){
        $address = str_replace("،", "",$request->job_address);;
        $appointment_id = $request->appointment_id;
        $job_type = $request->service_type;
        $appointment_date = $request->job_dates[0];
        $skip_req_days = $request->skip_req_days;
        $job_start_time = $request->job_start_time;
        $job_end_time = $request->job_end_time;
        $skip_pickup_date = 0;
        if($job_type == 2){
            $days = $skip_req_days * 86400000;
            $skip_pickup_date = $appointment_date + $days;
        }
        $jobData = array('tbl_appointments.appointment_type'=>$job_type,'tbl_appointments.appointment_date'=>$appointment_date,
            'tbl_appointments.pickup_date'=>$skip_pickup_date,'tbl_job.skip_req_days'=>$skip_req_days,
            'tbl_job.job_address'=>$address,'tbl_job.job_start_time'=>$job_start_time,'tbl_job.job_end_time'=>$job_end_time);
        $resp = appointmentsTbl::updateJobAppointment($appointment_id,$jobData);
        if($resp == 1){
            $response = $this->HelperMethods->getEmptyResponse(0, 'job data updated successfully', '');
            return $response;
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1, 'Error Updating job data', '');
            return $response;
        }
    }
    public function getTimeSlots($request){
        $d_date_mill = $request->d_date;
        $t_date_mill = $request->t_date;
        $time_zone = 'Europe/London';
        $d_seconds = $d_date_mill / 1000;
        $t_seconds = $t_date_mill / 1000;
        $d_date = Carbon::createFromTimestamp($d_seconds,$time_zone)->format('Y-m-d');
        $t_date = Carbon::createFromTimestamp($t_seconds,$time_zone)->format('Y-m-d');
        $d_date_day = Carbon::createFromTimestamp($d_seconds,$time_zone)->format('D');
        $t_date_day = Carbon::createFromTimestamp($t_seconds,$time_zone)->format('D');
        $t_date_time = Carbon::createFromTimestamp($t_seconds,$time_zone)->format('H:i');
        if($d_date == $t_date && $t_date_day == 'Sun'){
            $resp = timeSlotTbl::todaysaSlots('Mon',0);
            $date = Carbon::createFromTimestamp($t_seconds,$time_zone)->addDays(2)->format("Y-m-d");
        } else
        if($d_date == $t_date && $t_date_day != 'Sat' && $t_date_time < '11:00'){
            $resp = timeSlotTbl::todaysaSlots($t_date_day,2);
            $date = $t_date;
        }else if($d_date == $t_date && $t_date_day == 'Sat' && $t_date_time < '11:00'){
            $resp = timeSlotTbl::todaysaSlots('Mon',0);
            $date = Carbon::createFromTimestamp($t_seconds,$time_zone)->addDays(2)->format("Y-m-d");
        }
        else if($d_date == $t_date && $t_date_time > '11:00' && $t_date_day != 'Fri' && $t_date_day != 'Sat'){
            $n_day = Carbon::createFromTimestamp($t_seconds,$time_zone)->addDays(1)->format('D');
            $resp = timeSlotTbl::todaysaSlots($n_day,0);
            $date = Carbon::createFromTimestamp($t_seconds,$time_zone)->addDays(1)->format("Y-m-d");
        }else if($d_date == $t_date && $t_date_day == 'Fri' || $t_date_day == 'Sat' && $t_date_time > '11:00'){
            $resp = timeSlotTbl::todaysaSlots('Mon',0);
            if($t_date_day == 'Fri'){
                $date = Carbon::createFromTimestamp($t_seconds,$time_zone)->addDays(3)->format("Y-m-d");
            }else if($t_date_day == 'Sat'){
                $date = Carbon::createFromTimestamp($t_seconds,$time_zone)->addDays(2)->format("Y-m-d");
            }
        }
        else if($d_date != $t_date && $d_date_day == 'Sun'){
            $resp = timeSlotTbl::todaysaSlots('Mon',0);
            $date = Carbon::createFromTimestamp($d_seconds,$time_zone)->addDays(1)->format("Y-m-d");
        }else
        if($d_date != $t_date && $t_date_day == 'Fri' && $d_date_day == 'Sat' && $t_date_time > '11:00'){
            $resp = timeSlotTbl::todaysaSlots('Mon',0);
            $date = Carbon::createFromTimestamp($d_seconds,$time_zone)->addDays(2)->format("Y-m-d");
        }
        else if($d_date != $t_date ){
            $resp = timeSlotTbl::todaysaSlots($d_date_day,0);
            $date = $d_date;
        }
        $respon = array('time_slots'=>$resp ,'date'=>$date);
        $response = $this->HelperMethods->getArrayResponse(0, 'Time slots found', $respon);
        return $response;
    }
    public function findAutoDrivers($request){
        $job_id = $request->job_id;
        $job_detail = jobTbl::getjobDetail($job_id);
        $service_id = $job_detail[0]->service_id;
        $job_start_time = $job_detail[0]->job_start_time;
        $job_end_time = $job_detail[0]->job_end_time;
        $job_address = $job_detail[0]->job_address;
        if($job_address == ''){
            $job_area = 'W5';
        }else{
            $myArray = explode(',', $job_address);
            $index = sizeof($myArray) - 1;
            $post_code = explode(' ', $myArray[$index]);
            $job_area = $post_code[1];
        }
        $drivers = rawQuery::getDriversJobRequest($service_id,$job_start_time,$job_end_time,$job_area);
        foreach ($drivers as $driver){
            $result = jobRequestTbl::checkIfexist($driver->user_id,$job_id);
            if(!empty($result[0]->req_id)){
            }else{
                $data = array('job_id'=>$job_id,'driver_user_id'=>$driver->user_id);
                jobRequestTbl::insertion($data);
            }
        }
        if(count($drivers)>0){
            $response = $this->HelperMethods->getArrayResponse(0, 'Drivers found successfully', $drivers);
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(1, 'No driver found', '');
            return $response;
        }
    }
    public function jobRequestEmail($customer_id,$job_id){
        $cust_Info =  userInfoTbl::getcustomerEmailPhone($customer_id);
        $job_type = null;
        if($job_id == 0){
            $mail = $_ENV['ADMINEMAIL'];
            Mail::send('emails.failedjob', ['data' => $cust_Info[0],'job_id'=>$job_id,'reason'=>'Unable to find any driver', 'body'=> 'A customer tries to place order but he did not find any driver.'], function ($message) use ($mail) {
                $message->from($_ENV['INFOEMAIL'], 'Skrap');
                $message->to($mail)->subject('Customer job request failed');
            });
        }else{
            $driver_id = $_ENV['AUTO_DRIVER_ID'];
            $data = array('job_id'=>$job_id,'driver_user_id'=>$driver_id);
            jobRequestTbl::insertion($data);
            $checkAccepted = jobTbl::checkAccepted($job_id);
            $job_detail = jobTbl::getjobSerDetail($job_id);
            $job_detail[0]->appointment_type = $job_type;
            $job_failed = jobAndReqFailedTbl::getJobFailedReason($job_id);
            if($checkAccepted[0]->driver_user_id == null && $checkAccepted[0]->status != 2 && count($job_failed) == 0){
                $request = Request::create('','POST',array('job_id'=>$job_id,'driver_user_id'=>$driver_id,'user_id'=>$driver_id));
                $resp = $this->jobAccepted($request);
                if($resp['code'] == 0){
                    $job_detail[0]->appointment_type = $resp['result']['appointment_type'];
                    $mail = $_ENV['ADMINEMAIL'];
                    Mail::send('emails.failedjob', ['data' => $cust_Info[0],'job_detail'=>$job_detail[0],'job_id'=>$job_id,'reason'=>'','body'=> 'A customer tries to place order but he did not find any driver, we have auto accepted job and assigned to specified driver.'], function ($message) use ($mail) {
                        $message->from($_ENV['INFOEMAIL'], 'Skrap');
                        $message->to($mail)->subject('Customer job request Auto Accepted');
                    });
                    $response = $this->HelperMethods->getObjectResponse(0, 'Skrap has accepted your job request.',$resp['result']);
                    return $response;
                }else if($resp['code'] == 8){
                    $job_failed = jobAndReqFailedTbl::getJobFailedReason($job_id);
                    $failed_resp = json_decode($job_failed[0]->failed_reason);
                    $mail = $_ENV['ADMINEMAIL'];
                    Mail::send('emails.failedjob', ['data' => $cust_Info[0],'job_detail'=>$job_detail[0],'job_id'=>$job_id,'reason'=>$failed_resp->description, 'body' => 'A customer tries to place order but the job failed, '. $failed_resp->description.'.'], function ($message) use ($mail) {
                        $message->from($_ENV['INFOEMAIL'], 'Skrap');
                        $message->to($mail)->subject('Customer job request failed');
                    });
                    $response = $this->HelperMethods->getObjectResponse(1, $failed_resp->description.', For more infromation  please call Skrap customer support on 0330 113 1561.', '');
                    return $response;
                }else{
                    $mail = $_ENV['ADMINEMAIL'];
                    Mail::send('emails.failedjob', ['data' => $cust_Info[0],'job_detail'=>$job_detail[0],'job_id'=>$job_id,'reason'=>'System Error, unable to assign job', 'body' => 'A customer tries to place order but the job failed, System Error, unable to assign job.'], function ($message) use ($mail) {
                        $message->from($_ENV['INFOEMAIL'], 'Skrap');
                        $message->to($mail)->subject('Customer job request failed');
                    });
                    $response = $this->HelperMethods->getObjectResponse(2, 'System Error, unable to process job, For more infromation  please call Skrap customer support on 0330 113 1561.', '');
                    return $response;
                }
            }else if($checkAccepted[0]->driver_user_id == null && $checkAccepted[0]->status == 0 && count($job_failed) > 0){
                $failed_resp = json_decode($job_failed[0]->failed_reason);
                $mail = $_ENV['ADMINEMAIL'];
                Mail::send('emails.failedjob', ['data' => $cust_Info[0],'job_detail'=>$job_detail[0],'job_id'=>$job_id,'reason'=>$failed_resp->description, 'body' => 'A customer tries to place order but the job failed, '.$failed_resp->description.'.'], function ($message) use ($mail) {
                    $message->from($_ENV['INFOEMAIL'], 'Skrap');
                    $message->to($mail)->subject('Customer job request failed');
                });
                $response = $this->HelperMethods->getObjectResponse(1, $failed_resp->description.', For more infromation  please call Skrap customer support on 0330 113 1561.', '');
                return $response;
            }else if(count($job_failed) > 0){
                $failed_resp = json_decode($job_failed[0]->failed_reason);
                $mail = $_ENV['ADMINEMAIL'];
                Mail::send('emails.failedjob', ['data' => $cust_Info[0],'job_detail'=>$job_detail[0],'job_id'=>$job_id,'reason'=>$failed_resp->description, 'body' => 'A customer tries to place order but the job failed, '.$failed_resp->description.'.'], function ($message) use ($mail) {
                    $message->from($_ENV['INFOEMAIL'], 'Skrap');
                    $message->to($mail)->subject('Customer job request failed');
                });
                $response = $this->HelperMethods->getObjectResponse(1, $failed_resp->description.', For more infromation  please call Skrap customer support on 0330 113 1561.', '');
                return $response;
            }
            else if($checkAccepted[0]->driver_user_id != null){
                $appointment = appointmentsTbl::getAppointmentId($job_id);
                $response = $this->HelperMethods->getObjectResponse(3, 'Your job request is accepted by service provider.', $appointment[0]);
                return $response;
            }else{
                $response = $this->HelperMethods->getObjectResponse(2, 'Sorry your oder failed for some reason, For more infromation  please call Skrap customer support on 0330 113 1561.', '');
                return $response;
            }
        }
    }
    public function cancelJobRequest($request){
        $job_id = $request->job_id;
        $accepted = jobTbl::checkAccepted($job_id);
        if($accepted[0]->driver_user_id == null){
            $res = jobTbl::cancelJobReq($job_id);
            $device_ids = jobRequestTbl::getJobRequestUsers($job_id);
            if(count($device_ids)>0 || $res == 0){
                $deviceid = $device_ids->pluck('device_id')->values()->toArray();
                $ios_id = $device_ids->pluck('ios_id')->values()->toArray();
                $web_token = $device_ids->pluck('web_token')->values()->toArray();
                $deviceIds = array_merge($deviceid,$web_token);
                $title = 'Job canceled';
                $body = 'Your job has been canceled';
                $noti_data = array('job_id'=>$job_id);
                $type = 7;
                if(!empty($deviceIds) || !empty($ios_ids)) {
                    //////////////////// send notifications by calling notification method of FCM controller /////////
                    try {
                        $pushresp = $this->FCMcontroller->FCMSendNotifications($deviceIds, $noti_data, $title, $body, $type);
                        $pushresp2 = $this->FCMcontroller->FCMSendNotificationsIos($ios_id, $noti_data, $title, $body, $type);
                        $response = $this->HelperMethods->getEmptyResponse(0, 'Job cancelled successfully','');
                        return $response;
                    } catch (Exception $e) {

                    }
                }
                else{
                    $response = $this->HelperMethods->getEmptyResponse(2, 'unable to find any requested drivers','');
                    return $response;
                }
            }else{
                $response = $this->HelperMethods->getEmptyResponse(2, 'unable to find any requested drivers','');
                return $response;
            }
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1, 'Job Request Accepted','');
            return $response;
        }
    }
    public function getExtentions($request){
        $resp = extentionTbl::getSkipExtention();
        if(count($resp)>0){
            $response = $this->HelperMethods->getArrayResponse(0, 'Skip extensions found successfully',$resp);
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(1, 'Error getting extensions list','');
            return $response;
        }
    }
    public function addExtention($request){
        $job_id = $request->job_id;
        $extention_id = $request->extention_id;
        $extention_amount = $request->extention_amount;
        $pickup_date = $request->pickup_date;
        $card_id = $request->card_id;
        $cust_user_id = $request->user_id;
        $update_job = array('extention_id'=>$extention_id,'extention_amount'=>$extention_amount);
        $update_appointment = array('pickup_date'=>$pickup_date);
        $payinRes = $this->PaymentController->createCharge($job_id,$card_id,$extention_amount,$cust_user_id);
        if($payinRes['code'] == 1){
            return $response = $this->HelperMethods->getEmptyResponse(2,$payinRes['description'],'');
            exit;
        }else{
            $resp = jobTbl::updateExtention($job_id,$update_job);
            $resp1 = appointmentsTbl::updatePickup($job_id,$update_appointment);
            if($resp == 1 && $resp1 == 1){
                $deviceIds = userTbl::getDriverDeviceIdsByJobId($job_id);
                if ($deviceIds[0]->device_id != null) {
                    $pushresp = $this->FCMcontroller->FCMSendNotifications($deviceIds[0]->device_id, array('job_id' => $job_id), 'Skip Extension Request', 'Skip extension is requested by customer', 8);
                } else {
                    $pushresp2 = $this->FCMcontroller->FCMSendNotificationsIos($deviceIds[0]->ios_id, array('job_id' => $job_id), 'Skip Extension Request', 'Skip extension is requested by customer', 8);
                }
                $response = $this->HelperMethods->getEmptyResponse(0, 'Extension added successfully','');
                return $response;
            }else{
                $response = $this->HelperMethods->getEmptyResponse(1, 'Error adding Extension','');
                return $response;
            }
        }
    }
    public function requestCollection($request){
        $job_id = $request->job_id;
        $pickup_date = $request->pickup_date;
        $update_appointment = array('pickup_date'=>$pickup_date);
        $res = appointmentsTbl::updatePickup($job_id,$update_appointment);
        if($res == 1) {
            $deviceIds = userTbl::getDriverDeviceIdsByJobId($job_id);
            if ($deviceIds[0]->device_id != null) {
                $pushresp = $this->FCMcontroller->FCMSendNotifications($deviceIds[0]->device_id, array('job_id' => $job_id), 'Skip Collection Request', 'Skip collection is requested by customer', 9);
            } else {
                $pushresp2 = $this->FCMcontroller->FCMSendNotificationsIos($deviceIds[0]->ios_id, array('job_id' => $job_id), 'Skip Collection Request', 'Skip collection is requested by customer', 9);
            }
            $response = $this->HelperMethods->getEmptyResponse(0, 'Skip collection request submitted successfully', '');
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1, 'Skip collection request already submitted', '');
        }
        return $response;
    }
    public function sendGenNotification(){
        $resp = userTbl::getAllDeviceIds();
        $mobileTokens = $resp->pluck('device_id')->values()->toArray();
        $this->FCMcontroller->sendGenNotification($mobileTokens);
    }
    public function uploadJobImages($job_id, $job_images){
        jobImagesTbl::deleteJobImages($job_id);
        if(count($job_images) > 0){
            $imagesArr = [];
            foreach ($job_images as $image){
                if($image != '' || $image != null){
                    $img = $image;
                    $img = str_replace('data:image/jpeg;base64,', '', $img);
                    $data = base64_decode($img);
                    $image_name = uniqid().'.png';
                    $destinationPath = public_path('/job_images/'.$image_name);
                    file_put_contents($destinationPath, $data);
                    $job_image_url = url('/')."/job_images/".$image_name;
                    $imgData = array('job_id'=>$job_id, 'image_url'=>$job_image_url);
                    array_push($imagesArr, $imgData);

                }
            }
            $resp = jobImagesTbl::insertJobImages($imagesArr);
            return $resImg = jobImagesTbl::getJobImages($job_id);
        }
    }
}
