<?php

namespace App\Http\Controllers;

use App\userInfoTbl;
use App\jobTbl;
use App\rawQuery;
use App\stripeCustomerTbl;
use App\stripeChargeTbl;
use App\stripeRefunTbl;
use App\stripeSourcesTbl;
use App\stripePayoutTbl;
use App\providerTbl;
use App\providerBankManualTbl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Mail;
use GuzzleHttp\Client;
use File;

class PaymentController extends Controller
{
    protected $HelperMethods;
    protected $stripeController;

    public function __construct(HelperMethods $HelperMethods, stripeController $stripeController) {
        $this->HelperMethods = $HelperMethods;
        $this->stripeController = $stripeController;
    }

    /////////////////////// CREATE STRIPE CUSTOMER ////////////////////////////////

    public function createCustomer($user_id){
        /////////////////////// selecting data from user info table ///////////////////
        $res = userInfoTbl::selectSingle($user_id);
        $first_name = $res[0]->first_name;
        $last_name = $res[0]->last_name;
        $email = $res[0]->email;
        ////////////////// updating user address ///////////////////////////
        $check_customer_exists = stripeCustomerTbl::checkStripeCustomerExists($user_id);
        if(count($check_customer_exists) > 0){
            $response = $this->HelperMethods->getEmptyResponse(1, 'Stripe customer already exists', '');
            return $response;
        }
        else{
            $Custresp = $this->stripeController->createCustomer($first_name, $last_name, $email);
            if($Custresp['code'] === 0){
                $stripeCustomer = array('user_id'=>$user_id,'stripe_customer_id'=>$Custresp['result']['id']);
                $custSave = stripeCustomerTbl::insertCustomer($stripeCustomer);
                if($custSave == 1){
                    $response = $this->HelperMethods->getObjectResponse(0, 'Stripe customer created successfully', $Custresp);
                    return $response;
                }else{
                    $response = $this->HelperMethods->getEmptyResponse(3, 'Error saving stripe Customer','');
                    return $response;
                }
            }else{
                $response = $this->HelperMethods->getObjectResponse(2, 'Error creating stripe customer', $Custresp);
                return $response;
            }
        }
    }

    public function createCustomerCardSource($request){
        $user_id = $request->user_id;
        $card_number = $request->card_number;
        $exp_month = $request->exp_month;
        $exp_year = $request->exp_year;
        $cvv = $request->cvv;
        $name = $request->card_holder_name;
        $customer_id = stripeCustomerTbl::selectCustomerId($user_id);
        if(count($customer_id) > 0){
            $Custresp = $this->stripeController->createSource($card_number, $exp_month, $exp_year, $cvv, $name);
            if($Custresp['code'] == 0){
                $source_id = $Custresp['result']['id'];
                $stripe_customer_id = $customer_id[0]->stripe_customer_id;
                $customerId = $customer_id[0]->customer_id;
                $sourceResp = $this->stripeController->getAllSources($stripe_customer_id);
                if(json_decode(json_encode($sourceResp['result']), True) != []){
                    $object = $sourceResp['result'];
                    $cards = array_column($object, 'card');
                    $fingerPrints = array_column($cards, 'fingerprint');
                    if (in_array($Custresp['result']['card']['fingerprint'], $fingerPrints))
                    {
                        $response = $this->HelperMethods->getEmptyResponse(5, 'This card is already attached to your account','');
                        return $response;
                        exit;
                    }
                }
                $sourceAssign = $this->stripeController->assignSourceCustomer($stripe_customer_id,$source_id);
                if($sourceAssign['code'] == 0){
                    $source = array('customer_id'=>$customerId,'stripe_source_id'=>$source_id);
                    $sourceSave = stripeSourcesTbl::insertSource($source);
                    if($sourceSave == 1){
                        $response = $this->HelperMethods->getEmptyResponse(0, 'card created Successfully','');
                        return $response;
                    }else{
                        $response = $this->HelperMethods->getEmptyResponse(2, 'Error saving source','');
                        return $response;
                    }
                }else{
                    $response = $this->HelperMethods->getEmptyResponse(3, $sourceAssign['description'],'');
                    return $response;
                }
            }else{
                $response = $this->HelperMethods->getEmptyResponse(4, $Custresp['description'],'');
                return $response;
            }
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1, 'Cannot create card Stripe customer not found','');
            return $response;
        }
    }

    public function getSourceList($user_id){
        $customer_id = stripeCustomerTbl::selectCustomerId($user_id);
        if(count($customer_id) > 0){
            $customerId = $customer_id[0]->customer_id;
            $Source_list = stripeSourcesTbl::getSourceIdByCustomerId($customerId);
            $sourceData = [];
            foreach($Source_list as $source){
                $source_id = $source->stripe_source_id;
                $sourceResp = $this->stripeController->getSource($source_id);
                if($sourceResp['result'] != ""){
                    if($sourceResp['result']['status'] == 'consumed'){
                        $this->deactivateCard($user_id, $sourceResp['result']['id']);
                    }else{
                        array_push($sourceData,$sourceResp['result']);
                    }
                }
            }
            if(count($sourceData) > 0){
                $response = $this->HelperMethods->getArrayResponse(0, 'customer found',$sourceData);
                return $response;
            }else{
                $response = $this->HelperMethods->getArrayResponse(2, 'No cards found','');
                return $response;
            }
        } else{
            $response = $this->HelperMethods->getArrayResponse(1, 'customer not found','');
            return $response;
        }

    }
    public function createCharge($job_id,$card_id,$amount,$customer_user_id){
        $customer_id = stripeCustomerTbl::selectCustomerId($customer_user_id);
        $get_source_id = stripeSourcesTbl::getSourceId($card_id);
        if(count($get_source_id) > 0){
            $source_id = $get_source_id[0]->source_id;
            $stripe_customer_id = $customer_id[0]->stripe_customer_id;
            $customerId = $customer_id[0]->customer_id;
            $CargeResp = $this->stripeController->createCharge($stripe_customer_id,$card_id,$amount);
            if($CargeResp['code'] == 0){
                $payainObj = json_encode($CargeResp['result']);
                $payment = array('job_id'=>$job_id,'customer_id'=>$customerId,'source_id'=>$source_id,'charge_amount'=>$amount,
                    'stripe_charge_id'=>$CargeResp['result']['id'],'charge_status'=>$CargeResp['result']['status'],
                    'charge_obj'=>$payainObj);
                $reponse = stripeChargeTbl::insertCharge($payment);
                if($reponse == 1){
                    $statusPay = 1;
                    $paid = jobTbl::updatePaymentStatus($job_id,$statusPay);
                    if($paid == 1){
                        return $response = $this->HelperMethods->getEmptyResponse(0, 'Payment successful','');
                    }else{
                        return $response = $this->HelperMethods->getEmptyResponse(2, 'Payment status not updated','');
                    }
                }
            }else{
                return $response = $this->HelperMethods->getArrayResponse(1, $CargeResp['description'],$CargeResp);
            }
        }else{
            return $response = $this->HelperMethods->getEmptyResponse(2, 'this card is deleted '. $card_id, '');
        }
    }
    public function refundCharge($appointment_id,$is_fee){
        $payment_data = rawQuery::getPaymentDetailByAppId($appointment_id);
        $stripe_charge_id = $payment_data[0]->stripe_charge_id;
        $job_id = $payment_data[0]->job_id;
        $charge_id = $payment_data[0]->charge_id;
        $customer_id = $payment_data[0]->customer_id;
        $source_id = $payment_data[0]->source_id;
        $amount = $payment_data[0]->transaction_cost;
        $refundResp = $this->stripeController->refundCharge($stripe_charge_id,$is_fee,$amount);
        if($refundResp['code'] == 0){
            $refund = array('job_id'=>$job_id,'customer_id'=>$customer_id,'source_id'=>$source_id,'charge_id'=>$charge_id,
                'stripe_refund_id'=>$refundResp['result']['id'],'refund_status'=>$refundResp['result']['status'],
                'refund_obj'=>json_encode($refundResp['result']));
            $res = stripeRefunTbl::refundInsert($refund);
            if($res == 1){
                $statusPay = 4;
                $paid = jobTbl::updatePaymentStatus($job_id, $statusPay);
                $response = $this->HelperMethods->getEmptyResponse(0, 'charge Refunded successfully','');
                return $response;
            }
        }else{
            $response = $this->HelperMethods->getEmptyResponse($refundResp['code'], $refundResp['description'],'');
            return $response;
        }
    }

    public function getProviderAccountDetail($request){
        $provider_user_id = $request->provider_id;
        $provider = providerTbl::getProviderId($provider_user_id);
        $provider_id = $provider[0]->provider_id;
        $res = providerBankManualTbl::getAccountDetail($provider_id);
        if(count($res)>0){
            $response = $this->HelperMethods->getObjectResponse(0, 'Account Info Found successfully',$res[0]);
            return $response;
        } else {
            $response = $this->HelperMethods->getObjectResponse(1, 'Account Info found Found','');
            return $response;
        }
    }
    public function updateProviderAccountDetail($request){
        $bank_id = $request->bank_id;
        $account_number = $request->account_number;
        $account_type = $request->account_type;
        $account_name = $request->account_name;
        $sort_code = $request->sort_code;
        $accountDetail = array('account_number'=>$account_number, 'account_name'=>$account_name, 'account_type'=>$account_type, 'sort_code'=>$sort_code);
        $res = providerBankManualTbl::updateAccountInfo($accountDetail,$bank_id);
        if($res === 1){
            $response = $this->HelperMethods->getEmptyResponse(0, 'Account Info updated successfully','');
            return $response;
        } else {
            $response = $this->HelperMethods->getEmptyResponse(1, 'Error updating account detail','');
            return $response;
        }
    }
    public function setDefaultSource($request){
        $source_id = $request->card_id;
        $user_id = $request->user_id;
        $SourceRemove = stripeSourcesTbl::removeAllDefaultSources($user_id);
        $SourceDefault = stripeSourcesTbl::setDefaultSource($user_id, $source_id);
        if($SourceDefault == 1){
            $response = $this->HelperMethods->getEmptyResponse(0, 'source successfully set as default ','');
            return $response;
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1, 'error setting Default source','');
            return $response;
        }
    }
    public function getDefaultSource($request){
        $user_id = $request->user_id;
        $SourceDefault = stripeSourcesTbl::getDefaultSource($user_id);
        if(count($SourceDefault) > 0){
            $source_id = $SourceDefault[0]->stripe_source_id;
            $sourceResp = $this->stripeController->getSource($source_id);
            if($sourceResp['code'] == 0){
                $response = $this->HelperMethods->getObjectResponse(0,'Default source found successfully',$sourceResp['result']);
                return $response;
            }else{
                $response = $this->HelperMethods->getObjectResponse(2,$sourceResp['description'],'');
                return $response;
            }
        }else{
            $response = $this->HelperMethods->getObjectResponse(1, 'Default source not found','');
            return $response;
        }
    }
    public function deactivateCard($user_id, $source_id){
        $customer_id = stripeCustomerTbl::selectCustomerId($user_id);
        if(count($customer_id)>0){
            $stripeCustomerId = $customer_id[0]->stripe_customer_id;
            $resp = $this->stripeController->DetachSource($source_id,$stripeCustomerId);
            if($resp['code']==0 || $resp['code']==404){
                $result = stripeSourcesTbl::DeleteResource($source_id);
                $response = $this->HelperMethods->getEmptyResponse(0, 'Card Deactivated Successfully','');
                return $response;
            }else{
                $response = $this->HelperMethods->getEmptyResponse(1, $resp['description'],'');
                return $response;
            }
        }else{
            $response = $this->HelperMethods->getEmptyResponse(2, 'Error Deactivating card','');
            return $response;
        }
    }
    public function extrachargeCustomer($request){
        $cust_id = $request->cust_id;
        $source_id = $request->source_id;
        $amount = $request->amount;

        $response = $this->stripeController->createCharge($cust_id,$source_id,$amount);
        return $response;
    }
    public function updatePaymentStatus($request){
        $job_id = $request->job_id;
        $status = $request->status;
        $resp = jobTbl::updatePaymentStatus($job_id, $status);
        if($resp > 0) {
            $response = $this->HelperMethods->getEmptyResponse(0, 'Payment status updated successfully','');
            return $response;
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1, 'Error updating payment status','');
            return $response;
        }
    }
    public function chargeCustomer($request){
        $cust_user_id = $request->customer_user_id;
        $source_id = $request->card_id;
        $amount = $request->amount;
        $job_id = $request->job_id;
        $resp = $this->createCharge($job_id,$source_id,$amount,$cust_user_id);
        if($resp['code'] == 0){
            $update = jobTbl::updateJobCardId($job_id,$source_id);
            if($update == 1){
                return $response = $this->HelperMethods->getEmptyResponse(0, 'Payment successful','');
            }
        }else{
            return $resp;
        }
    }
}
