<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class adminApiController extends Controller
{
    protected $HelperMethods;
    protected $UserMethods;
    protected $JobMethods;
    protected $ServicesMethods;
    protected $VehicleMethods;
    protected $PaymentController;
    protected $adminStatsController;
    public function __construct(HelperMethods $HelperMethods, UserMethods $UserMethods, JobMethods $JobMethods, ServicesMethods $ServicesMethods, VehicleMethods $VehicleMethods, PaymentController $PaymentController, adminStatsController $adminStatsController)
    {
        $this->HelperMethods = $HelperMethods;
        $this->UserMethods = $UserMethods;
        $this->JobMethods = $JobMethods;
        $this->ServicesMethods = $ServicesMethods;
        $this->VehicleMethods = $VehicleMethods;
        $this->PaymentController = $PaymentController;
        $this->adminStatsController = $adminStatsController;
    }

    public function getAllJobs(Request $request){
        $response = $this->JobMethods->getAllJobs($request);
        return $response;
    }
    public function getJobDetailAdmin(Request $request){
        $response = $this->JobMethods->getJobDetailAdmin($request);
        return $response;
    }
    public function getProviderListing(Request $request){
        $response = $this->UserMethods->getProviderListing($request);
        return $response;
    }
    public function getProviderDetail(Request $request){
        $response = $this->UserMethods->getProviderDetail($request);
        return $response;
    }
    public function verifyProvider(Request $request){
        $response = $this->UserMethods->verifyProvider($request);
        return $response;
    }
    public function getProviderEarnings(Request $request){
        $response = $this->JobMethods->getProviderEarnings($request);
        return $response;
    }
    public function getProviderTotalEarnings(Request $request){
        $response = $this->JobMethods->getProviderTotalEarnings($request);
        return $response;
    }
    public function providerTransferPayout(Request $request){
        $response = $this->PaymentController->providerTransferPayout($request);
        return $response;
    }
    public function getCustomerListing(Request $request){
        $response = $this->UserMethods->getCustomerListing($request);
        return $response;
    }
    public function addPromoCode(Request $request){
        $response = $this->HelperMethods->createCoupon($request);
        return $response;
    }
    //////////////////////// GET PENDING JOB LISTING SERVICE METHOD ////////////////////////////

    public function getAutoAcceptJobListing(Request $request){
        $response = $this->JobMethods->getAutoAcceptJobListing();
        return $response;
    }
    //////////////////////// FIND AUTO ACCEPT DRIVERS SERVICE METHOD ////////////////////////////

    public function findAutoDrivers(Request $request){
        $response = $this->JobMethods->findAutoDrivers($request);
        return $response;
    }
    //////////////////////// GET ADMIN INSTANCES SERVICE METHOD ////////////////////////////

    public function getAdminInstances(Request $request){
        $response = $this->adminStatsController->getAdminInstances();
        return $response;
    }
    //////////////////////// GET USERS CHART DATA SERVICE METHOD ////////////////////////////

    public function getUsersChartData(Request $request){
        $response = $this->adminStatsController->getUsersChartData();
        return $response;
    }
    //////////////////////// GET JOBS CHART DATA SERVICE METHOD ////////////////////////////

    public function getJobsChartData(Request $request){
        $response = $this->adminStatsController->getJobsChartData();
        return $response;
    }
    //////////////////////// GET EARNINGS CHART DATA SERVICE METHOD ////////////////////////////

    public function getOneMonthEarningsData(Request $request){
        $response = $this->adminStatsController->getOneMonthEarningsData();
        return $response;
    }
    public function deleteCoupon(Request $request){
        $response = $this->HelperMethods->deleteCoupon($request);
        return $response;
    }
    public function AddServiceRates(Request $request){
        $response = $this->ServicesMethods->AddServiceRates($request);
        return $response;
    }
    public function updateServiceRates(Request $request){
        $response = $this->ServicesMethods->updateServiceRates($request);
        return $response;
    }
    public function deleteServiceRate(Request $request){
        $response = $this->ServicesMethods->deleteServiceRate($request);
        return $response;
    }
    public function getAllServiceRates(){
        $response = $this->ServicesMethods->getAllServiceRates();
        return $response;
    }
    public function getAllAreaCodes(){
        $response = $this->ServicesMethods->getAllAreaCodes();
        return $response;
    }
    public function updateCoupon(Request $request){
        $response = $this->HelperMethods->updateCoupon($request);
        return $response;
    }
    public function getUserCredentials(Request $request){
        $response = $this->UserMethods->getUserCredentials($request);
        return $response;
    }
    public function updatePaymentStatus(Request $request){
        $response = $this->PaymentController->updatePaymentStatus($request);
        return $response;
    }
    public function sendMarketingMsg(Request $request){
        $response = $this->UserMethods->sendMarketingMsg($request);
        return $response;
    }
    public function enableMarketingMsg(Request $request){
        $response = $this->UserMethods->enableMarketingMsg($request);
        return $response;
    }
}
