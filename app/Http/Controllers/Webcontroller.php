<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class Webcontroller extends Controller
{
    public function SkrapLogs(){
        $todays = date("d-m-Y (D)");
        $logs = File::get(storage_path('logs/http'.$todays.'.log'));
        $user_seats = explode(';}', $logs);
        $data = array();
        foreach ($user_seats as $logData) {
            if (strlen($logData)) {
                $logData .= ';}';
                $booked_seat = $logData;
                $b = unserialize($booked_seat);
                array_push($data,$b);
            }
        }
        return view('LogsView.httpLogs',['data'=>$data]);
    }
    public function stripeTesting(){
        return view('LogsView.stripePaymentTest');
    }
}
