<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reports;
use Mail;
use Exception;
use Carbon\Carbon;

class ReportsController extends Controller
{
    protected $HelperMethods;
    public function __construct(HelperMethods $HelperMethods)
    {
        $this->HelperMethods = $HelperMethods;
    }
    public function getProviderPaymentReport($request){
        $user_id = $request->user_id;
        $from_date = $request->from;
        $to_date = $request->to;
        $report_type = $request->report_type;
        $report = Reports::GetPaymentReport($user_id,$from_date,$to_date,$report_type);
        $totalEarnings = Reports::getTotalEarnings($user_id,$from_date,$to_date,$report_type);
        $processedEarnings = Reports::getProcessedEarnings($user_id,$from_date,$to_date,$report_type);
        $pendingEarnings = Reports::getPendingEarnings($user_id,$from_date,$to_date,$report_type);
        $CancelledEarnings = Reports::getCancelledEarnings($user_id,$from_date,$to_date,$report_type);
        $resp_arr = array('report'=>$report,'totalEarnings'=>$totalEarnings[0]->totalEarnings,'processedEarnings'=>$processedEarnings[0]->totalEarnings,
        'pendingEarnings'=>$pendingEarnings[0]->totalEarnings,'CancelledEarnings'=>$CancelledEarnings[0]->totalEarnings);
        $response = $this->HelperMethods->getArrayResponse(0, 'Report Data found Successfully', $resp_arr);
        return $response;
    }
}
