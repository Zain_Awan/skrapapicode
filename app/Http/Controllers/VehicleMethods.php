<?php

namespace App\Http\Controllers;

use App\vehicleTbl;
use Illuminate\Http\Request;
use App\rawQuery;
use App\providerTbl;
use App\driverVehicleTbl;

//////////////////////// VEHICLES CLASS ///////////////////////////
class VehicleMethods extends Controller
{
    protected $HelperMethods;
    public function __construct(HelperMethods $HelperMethods)
    {
        $this->HelperMethods = $HelperMethods;
    }
    /////////////////// GET PROVIDERS UNASSIGNED VEHICLES METHOD ///////////////////////
    public function getProviderUnAssignedVehicles($request){
        ///////////////////// request data //////////////////////////
        $provider_user_id = $request->provider_user_id;
        /////////////////// get provider vehicle by calling rawQuery Model /////////////////////////
        $ProviderVehicles = rawQuery::getProviderUnAssignedVehicles($provider_user_id);
        ///////////////// if result has the data of vehicles ///////////////////
        if(count($ProviderVehicles)>0){
            ///////////////// return response ////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Vehicles found successfully',$ProviderVehicles);
            return $response;
        }else{
            ///////////////// return response ////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'No vehicles found','');
            return $response;
        }
    }
    /////////////////// ASSIGN DRIVER TO VEHICLES METHOD ///////////////////////
    public function assingDriverToVehicle(Request $request)
    {
        /////////////////////////// request Params //////////////////////////////////
        $driver_id = $request->driver_id;
        $vehicle_id = $request->vehicle_id;
        $vehicleDriver = array('driver_id'=>$driver_id,'vehicle_id'=>$vehicle_id);
        /////////////// assigning vehicle to driver using save driver to vehicle method //////////////////////////////
        $res = $this->HelperMethods->saveDriverToVehicle($vehicleDriver);
        ////////////// checking response //////////////////////////////////////////////
        if($res == 'DriverVehicleSaved'){
            //////////////////// return response ////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'vehicle assinged to driver successfully','');
            return $response;
        }else if($res == "ErrorSavingDriverVehicle"){
            //////////////////// return response ////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error assinging vehicle to driver','');
            return $response;
        }else if($res == 'exists'){
            //////////////////// return response ////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Vehicle already assigned to driver','');
            return $response;
        }
    }
    /////////////////// ADD VEHICLES METHOD ///////////////////////
    public function addVehicle($request){
        ////////////////////////// request Params ////////////////////
        $user_id = $request->user_id;
        $reg_number = $request->reg_number;
        $vehicle_type = $request->type;
        $owner_name = $request->owner_name;
        $make = $request->make;
        $model= $request->model;
        $doc_image = $request->image;
        ////////// get provider id using getProviderId Method ///////////////////
        $res = providerTbl::getProviderId($user_id);
        $provider_id = $res[0]->provider_id;
        //////////////////////// uploading image ////////////////////////
        $input['imagename'] = time().'.'.$doc_image->getClientOriginalName();
        $destinationPath = public_path('vehicleRegistrationImages');
        $doc_image->move($destinationPath, $input['imagename']);
        $imgname = url('/')."/vehicleRegistrationImages/".$input['imagename'];
        /////////////////////////////// creating array t insert data /////////////////////
        $vehicle = array('provider_id'=>$provider_id,'vehicle_type'=>$vehicle_type,'reg_number'=>$reg_number,'owner_name'=>$owner_name,'make'=>$make,'model'=>$model,'doc_image'=>$imgname);
        //////////////////// inserting data using saveVehivle Method /////////////////////////
        $res = $this->HelperMethods->saveVehicle($vehicle);
        ///////////////////// checking response //////////////////////////////////
        if($res == 'vehicleSaved'){
            /////////////// return response ////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'vehicle added successfully','');
            return $response;
        }else if($res == 'ErrorSavingvehicle'){
            /////////////// return response ////////////////////
            $response =  $response = $this->HelperMethods->getEmptyResponse(1,'Error adding Vehicle','');
            return $response;
        }else if($res == 'exists'){
            /////////////// return response ////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Vehicle already registerd with registaration number','');
            return $response;
        }
    }
    /////////////////// GET VEHICLES LISTINGS METHOD ///////////////////////
    public function getVehiclesListing($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $provider_id = $request->user_id;
        ////////////////////////// selecting vehicles data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getVehicleListing($provider_id);
        $vehiclessData = array();
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            foreach($resultquery as $drivers){
                $array = json_decode(json_encode($drivers), True);
                $vehicle_detail = array_slice($array,0,8);
                $data = array('vehicle_detail'=>$vehicle_detail);
                array_push($vehiclessData, $data);
            }
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Vehicles found successfully',$vehiclessData);
            return $response;
        }else{
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Vehicles not found','');
            return $response;
        }
    }

    /////////////////// Un Assign Driver Vehicle METHOD ///////////////////////
    public function UnAssignDriverVehicle($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $driver_id = $request->driver_id;
        $vehicle_id = $request->vehicle_id;
        ////////////////////////// selecting vehicles data using rawQuery model ////////////////////////////////////
        $res = driverVehicleTbl::UnAssignVehicle($driver_id, $vehicle_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($res == 1){
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Vehicle Unassigned Successfully','');
            return $response;
        }else{
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error Unassigning Vehicle','');
            return $response;
        }
    }

    public function getVehicleDetail($request){
        $vehicle_id = $request->vehicle_id;
        $res = vehicleTbl::getVehicleDetail($vehicle_id);
        if(count($res)>0){
            $response = $this->HelperMethods->getObjectResponse(0,'Vehicle detail found',$res[0]);
            return $response;
        }else{
            $response = $this->HelperMethods->getObjectResponse(1,'Error finding Vehicle detail','');
            return $response;
        }
    }

    public function updateVehicleInfo($request){
        $owner_name = $request->owner_name;
        $reg_number = $request->reg_number;
        $make = $request->make;
        $model = $request->model;
        $vehicle_type = $request->vehicle_type;
        $vehicle_id = $request->vehicle_id;
        $doc_image = $request->doc_image;

        if(!empty($doc_image)) {
            /////////////////////////////// uploading Image ////////////////////////////////////
            $input['imagename'] = time().'.'.$doc_image->getClientOriginalName();
            $destinationPath = public_path('vehicleRegistrationImages');
            $doc_image->move($destinationPath, $input['imagename']);
            $imgname = url('/')."/vehicleRegistrationImages/".$input['imagename'];
            $updateVehicle = array('owner_name'=>$owner_name,'reg_number'=>$reg_number,'make'=>$make,'model'=>$model,'vehicle_type'=>$vehicle_type,'doc_image'=>$imgname);
        }else{
            $updateVehicle = array('owner_name'=>$owner_name,'reg_number'=>$reg_number,'make'=>$make,'model'=>$model,'vehicle_type'=>$vehicle_type);
        }
        $res = vehicleTbl::updation($vehicle_id,$updateVehicle);
        $response = $this->HelperMethods->getEmptyResponse(0,'Vehicle updated successfully','');
        return $response;
    }
}
