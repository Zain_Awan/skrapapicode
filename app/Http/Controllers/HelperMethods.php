<?php

namespace App\Http\Controllers;

use App\rawQuery;
use App\skipRatesTbl;
use Illuminate\Http\Request;
use App\userTbl;
use App\providerTbl;
use App\userInfoTbl;
use App\servicesTbl;
use App\companyTbl;
use App\vatInfoTbl;
use App\providerServicesTbl;
use App\providerWalletTbl;
use App\jobTbl;
use App\driverTbl;
use App\vehicleTbl;
use App\driverServicesTbl;
use App\providerServiceAreasTbl;
use App\jobRequestTbl;
use App\appointmentsTbl;
use App\driverVehicleTbl;
use App\notificationsTbl;
use App\scheduleJobTbl;
use App\jobCancelTbl;
use App\dealsTbl;
use App\couponTbl;
use App\couponUserTbl;
use Mail;
use App\couponServicesTbl;
use App\couponAreasTbl;
use App\jobAndReqFailedTbl;
use Exception;
use Carbon\Carbon;

class HelperMethods extends Controller
{
    public function saveUser($mobile, $user_type, $email,$password,$referalId,$device_type){
            $exists = rawQuery::checkUser($mobile,$user_type,$email);
            if(count($exists)==0){
                $user_pwd = $this->randomPassword();
                $encrypted_userPassword = base64_encode($user_pwd);
                if($user_type == 2){
                    $users = array('user_name'=>$email,'user_type'=>$user_type,'user_pwd'=>$encrypted_userPassword);
                }else{
                    if($user_type == 1){
                        $users = array('user_name'=>$mobile,'user_type'=>$user_type,'user_pwd'=>base64_encode($password),'referal_id'=>$referalId,'device_type'=>$device_type);
                        //$users = array('user_name'=>$mobile,'user_type'=>$user_type,'user_pwd'=>$encrypted_userPassword);
                    }else{
                        $users = array('user_name'=>$mobile,'user_type'=>$user_type,'user_pwd'=>$encrypted_userPassword);
                    }
                }
                $res = userTbl::insertion($users);
                $user_pwd = base64_decode($res->user_pwd);
                $user_name = $res->user_name;
                $user_id = $res->user_id;
                $referal_id = $res->referal_id;
                $otp = $res->opt;
                $response = array('user_id'=>$user_id,'user_name'=>$user_name,'user_pwd'=>$user_pwd,'otp'=>$otp,'referal_id'=>$referal_id);
                return $response;
            }
            else
            {
                $res = 'already_registered';
                return $res;
            }
    }
    public function saveUserInfo($userInfo){
        $res = userInfoTbl::insertion($userInfo);
        return $res;
    }
    public function saveProvider($user_id,$vat_status){
        $provider = array('user_id'=>$user_id,'vat_status'=>$vat_status);
        $res = providerTbl::insertion($provider);
        return $res;
    }
    public function saveCompany($compInfo){
        // calling saveProvider method and passing params
        $comp_number = $compInfo['Registration_number'];
        $exists = $this->companyExists($comp_number);
        if($exists['code'] == 0){
            $res = companyTbl::insertion($compInfo);
            $response = $this->getEmptyResponse(0,'','');
            return $response;
        }else{
            $response = $this->getEmptyResponse(1,'','');
            return $response;
        }

    }
    public function saveVat($vatInfo){
        // calling saveProvider method and passing params
        $full_vat_number = $vatInfo['query'];
        $exists = $this->vatExists($full_vat_number);
        if($exists['code'] == 0){
            $res = vatInfoTbl::insertion($vatInfo);
            if($res == 1){
                $response = $this->getEmptyResponse(0,'','');
                return $response;
            }else{
                $response = $this->getEmptyResponse(1,'','');
                return $response;
            }
        }else{
            $response = $this->getEmptyResponse(1,'','');
            return $response;
        }

    }
    public function saveDriver($driverDetail){
        $res = driverTbl::insertion($driverDetail);
        return $res;
    }
    public function saveProviderServices($services,$provider_id){
        // calling saveProvider method and passing params
        //$var=explode(',',$services);
        foreach($services as $service_id)
        {
            $data = array('service_id' => $service_id, 'provider_id' => $provider_id);
            $res = providerServicesTbl::insertion($data);
        }
        return $res;
    }
    public function saveProviderServicesArea($districtAreas,$provider_id){
        // calling saveProvider method and passing params
        //$var=explode(',',$services);
        foreach($districtAreas as $district_id)
        {
            $data = array('district_code_id' => $district_id, 'provider_id' => $provider_id);
            $res = providerServiceAreasTbl::insertion($data);
        }
        return $res;
    }
    public function saveDriverServices($services,$driver_id){
        // calling saveProvider method and passing params
        $var=explode(',',$services);
        foreach($var as $service_id)
        {
            $service = array('service_id' => $service_id, 'driver_id' => $driver_id);
            $res = driverServicesTbl::insertion($service);
        }
        return $res;
    }
    public function saveVehicle($vehicle){
        $res = vehicleTbl::vehicleCheck($vehicle['reg_number']);
        if($res->count() == 0){
            $response = vehicleTbl::insertion($vehicle);
            if($response==1){
                return 'vehicleSaved';
            }else{
                return 'ErrorSavingvehicle';
            }
        }else{
            return 'exists';
        }
    }
    public function createReferalId($first_name){
        $alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $referalId = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $referalId[] = $alphabet[$n];
        }
        $referal = implode($referalId);
        $referalID = $first_name.'-'.$referal;
        return $referalID;
    }
    public function createCoupon($request){
        $title = $request->title;
        $short_desc = $request->short_desc;
        $ful_desc = $request->ful_desc;
        $coupon_code = $request->coupon_code;
        $coupon_type = $request->coupon_type;
        $discount = $request->discount;
        $max_dicount = $request->max_discount;
        $max_uses = $request->max_uses;
        $valid_for_days = $request->valid_for_days;
        $media = $request->media;
        $is_service = $request->is_services;
        $is_service_area = $request->is_service_area;
        $services = $request->services;
        $service_areas = $request->service_areas;
        $is_fixed = $request->is_fixed;
        $today = Carbon::now();
        $startDate = round(strtotime($today) * 1000);
        $nextDate = Carbon::now()->addDays($valid_for_days);
        $endDate = round(strtotime($nextDate) * 1000);

        $coupon_array = array('title'=>$title,'short_desc'=>$short_desc,'ful_desc'=>$ful_desc,'coupon_code'=>$coupon_code
        ,'coupon_type'=>$coupon_type,'discount'=>$discount,'max_discount'=>$max_dicount,'max_uses'=>$max_uses,'valid_for_days'=>$valid_for_days,
            'is_service'=>$is_service,'is_service_area'=>$is_service_area,'start_date'=>$startDate,'end_date'=>$endDate,'is_fixed'=>$is_fixed);

        if($media){
            $input['imagename'] = time().'.'.$media->getClientOriginalName();
            $destinationPath = public_path('/driverDocs/driving_license');
            $media->move($destinationPath, $input['imagename']);
            $media = url('/')."/driverDocs/driving_license/".$input['imagename'];
            $coupon_array['media'] = $media;
        }
        $coupon = couponTbl::insertCoupon($coupon_array);
        if($coupon !== 0){
            $coupon_id = $coupon->coupon_id;
            if($is_service == 1){
                $coupon_services = [];
                $var = explode(',',$services);
                foreach($var as $service_id)
                {
                    $service = array('service_id' => $service_id, 'coupon_id' => $coupon_id);
                    array_push($coupon_services,$service);
                }
                $serviceResp = couponServicesTbl::insertServices($coupon_services);
            }else{
                $serviceResp = 1;
            }
            if($is_service_area == 1){
                $coupon_areas = [];
                $var = explode(',',$service_areas);
                $res = rawQuery::getdistrictCodes($var);
                foreach($res as $area_id)
                {
                    $area = array('area_id' => $area_id->district_id, 'coupon_id' => $coupon_id);
                    array_push($coupon_areas,$area);
                }
                $arearesp = couponAreasTbl::insertAreas($coupon_areas);
            }else{
                $arearesp = 1;
            }
            if($serviceResp == 1 && $arearesp == 1){
                $response = $this->getEmptyResponse(0,'Coupon Added successfully','');
                return $response;
            }else{
                $response = $this->getEmptyResponse(1,'Error adding coupon','');
                return $response;
            }
        }
        else {
            $response = $this->getEmptyResponse(2,'Error adding coupon','');
            return $response;
        }
    }
    public function AssignCoupon($user_id, $type, $referal_user, $coupon_user_id){
        $coupon = couponTbl::selectCoupon($type);
        $activecoupon = couponTbl::findActiveCoupon($user_id);
        if(count($coupon)>0){
            if(count($activecoupon)>0){
                $is_active = 0;
            }else{
                $is_active = 1;
            }
            $today = Carbon::now();
            $startDate = round(strtotime($today) * 1000);
            $nextDate = Carbon::now()->addDays($coupon[0]->valid_for_days);
            $endDate = round(strtotime($nextDate) * 1000);
            $couponData = array('user_id'=>$user_id,'coupon_id'=>$coupon[0]->coupon_id,'start_date'=>$startDate,'end_date'=>$endDate,'total_use'=>$coupon[0]->max_uses,'is_active'=>$is_active,'referal_user_id'=>$referal_user);
            $copunUser = couponUserTbl::AddCouponUser($couponData);
            return $copunUser;
        }else{
            return 0;
        }
    }
    public function AssignCouponReferal($user_id, $type){
        $coupon_user_count = couponUserTbl::countCoupon($user_id);
        if($coupon_user_count < 20){
            $coupon = couponTbl::selectCoupon($type);
            $activecoupon = couponTbl::findActiveCoupon($user_id);
            if(count($coupon)>0){
                if(count($activecoupon)>0){
                    $is_active = 0;
                }else{
                    $is_active = 1;
                }
                $today = Carbon::now();
                $startDate = round(strtotime($today) * 1000);
                $nextDate = Carbon::now()->addDays($coupon[0]->valid_for_days);
                $endDate = round(strtotime($nextDate) * 1000);
                $couponData = array('user_id'=>$user_id,'coupon_id'=>$coupon[0]->coupon_id,'start_date'=>$startDate,'end_date'=>$endDate,'total_use'=>$coupon[0]->max_uses,'status'=>1);
                $copunUser = couponUserTbl::AddReferalCouponUser($couponData);
                return array('code'=>1,'result'=>$copunUser[0]);
            }else{
                return array('code'=>0,'result'=>'');
            }
        }else{
            return array('code'=>2,'result'=>'');
        }

    }
    public function findActiveCoupon($user_id, $service_id, $area_id){
        $coupon = couponTbl::findActiveCoupon($user_id);
        if(count($coupon)>0){
            $user_coupon = $coupon[0];
            if($user_coupon->is_service == 1){
                $couponDetail = couponTbl::couponWithService($service_id, $user_coupon->coupon_id);
                return $couponDetail;
            }elseif ($user_coupon->is_service_area == 1){
                $couponDetail = couponTbl::couponWithArea($area_id, $user_coupon->coupon_id);
                return $couponDetail;
            }else{
                return $coupon;
            }
        }else{
            return $coupon;
        }
    }
    public function ActivateCoupon($request){
        $coupon = $request->coupon_code;
        $user_id = $request->user_id;
        $result = couponTbl::findCoupon($user_id, $coupon);
        if(count($result)>0){
            $coupon_user_id = $result[0]->coupon_user_id;
            couponUserTbl::deactivateAllCoupons($user_id);
            $res = couponUserTbl::activateCoupons($coupon_user_id);
            if($res == 1){
                $response = $this->getEmptyResponse(0,'Coupon Activated successfully','');
                return $response;
            }
        }else{
            $result = couponTbl::getgenCoupon($coupon);
            if(count($result) > 0){
                couponUserTbl::deactivateAllCoupons($user_id);
                $couponData = array('user_id'=>$user_id,'coupon_id'=>$result[0]->coupon_id,'start_date'=>$result[0]->start_date,'end_date'=>$result[0]->end_date,'total_use'=>$result[0]->max_uses,'is_active'=>1);
                $res = couponUserTbl::AddCouponUser($couponData);
                if($res == 1){
                    $response = $this->getEmptyResponse(0,'Coupon Activated successfully','');
                    return $response;
                }
            }else{
                $response = $this->getEmptyResponse(1,'Invalid promo code','');
                return $response;
            }
        }
    }
    public function getCoupons($request){
        $user_id = $request->user_id;
        $result = couponTbl::getCoupons($user_id);
        //$generic = couponTbl::getGenericCoupon();
        if(count($result)>0){
            ///$array = array_merge($result->toArray(), $generic->toArray());
            $response = $this->getArrayResponse(0,'Coupons Found successfully',$result);
            return $response;
        }else{
            $response = $this->getArrayResponse(1,'Coupons not found','');
            return $response;
        }
    }
    public function getCouponList(){
        $resp = couponTbl::getCouponList();
        if(count($resp)>0){
            $response = $this->getArrayResponse(0,'Coupons list found successfully',$resp);
            return $response;
        }else{
            $response = $this->getArrayResponse(1,'Coupons list not found',$resp);
            return $response;
        }

    }
    public function CouponUses($user_id){
        $result = couponTbl::findActiveCoupon($user_id);
        if(count($result)>0){
            $coupon_user_id = $result[0]->coupon_user_id;
            $uses = $result[0]->uses;
            $new_uses = $uses+1;
            $result = couponUserTbl::updateUses($coupon_user_id, $new_uses);
            $response = $this->getArrayResponse(0,'Coupons Found successfully',$result);
            return $response;
        }else{
            $response = $this->getArrayResponse(1,'Coupons not found','');
            return $response;
        }
    }
    public function referalCouponActivate($job_id){
        $result = jobTbl::getCouponId($job_id);
        if($result[0]->coupon_id != null){
            $coupon_user_id = $result[0]->coupon_id;
            $result = couponUserTbl::getReferalCouponId($coupon_user_id);
            if($result[0]->referal_coupon_user_id != null){
                couponUserTbl::updateReferalCouponStatus($result[0]->referal_coupon_user_id);
            }
        }
    }
    public function saveDriverToVehicle($vehicleDriver){
        $res = driverVehicleTbl::vehicleDriverCheck($vehicleDriver['driver_id']);
        if($res->count() == 0){
            $response = driverVehicleTbl::insertion($vehicleDriver);
            if($response==1){
                return 'DriverVehicleSaved';
            }else{
                return 'ErrorSavingDriverVehicle';
            }
        }else{
            return 'exists';
        }
    }
    public function saveJob($job_data,$job_dates,$service_type){
        $is_schedule = $job_data['is_schedule'];
        $res = jobTbl::insertion($job_data);
        if($res->count()>0){
            $job_id = $res->job_id;
            foreach($job_dates as $job_date){
                $scheduleJob = array('job_id'=>$job_id,'schedule_date'=>$job_date,'job_type'=>$service_type);
                $res = scheduleJobTbl::insertion($scheduleJob);
            }
            $data = array('job_id'=>$job_id,'jobSaved'=>'jobSaved');
            return $data;
        }else{
            $data = array('jobSaved'=>'errorSavingJob');
            return $data;
        }
    }
    public function updateJob($job_data,$job_dates,$job_id,$service_type){
        $is_schedule = $job_data['is_schedule'];
        $res = jobTbl::updateJob($job_data,$job_id);
        if($res == 1 || $res == 0){
            scheduleJobTbl::deletescheduleJob($job_id);
            foreach($job_dates as $job_date){
                $scheduleJob = array('job_id'=>$job_id,'schedule_date'=>$job_date,'job_type'=>$service_type);
                $res = scheduleJobTbl::insertion($scheduleJob);
            }
            $data = array('job_id'=>$job_id,'jobSaved'=>'jobSaved');
            return $data;
        }else{
            $data = array('jobSaved'=>'errorSavingJob');
            return $data;
        }
    }
    public function assingDriverToVehiclelocal($driver_id,$vehicle_id){
        /////////////////////////// request Params //////////////////////////////////
        $vehicleDriver = array('driver_id'=>$driver_id,'vehicle_id'=>$vehicle_id);
        /////////////// assigning vehicle to driver using save driver to vehicle method //////////////////////////////
        $res = $this->saveDriverToVehicle($vehicleDriver);
        ////////////// checking response //////////////////////////////////////////////
        if($res == 'DriverVehicleSaved'){
            //////////////////// return response ////////////////////
            $response = $this->getEmptyResponse(0,'vehicle assinged to driver successfully','');
            return $response;
        }else if($res == "ErrorSavingDriverVehicle"){
            //////////////////// return response ////////////////////
            $response = $this->getEmptyResponse(1,'Error assinging vehicle to driver','');
            return $response;
        }else if($res == 'exists'){
            //////////////////// return response ////////////////////
            $response = $this->getEmptyResponse(1,'Vehicle already assigned to driver','');
            return $response;
        }

    }
    public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    public function checkOldPassword($user_id,$password){
        $res = userTbl::checkPassword($user_id,$password);
        if(count($res)>0){
            $response = 0;
            return $response;
        }else{
            $response = 1;
            return $response;
        }
    }
    public function userExists($email,$user_type){
        $res = userInfoTbl::emailCheck($email,$user_type);
        if(count($res) > 0){
            $response = $this->getEmptyResponse(1,'User already registered with this email','');
            return $response;
        }else{
            $response = $this->getEmptyResponse(0,'','');
            return $response;
        }
    }
    public function userMobileExists($mobile,$user_type){
        $res = userInfoTbl::mobileCheck($mobile,$user_type);
        if(count($res) > 0){
            $response = $this->getEmptyResponse(1,'User already registered with this mobile number','');
            return $response;
        }else{
            $response = $this->getEmptyResponse(0,'','');
            return $response;
        }
    }
    public function companyExists($comp_number){
        $res = companyTbl::compNumCheck($comp_number);
        if(count($res) > 0){
            $response = $this->getEmptyResponse(1,'User already registered with this Waste carrier license number','');
            return $response;
        }else{
            $response = $this->getEmptyResponse(0,'','');
            return $response;
        }
    }
    public function vatExists($full_vat_number){
        $res = vatInfoTbl::vatNumCheck($full_vat_number);
        if(count($res) > 0){
            $response = $this->getEmptyResponse(1,'User already registered with this vat number','');
            return $response;
        }else{
            $response = $this->getEmptyResponse(0,'','');
            return $response;
        }
    }
    public function checkMangoProviderExists($user_id){
        $resp = providerWalletTbl::selectSingle($user_id);
        if(count($resp)>0){
            if(!empty($resp[0]->provider_mango_user) && !empty($resp[0]->provider_wallet)){
                $response = $this->getObjectResponse(1, 'Both Exists', $resp[0]);
                return $response;
            }elseif(!empty($resp[0]->provider_mango_user) && empty($resp[0]->provider_wallet)){
                $response = $this->getObjectResponse(2, 'User Exists', $resp[0]);
                return $response;
            }
        }else{
            $response = $this->getEmptyResponse(0, 'both not exists', '');
            return $response;
        }
    }

///////////////////////////////// RESPONSE CREATOR METHODS ////////////////////////////////////////

    public function getEmptyResponse($code,$description,$result){
        return array('code'=>$code,'description'=>$description,'result'=>'');
    }
    public function getObjectResponse($code,$description,$result){
        if(empty($result)){
            return array('code'=>$code,'description'=>$description,'result'=>(object) array());
        }
        else{
            return array('code'=>$code,'description'=>$description,'result'=>$result);
        }

    }
    public function getArrayResponse($code,$description,$result){
        if(empty($result)){
            return array('code'=>$code,'description'=>$description,'result'=>[]);
        }else{
            return array('code'=>$code,'description'=>$description,'result'=>$result);
        }

    }
    public function getRefCoupon(){
        $resp = couponTbl::getRefCoupon();
        $response = $this->getObjectResponse(0,'coupon found',$resp);
        return $response;
    }
    public function deleteCoupon($request){
        $coupon_id = $request->coupon_id;
        $resp = couponTbl::deleteCoupon($coupon_id);
        if($resp === 1){
            $response = $this->getObjectResponse(0,'Coupon code deleted successfully',$resp);
        }else{
            $response = $this->getObjectResponse(0,'Error deleting coupon code',$resp);
        }
        return $response;
    }
    public function updateCoupon($request){
        $coupon_id = $request->coupon_id;
        $save_date = $request->save_date;
        $title = $request->title;
        $short_desc = $request->short_desc;
        $ful_desc = $request->ful_desc;
        $coupon_code = $request->coupon_code;
        $coupon_type = $request->coupon_type;
        $discount = $request->discount;
        $max_dicount = $request->max_discount;
        $max_uses = $request->max_uses;
        $valid_for_days = $request->valid_for_days;
        $media = $request->media;
        $is_fixed = $request->is_fixed;

        $today = Carbon::parse($save_date);
        $startDate = round(strtotime($today) * 1000);
        $nextDate = Carbon::parse($save_date)->addDays($valid_for_days);
        $endDate = round(strtotime($nextDate) * 1000);

        $coupon_array = array('title'=>$title,'short_desc'=>$short_desc,'ful_desc'=>$ful_desc,'coupon_code'=>$coupon_code
        ,'coupon_type'=>$coupon_type,'discount'=>$discount,'max_discount'=>$max_dicount,'max_uses'=>$max_uses,
            'valid_for_days'=>$valid_for_days,'start_date'=>$startDate,'end_date'=>$endDate,'is_fixed'=>$is_fixed);
        if($media){
            $input['imagename'] = time().'.'.$media->getClientOriginalName();
            $destinationPath = public_path('/driverDocs/driving_license');
            $media->move($destinationPath, $input['imagename']);
            $media = url('/')."/driverDocs/driving_license/".$input['imagename'];
            $coupon_array['media'] = $media;
        }

        $resp = couponTbl::updateCoupon($coupon_id,$coupon_array);
        if($resp === 1){
            $response = $this->getObjectResponse(0,'Coupon code updated successfully','');
        }else{
            $response = $this->getObjectResponse(1,'Error updating coupon code','');
        }
        return $response;
    }
}
