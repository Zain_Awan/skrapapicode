<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Cartalyst\Stripe\Api;
use Cartalyst\Stripe\Exception\CardErrorException;
use Cartalyst\Stripe\Exception\StripeException;
use Cartalyst\Stripe\Exception\MissingParameterException;

class stripeController extends Controller
{
    protected $HelperMethods;
    public function __construct(HelperMethods $HelperMethods) {
        $this->HelperMethods = $HelperMethods;
    }
    //////////////////////// Create customer on stripe ////////////////////////////
    public function createCustomer($first_name, $last_name, $email){
        try{
            $customer = Stripe::Customers()->create(array(
                "description" => $first_name.' '.$last_name,
                "email" => $email
            ));
            $response = $this->HelperMethods->getObjectResponse(0,'customer created successfully',$customer);
            return $response;
        } catch (MissingParameterException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
    }
    ///////////////////////////// create card source for customer Stripe ////////////////////////////
    public function createSource($card_number,$exp_month,$exp_year,$cvv,$name){
        try{
            $source = Stripe::Sources()->create(array(
                "type" => "card",
                "currency" => "gbp",
                "owner" => array(
                    "name" => $name
                ),
                "card" => array(
                    "number" => $card_number,
                    "exp_month" => $exp_month,
                    "exp_year" => $exp_year,
                    "cvc" => $cvv
                )
            ));
            $response = $this->HelperMethods->getObjectResponse(0,'Source created successfully',$source);
            return $response;
        } catch(MissingParameterException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        } catch (CardErrorException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
    }
    ////////////////////////// assign source to customer Stripe /////////////////////////////
    public function assignSourceCustomer($cust_id,$src_id){
        try{
            $customer = Stripe::Cards()->create($cust_id, $src_id);
            $response = $this->HelperMethods->getObjectResponse(0,'Sources Assigned successfully',$customer);
            return $response;
        }
        catch(MissingParameterException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
        catch (CardErrorException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
    }
    ///////////////////////////// create charge on customer stripe //////////////////////////////////
    public function createCharge($cust_id,$src_id,$amount){
        try{
            $charge = Stripe::Charges()->create(array(
                "amount" => $amount,
                "currency" => "gbp",
                "customer" => $cust_id,
                "source" => $src_id,
                "description" => "Skrap job charge"
            ));
            $response = $this->HelperMethods->getObjectResponse(0,'Charge created successfully',$charge);
            return $response;
        }
        catch(MissingParameterException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
        catch (CardErrorException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
    }
    /////////////////////// get Customer Detail /////////////////////
    public function getAllSources($customer_id){
        try{
            $source = Stripe::Sources()->all($customer_id);
            if(count($source['data']) == 0){
                $response = $this->HelperMethods->getObjectResponse(0,'Customerfound successfully',[]);
                return $response;
            }else{
                $response = $this->HelperMethods->getObjectResponse(0,'Customerfound successfully',$source['data']);
                return $response;
            }
        }
        catch(MissingParameterException $e){
        $msg = $e->getMessage();
        $Error = $e->getErrorCode();
        $Code = $e->getCode();
        $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
        return $response;
        }
    }

    public function getSource($source_id){
        try{
            $source = Stripe::Sources()->find($source_id);
            $response = $this->HelperMethods->getObjectResponse(0,'Customer found successfully',$source);
            return $response;
        }
        catch(MissingParameterException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
    }

    public function DetachSource($source_id,$stripeCustomerId){
        try{
            $source = Stripe::Cards()->delete($stripeCustomerId,$source_id);
            $response = $this->HelperMethods->getObjectResponse(0,'Customerfound successfully',$source);
            return $response;
        }
        catch(MissingParameterException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
        catch (CardErrorException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
        catch (StripeException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
    }
    public function stripeTest(Request $request){
        try {
            $card_number = $request->card_number;
            $exp_month = $request->exp_month;
            $exp_year = $request->exp_year;
            $cvv = $request->cvv;

            $customer_id = 'cus_CedmmZRa34A37w';
            $source = $this->createSource($card_number,$exp_month,$exp_year,$cvv);
            $source_id = $source['id'];
            $assign = $this->assignSourceCustomer($customer_id,$source_id);
            $charge = $this->createCharge($customer_id,$source_id);
            return $charge;
        } catch(MissingParameterException $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            return array('code'=>$Code,'ErrorCode'=>$Error,'Message'=>$msg);

        }
    }
    public function refundCharge($charge_id,$is_fee,$amount){
        try {
            if($is_fee == 1){
                $amount_to_refund = $amount-50;
                $refund = Stripe::Refunds()->create($charge_id,$amount_to_refund);
            }else{
                $refund = Stripe::Refunds()->create($charge_id);
            }
            $response = $this->HelperMethods->getObjectResponse(0,'Charge Refunded successfully',$refund);
            return $response;
        } catch(MissingParameterException $e){
            $msg = $e->getMessage();
            $Error = $e->getErrorCode();
            $Code = $e->getCode();
            $response = $this->HelperMethods->getEmptyResponse($Code,$msg,'');
            return $response;
        }
    }

}
