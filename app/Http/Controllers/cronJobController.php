<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FCMcontroller;
use App\rawQuery;
use App\appointmentsTbl;
use App\notificationsTbl;
use App\cronAlertTbl;
use Carbon\Carbon;

class cronJobController extends Controller
{
    public function cronJobAppointments(){
        $res = cronAlertTbl::getCronJobList();
        $array = [];
        foreach($res as $cronJobs){
            $today = Carbon::now();
            $current = strtotime($today) * 1000;
            $resp = appointmentsTbl::getCronJobData($cronJobs->alert_time,$cronJobs->alert_range,$current,$cronJobs->alert_id);
            $resp->alert_id = $cronJobs->alert_id;
            if(count($resp)>0){
                array_push($array,$resp);
                foreach($resp as $jobData){
                    $mobileTokens = $jobData->device_id;
                    $ios_ids =$jobData->ios_id;
                    $minutes = $jobData->Diffrence/60000;
                    $time_zone = 'Europe/London';
                    $time = Carbon::createFromTimestamp($minutes,$time_zone)->format('H:i');
                    $title = 'Next Job';
                    $body = 'Your next job will starts in ' .$time. ' minutes';
                    $jobId = ['appoint_id_noti'=>$jobData->appointment_id];
                    $type = 10;
                    $FCMcontroller = new FCMcontroller();
                    if(!empty($mobileTokens) && $mobileTokens != null){
                        $pushresp = $FCMcontroller->FCMSendNotifications($mobileTokens,$jobId,$title,$body,$type);
                        $notificationDetails = array('user_id'=>$jobData->user_id,'notification_type'=>$type,'notification_data'=>json_encode($jobId),'title'=>$title,'body'=>$body,'job_id'=>$jobData->job_id,'alert_id'=>$resp->alert_id);
                        notificationsTbl::insertion($notificationDetails);
                    }
                    if(!empty($ios_ids) && $ios_ids != null){
                        $pushresp2 = $FCMcontroller->FCMSendNotificationsIos($ios_ids,$jobId,$title,$body,$type);
                        $notificationDetails = array('user_id'=>$jobData->user_id,'notification_type'=>$type,'notification_data'=>json_encode($jobId),'title'=>$title,'body'=>$body,'job_id'=>$jobData->job_id,'alert_id'=>$resp->alert_id);
                        notificationsTbl::insertion($notificationDetails);
                    }
                }
            }
        }
        //$response = array(''0,'notifications send',$array);
        return array('code'=>0);
    }
}
