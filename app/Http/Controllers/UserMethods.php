<?php

namespace App\Http\Controllers;

use App\notificationsTbl;
use Illuminate\Http\Request;
use App\userTbl;
use App\providerTbl;
use App\driverTbl;
use App\userInfoTbl;
use App\driverJobAreaTbl;
use App\driverServicesTbl;
use App\providerServicesTbl;
use App\rawQuery;
use App\ratingTbl;
use App\providerBankManualTbl;
use Mail;
use Illuminate\Support\Facades\DB;

class UserMethods extends Controller
{
    protected $HelperMethods;
    protected $nodeApiController;
    protected $PaymentController;
    protected $ServicesMethods;
    protected $AuthController;
    protected $FCMcontroller;
    public function __construct(HelperMethods $HelperMethods, FCMcontroller $FCMcontroller, nodeApiController $nodeApiController, PaymentController $PaymentController, ServicesMethods $ServicesMethods, AuthController $AuthController)
    {
        $this->HelperMethods = $HelperMethods;
        $this->nodeApiController = $nodeApiController;
        $this->PaymentController = $PaymentController;
        $this->ServicesMethods = $ServicesMethods;
        $this->AuthController = $AuthController;
        $this->FCMcontroller = $FCMcontroller;
    }
    public function registerProvider($request)
    {
        // register provider with company
        $user_type = 2;
        $townAreas = explode(',', $request->townAreas);
        $services = explode(',', $request->services);
        $companyInfo = json_decode($request->companyInfo);
        $vatInfo = json_decode($request->vatInfo);
        $vat_status = $request->vat_status;
        $Address = $companyInfo->Address;
        $Applicant_type = $companyInfo->Applicant_type;
        $Business_name = $companyInfo->Business_name;
        $Postcode = $companyInfo->Postcode;
        $Registered_as = $companyInfo->Registered_as;
        $Registration_date = $companyInfo->Registration_date;
        $Registration_number = $companyInfo->Registration_number;
        $locality = $companyInfo->locality;
        $street_address = $companyInfo->street_address;
        $tier = $companyInfo->tier;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $email = $request->user_name;
        $mobile = $request->mobile;
        $dob = $request->dobp;
        $building = $request->building;
        $street = $request->street;
        $town = $request->town;
        $country = $request->country;
        $post_code = $request->postcode;
        $waste_carier_license = $request->waste_carier_license;
        $account_type = $request->account_type;
        $account_name = $request->account_name;
        $account_number = $request->account_number;
        $sort_code = $request->sort_code;

        $input['imagename'] = time().'.'.$waste_carier_license->getClientOriginalName();
        $destinationPath = public_path('/providerDocs/waste_carrier_license');
        $waste_carier_license->move($destinationPath, $input['imagename']);
        $waste_carier_licenseUrl = url('/')."/providerDocs/waste_carrier_license/".$input['imagename'];

        $districtAreas = $this->ServicesMethods->getdistrictCodes($townAreas);
        $Distarray = [];
        foreach ($districtAreas['result'] as $districs)
        {
            $districs->district_id;
            array_push($Distarray,$districs->district_id);
        }
        $exists = $this->HelperMethods->companyExists($Registration_number);
        if($exists['code'] == 1) {
            $response = $this->HelperMethods->getEmptyResponse(1, 'user already registered with this waste carrier license number', '');
            return $response;
            exit;
        }
        if($vat_status == 1){
            $vat_query =  $vatInfo->query;
            $vatExists = $this->HelperMethods->vatExists($vat_query);
            if($vatExists['code'] == 1) {
                $response = $this->HelperMethods->getEmptyResponse(1, 'user already registered with this VAT number', '');
                return $response;
                exit;
            }
        }
        // calling saveUser method and passing params
        $userRes = $this->HelperMethods->saveUser($mobile,$user_type,$email, '','',3);
        if($userRes == 'already_registered'){
            $response = array('code'=>1,'result'=>array(),'description'=>'provider already registerd');
            return $response;
            exit;
        }
        $user_id = $userRes['user_id'];
        $userInfo = array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'mobile_number'=>$mobile,'dob'=>$dob,'building'=>$building,'street'=>$street,'town'=>$town,'country'=>$country,'post_code'=>$post_code,'user_id'=>$user_id);
        // calling c method and pasing array
        $res = $this->HelperMethods->saveUserInfo($userInfo);
        // check for response
        $res = $this->HelperMethods->saveProvider($user_id,$vat_status);
        $provider_id = $res->provider_id;
        // created array to pass
        $compInfo = array( 'provider_user_id'=>$user_id, 'provider_id' => $provider_id,'Address' => $Address, 'Applicant_type' => $Applicant_type, 'Business_name' => $Business_name, 'Postcode' => $Postcode,
                'Registered_as' => $Registered_as,'Registration_date' => $Registration_date,'Registration_number' => $Registration_number,'locality' => $locality,'street_address' => $street_address,'tier' => $tier,
            'waste_license_url'=>$waste_carier_licenseUrl);
         // calling saveComapny method and passing array
        $res = $this->HelperMethods->saveCompany($compInfo);
        if($res['code'] == 1){
            $response = $this->HelperMethods->getEmptyResponse(1,'user already registered with this waste carrier license number','');
            return $response;
            exit;
        }
        if($vat_status == 1){
            $vatInfo->provider_id = $provider_id;
            $vatArr = json_decode(json_encode($vatInfo), True);
            $res = $this->HelperMethods->saveVat($vatArr);
            if($res['code'] == 1){
                $response = $this->HelperMethods->getEmptyResponse(1,'user already registered with this VAT number','');
                return $response;
                exit;
            }
        }
        $accountInfo = array( 'provider_id' => $provider_id,'account_name' => $account_name, 'account_number' => $account_number, 'sort_code' => $sort_code, 'account_type' => $account_type);
        $accResp = providerBankManualTbl::insertion($accountInfo);
        // calling saveProviderServices method and passing params
        $res1 = $this->HelperMethods->saveProviderServices($services,$provider_id);
        if($res1==1){
            $resarea = $this->HelperMethods->saveProviderServicesArea($Distarray,$provider_id);
            if($resarea == 1) {
                $payRes = $this->PaymentController->createCustomer($user_id);
                if ($payRes['code'] == 0) {
                    $mail = $email;
                    Mail::send('emails.welcome', ['data' => $userRes], function ($message) use ($mail) {
                        $message->from($_ENV['INFOEMAIL'], 'Skrap');
                        $message->to($mail)->subject('Skrap registration email');
                    });
                    $response = $this->HelperMethods->getEmptyResponse(0, 'Registration successful please check your email account for your login detail', '');
                    return $response;
                }
            }
        }
    }

    public function registerCustomer($request){
        // register customer
        $email = $request->email;
        $referal = $request->referal;
        $user_name = $request->mobile_number;
        $password = $request->password;
        $user_type = 1;
        $first_name = $request->first_name;
        $device_type = $request->device_type;
        $marketing = $request->marketing;
        // calling saveUser method and passing params
        $referalId = $this->HelperMethods->createReferalId($first_name);
        $userRes = $this->HelperMethods->saveUser($user_name,$user_type,$email,$password,$referalId,$device_type);
        if($userRes=='already_registered'){
            $response = $this->HelperMethods->getEmptyResponse(1,'user already registered','');
            return $response;
            exit;
        }
        $user_id = $userRes['user_id'];
        // user information
        $last_name = $request->last_name;
        $mobile_number = $request->mobile_number;
        $dob = $request->dob;
        $building = $request->building;
        $street = $request->street;
        $town = $request->town;
        $country = $request->country;
        $post_code = $request->post_code;
        if($marketing == true){
            $marketing = 0;
        }else{
            $marketing = 1;
        }
        // creating array for passing to saveUserInfo Method
        $userInfo = array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'dob'=>$dob,'building'=>$building,'street'=>$street,'town'=>$town,'country'=>$country,'post_code'=>$post_code,'user_id'=>$user_id,'mobile_number'=>$mobile_number,'is_messages'=>$marketing);
        // calling saveUserInfo method and pasing array
        $res = $this->HelperMethods->saveUserInfo($userInfo);
        if($res==1){
            $res = $this->PaymentController->createCustomer($user_id);
            if($res['code'] == 0 || $res['code'] == 1){
                $text = 'Welcome to Skrap! You have successfully registered and can order immediately. For any issue with your delivery contact Skrap 0330 133 1561.';
                $body = array('to'=>$mobile_number, 'text'=> $text);
                $this->nodeApiController->sendSms($body);
                $mail = $email;
                Mail::send('emails.welcome',['data'=>$userRes], function ($message) use ($mail){
                    $message->from($_ENV['INFOEMAIL'], 'Skrap');
                    $message->to($mail)->subject('Skrap registration email');
                });
                Mail::send('emails.customerRegister',['data'=>$userRes,'email'=>$email,'mobile_number'=>$mobile_number], function ($message) use ($mail){
                    $message->from($_ENV['INFOEMAIL'], 'Skrap');
                    $message->to($_ENV['ADMINEMAIL'])->subject('Skrap registration email');
                });
                $resp = $this->AuthController->register($mobile_number,$request->password);
                $tokenResp = $resp;
                $token = $tokenResp['success']['token'];
                $personal = array('first_name'=>$first_name,'last_name'=>$last_name,'mobile_number'=>$mobile_number,'email'=>$email);
                $data_array = array('user_id'=>$user_id,'user_type'=>$user_type,'otp'=>$userRes['otp'],'referal_id'=>$userRes['referal_id'],'personal_detail'=>$personal,'token'=>$token);
                $coupon1 = 0;
                $couponCode = 1;
                if(!empty($referal)){
                    $referal_user = userTbl::getIdByReferal($referal);
                    if(count($referal_user)>0){
                        $referal_user_id = $referal_user[0]->user_id;
                        $coupon = $this->HelperMethods->AssignCouponReferal($referal_user_id, 2);
                        if($coupon['code'] == 1){
                            $coupon_user_id = $coupon['result']->coupon_user_id;
                            $this->HelperMethods->AssignCoupon($user_id, 3, $referal_user_id, $coupon_user_id);
                        }
                    }
                }
                if($coupon1 == 0){
                    $response = $this->HelperMethods->getObjectResponse(0,'customer registered successfully',$data_array);
                    return $response;
                }else{
                    $response = $this->HelperMethods->getObjectResponse(3,'customer registered without token',$data_array);
                    return $response;
                }
            }else{
                $response = $this->HelperMethods->getObjectResponse(2,'Stripe customer creation failed',$res);
                return $response;
            }
        }
        else{
            $response = $this->HelperMethods->getEmptyResponse(4,'Error Registering user','');
            return $response;
        }
    }

    public function login($request){
        // requesting form data
        $username = $request->user_name;
        $password = base64_encode($request->password);
        $user_type = $request->user_type;
        // calling userTbl model selectSingle method for finding user record
        $res = userTbl::selectSingle($username,$password,$user_type);
        // if record found
        if(count($res)>0){
            $resp = $this->AuthController->getToken($username,$request->password);
            if($resp['success'] == 1){
                $resp = $this->AuthController->register($username,$request->password);
                $tokenResp = $resp;
            }else{
                $tokenResp = $resp;
            }
            $user_id = $res[0]->user_id;
            $user_type = $res[0]->user_type;
            $referal_id = $res[0]->referal_id;
            $result = userInfoTbl::selectSingle($user_id);
            if(count($result)>0){
                if($user_type == 3){
                    $data = array('online_status'=>1);
                    ///////////// updating status using driverTbl Model ///////////////////////
                    $res1 = driverTbl::updateDriverOnlineStatus($user_id,$data);
                }
                $token = $tokenResp['success']['token'];
                $data_array = array('user_id'=>$user_id,'user_type'=>$user_type,'otp'=>$res[0]->otp,'referal_id'=>$referal_id,'personal_detail'=>$result[0],'token'=>$token);
                $response = $this->HelperMethods->getObjectResponse(0,'Login successful',$data_array);
                // resturns respose of the request

                return $response;
            }
        }
        // if record not found
        else{
            $response = $this->HelperMethods->getObjectResponse(1,'Email or password is invalid','');
            // resturns respose of the request
            return $response;
        }
    }

    public function addDriver($request)
    {
        /////////////////// request params ///////////////////////////////
        $user_name = $request->mobile_number;
        $email = $request->email;
        $user_type = 3;
        $provider_user_id = $request->provider_user_id;
        ///////////////////////// calling save user Method to save user ////////////////////////////
        $userRes = $this->HelperMethods->saveUser($user_name,$user_type,$email,'','','');
        ///////////////////////// checking Response ////////////////////////////////
        if($userRes=='already_registered'){
            ///////////////////// return response //////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'user already registered','');
            return $response;
            exit;
        }
        /////////////////// get provider id using getProvderId Method ///////////////////////////////////////
        $res = providerTbl::getProviderId($provider_user_id);
        /////////////////// request Params //////////////////////
        $provider_id = $res[0]->provider_id;
        $user_id = $userRes['user_id'];
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $dob = $request->dob;
        $building = $request->building;
        $street = $request->street;
        $town = $request->town;
        $country = $request->country;
        $post_code = $request->post_code;
        $mobile_number = $request->mobile_number;
        /*$waste_carier_license = $request->waste_carier_license;*/
        $driving_license = $request->driving_license;
        $insurance_cert = $request->insurance_cert;
        $recent_pic = $request->recent_pic;
        $services = $request->services;
        $vehicle_id = $request->vehicle_id;
        /////////////////////////////// uploading Image ////////////////////////////////////
        /*$input['imagename'] = time().'.'.$waste_carier_license->getClientOriginalName();
        $destinationPath = public_path('/driverDocs/waste_carrier_license');
        $waste_carier_license->move($destinationPath, $input['imagename']);
        $waste_carier_license = url('/')."/driverDocs/waste_carrier_license/".$input['imagename'];*/
        /////////////////////////////// uploading Image ////////////////////////////////////
        $input['imagename'] = time().'.'.$driving_license->getClientOriginalName();
        $destinationPath = public_path('/driverDocs/driving_license');
        $driving_license->move($destinationPath, $input['imagename']);
        $driving_license = url('/')."/driverDocs/driving_license/".$input['imagename'];
        /////////////////////////////// uploading Image ////////////////////////////////////
        $input['imagename'] = time().'.'.$insurance_cert->getClientOriginalName();
        $destinationPath = public_path('/driverDocs/insurance_cert');
        $insurance_cert->move($destinationPath, $input['imagename']);
        $insurance_cert = url('/')."/driverDocs/insurance_cert/".$input['imagename'];
        /////////////////////////////// uploading Image ////////////////////////////////////
        $input['imagename'] = time().'.'.$recent_pic->getClientOriginalName();
        $destinationPath = public_path('/driverDocs/recent_pic');
        $recent_pic->move($destinationPath, $input['imagename']);
        $recent_pic = url('/')."/driverDocs/recent_pic/".$input['imagename'];
        //////////////// creating array to insert data //////////////////////////////
        $userInfo = array('user_id'=>$user_id,'first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'dob'=>$dob,'building'=>$building,'street'=>$street,'town'=>$town,'country'=>$country,'post_code'=>$post_code,'mobile_number'=>$mobile_number);
        //////////////// creating array to insert data //////////////////////////////
        $driverDetail = array('provider_id'=>$provider_id,'user_id'=>$user_id,'driving_license'=>$driving_license,'insurance_cert'=>$insurance_cert,'recent_pic'=>$recent_pic);
        //////////////// inserting data using SaveUserInfo Method //////////////////////////////
        $infoRes = $this->HelperMethods->saveUserInfo($userInfo);
        //////////////// inserting data using saveDriver Method //////////////////////////////
        $driverRes = $this->HelperMethods->saveDriver($driverDetail);
        $driver_id = $driverRes->driver_id;
        //////////////// inserting data using saveDriverServices Method //////////////////////////////
        $driveServRes = $this->HelperMethods->saveDriverServices($services,$driver_id);
        //////////////// inserting data using assingDriverToVehiclelocal Method //////////////////////////////
        if(!empty($vehicle_id)){$this->HelperMethods->assingDriverToVehiclelocal($driver_id,$vehicle_id);}
        ///////////////////////// calling getProviderServiceAreas Method to get service areas ////////////////////////////
        $providerAreas = $this->ServicesMethods->getProviderdistrictAreas($provider_user_id);
        if($providerAreas['code'] == 0){
            $providerAreas = $this->ServicesMethods->saveDriverServiceAreas($providerAreas['result'],$driver_id);
        }
        /////////////////////////////// checking Response //////////////////////////////////
        if($infoRes==1 && $driveServRes==1){
            $mail = $email;
            ////////////////// sending Registration email //////////////////////////
            Mail::send('emails.welcome',['data'=>$userRes], function ($message) use ($mail){
                $message->from($_ENV['INFOEMAIL'], 'Skrap');
                $message->to($mail)->subject('Skrap registration email');
            });
            ///////////////////// return Response //////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Driver registerd successfully','');
            return $response;
        }
        else{
            ///////////////////// return Response //////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error adding driver','');
            return $response;
        }
    }

    public function updateDriverOnlineStatus($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $user_id = $request->user_id;
        $online_status = $request->online_status;
        $data = array('online_status'=>$online_status);
        /////////////////////////// updating driver status on NodeJs Server /////////////////////////////////////
        //$result = $this->nodeApiController->updateDriverStatus($user_id,$online_status);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        //if($result['StatusCode'] == '200' && !empty($result['result']->code == 0))
        //{
            ///////////// updating status using driverTbl Model ///////////////////////
            $res = driverTbl::updateDriverOnlineStatus($user_id,$data);
            ///////////////////////// checking response //////////////////////////////////////////////////////
            if($res == 1){
                ///////////////////////// return response //////////////////////////////////////////////////////
                $response = $this->HelperMethods->getEmptyResponse(0,'driver online status updated successfully','');
                return $response;
            }else{
                ///////////////////////// return response //////////////////////////////////////////////////////
                $response = $this->HelperMethods->getEmptyResponse(1,'Error updating driver online status','');
                return $response;
            }
        //}else{
            ///////////////////////// return response //////////////////////////////////////////////////////
            //$response = $this->HelperMethods->getEmptyResponse(1,'Error updating driver online status','');
            //return $response;
        //}
    }

    public function getDrversListing($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $provider_id = $request->user_id;
        ////////////////////////// selecting drivers data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getDriversListing($provider_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        $driversData = array();
        if($resultquery->count()>0){
            $driver_ids = $resultquery->pluck('user_id')->values()->toArray();
            //$drivers_location = $this->nodeApiController->getProviderDriversLocation($driver_ids);
            $drivers_location = [];
            foreach($resultquery as $drivers){
                $driver_id = $drivers->user_id;
                $driverLocation = (object) array();
                foreach($drivers_location as $driverLoc){
                    if($driver_id == $driverLoc->driver_id){
                        $driverLocation = $driverLoc;
                    }
                }
                $array = json_decode(json_encode($drivers), True);
                $personal_detail = array_slice($array,0,5);
                $driver_detail = array_slice($array,5,9);
                $vehicle_detail = array_slice($array,14,3);
                $data = array('personal_detail'=>$personal_detail,'driver_detail'=>$driver_detail,'vehicle_detail'=>$vehicle_detail,'Location'=>$driverLocation);
                array_push($driversData, $data);
            }
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(0,'Drivers found successfully',$driversData);
            return $response;
        }else{
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getArrayResponse(1,'Drivers not found','');
            return $response;
        }
    }

    public function getDriver($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $driver_id = $request->user_id;
        ////////////////////////// selecting drivers data using rawQuery model ////////////////////////////////////
        $resultquery = rawQuery::getDriver($driver_id);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($resultquery->count()>0){
            $driver_services = rawQuery::getDriverServices($driver_id);
            //$driver_location = $this->nodeApiController->getProviderDriverLocation($resultquery[0]->user_id);
            $driver_location = '';
            $array = json_decode(json_encode($resultquery[0]), True);
            $personal_detail = array_slice($array,0,11);
            $driver_detail = array_slice($array,11,8);
            if($array['is_delete'] == 1){
                $vehicle_detail = (object) array();
            }
            else{
                $vehicle_detail = array_slice($array,18,8);
            }
            $driversData = array('personal_detail'=>$personal_detail,'driver_detail'=>$driver_detail,'driver_services'=>$driver_services,'vehicle_detail'=>$vehicle_detail,'Location'=>$driver_location);
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0,'Driver found successfully',$driversData);
            return $response;
        }else{
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getObjectResponse(1,'Driver not found','');
            return $response;
        }
    }

    public function updateDeviceId($request){
        /////////////////////////// getting request data /////////////////////////////////////
        $device_id = $request->device_id;
        $web_token = $request->web_token;
        $user_id = $request->user_id;
        $ios_id = $request->ios_id;
        if(!empty($web_token)){
            $data = array('web_token'=>$web_token);
        }elseif(!empty($device_id)){
            $data = array('device_id'=>$device_id,'ios_id'=>null);
        }else{
            $data = array('ios_id'=>$ios_id,'device_id'=>null);
        }
        /////////////////////////// upadating device id using userTbl model /////////////////////////////////////
        $res = userTbl::updateDeviceId($user_id,$data);
        ///////////////////////// checking response //////////////////////////////////////////////////////
        if($res == 1){
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'device id updated successfully','');
            return $response;
        }else{
            ///////////////////////// return response //////////////////////////////////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error updating device id','');
            return $response;
        }
    }

//////////////////// GET USER ONLINE STATUS METHOD ////////////////////////////

    public function getDriverOnlineStatus($user_id){
        ///////////////// get online status of the driver using drievrTbl Model //////////////////////
        $res = driverTbl::getDriverOnlineStatus($user_id);
        ///////////////////// checking response ///////////////////////
        if(count($res)>0){
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0,'successful',$res[0]);
            return $response;
        }else{
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getObjectResponse(1,'error getting status','');
            return $response;
        }
    }

///////////////////////// CHANGE PASSWORD SERVICE METHOD //////////////////////////

    public function changePassword($request){
        /////////////////// Request parameters and encrypting password ////////////////////////
        $old_password =  base64_encode($request->old_password);
        $new_password = base64_encode($request->new_password);
        $user_id = $request->user_id;
        $data = array('user_pwd'=>$new_password,'otp'=>0);
        /////////////////// check if the old password matches with given ////////////////////////
        $checkPassword = $this->HelperMethods->checkOldPassword($user_id, $old_password);
        /////////////////// if password dose not matches ////////////////////////
        if($checkPassword == 1){
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Password you entered dose not match the old password','');
            return $response;
            /////////////////// if password matches ////////////////////////
        }else{
            /////////////////// change user password using userTbl model////////////////////////
            $res = userTbl::changePassword($data, $user_id);
            /////////////////// if password changed ////////////////////////
            if($res == 1){
                /////////////////// return response ////////////////////////
                $response = $this->HelperMethods->getEmptyResponse(0,'Password changed successfully','');
                return $response;
            }else{
                /////////////////// return response ////////////////////////
                $response = $this->HelperMethods->getEmptyResponse(1,'Error changing password','');
                return $response;
            }
        }

    }

///////////////////////// FORGOT PASSWORD SERVICE METHOD //////////////////////////

    public function forgotPassword($request){
        /////////////////// Request parameters ////////////////////////
        $email =  $request->email;
        $user_type =  $request->user_type;
        /////////////////// check if user is registered or not ////////////////////////
        $checkEmail = userInfoTbl::emailCheck($email,$user_type);
        /////////////////// if user is not registered ////////////////////////
        if($checkEmail->count() == 0){
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'No account registered with this email','');
            return $response;
            /////////////////// if user is registered ////////////////////////
        }else{
            $user_id = $checkEmail[0]->user_id;
            /////////////////// get password of the registered user using userTbl Model ////////////////////////
            $res = userTbl::getPassword($user_id);
            /////////////////// decoding the encrypted password ////////////////////////
            $user_pwd = base64_decode($res[0]->user_pwd);
            $mail = $email;
            /////////////////// sending password to the user by email ////////////////////////
            Mail::send('emails.forgotPass',['data'=>$user_pwd], function ($message) use ($mail){
                $message->from($_ENV['INFOEMAIL'], 'Skrap');
                $message->to($mail)->subject('Skrap registration email');
            });
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'We have sent you the email with your password. ThankYou!','');
            return $response;
        }
    }

/////////////////////////////////// ADD DRIVER JOB AREA /////////////////////////////

    public function addDriverJobArea($request){
        /////////////////// Request Parameters ////////////////////////
        $driver_user_id = $request->driver_user_id;
        $zip_code = $request->zip_code;
        $lat = $request->lat;
        $lng = $request->lng;
        $radius = $request->radius;
        /////////////////// array to insert data ////////////////////////
        $jobArea = array('driver_user_id'=>$driver_user_id,'zip_code'=>$zip_code,'lat'=>$lat,'lng'=>$lng,'radius'=>$radius);
        /////////////////// inserting data using driverJobAreaTbl Model ////////////////////////
        $res = driverJobAreaTbl::insertion($jobArea);
        /////////////////// if data inserted ////////////////////////
        if($res == 1){
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'Job area added successfully','');
            return $response;
        }else{
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(1,'Error adding Job area','');
            return $response;
        }
    }

////////////////////////////// GET USER PROFILE METHOD ///////////////////////

    public function getUserProfile($request){
        /////////////////// Request Parameters ////////////////////////
        $user_id = $request->user_id;
        /////////////////// get user profile using rawQuery model ////////////////////////
        $res = rawQuery::getUserProfile($user_id);
        /////////////////// if response has data ////////////////////////
        if(count($res)>0){
            /////////////////// return response ////////////////////////
            $res[0]->profile_pic_android = $res[0]->profile_pic;
            $response = $this->HelperMethods->getObjectResponse(0,'User Profile found successfully',$res[0]);
            return $response;
        }
        else{
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0,'User Profile not found','');
            return $response;
        }
    }
/////////////////// UPDATE USER PROFILE SERVICE METHOD ////////////////////////
    public function updateUserProfile($request){
        /////////////////// Request Parameters ////////////////////////
        $user_id = $request->user_id;
        $first_name = $request->first_name;
        $last_name= $request->last_name;
        $mobile_number= $request->mobile_number;
        $dob= $request->dob;
        $building= $request->building;
        $street= $request->street;
        $town= $request->town;
        $country= $request->country;
        $post_code= $request->post_code;
        $profile_pic = $request->profile_pic;
        if($request->profile_pic_android){
            if(!empty($request->profile_pic_android)){
                $img = $request->profile_pic_android;
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $data = base64_decode($img);
                $image_name = uniqid().'.png';
                $destinationPath = public_path('/profile_pics/'.$image_name);
                file_put_contents($destinationPath, $data);
                $profile_pic = url('/')."/profile_pics/".$image_name;
                /////////////////// Array to update data ////////////////////////
                $user_profile = array('first_name'=>$first_name,'last_name'=>$last_name,'mobile_number'=>$mobile_number,'dob'=>$dob,'building'=>$building,'street'=>$street,'town'=>$town,'country'=>$country,'post_code'=>$post_code,'profile_pic'=>$profile_pic);
            }else{
                /////////////////// Array to update data ////////////////////////
                $user_profile = array('first_name'=>$first_name,'last_name'=>$last_name,'mobile_number'=>$mobile_number,'dob'=>$dob,'building'=>$building,'street'=>$street,'town'=>$town,'country'=>$country,'post_code'=>$post_code);

            }
        }
        else{
            if(!empty($profile_pic)){
                $input['imagename'] = time().'.'.$profile_pic->getClientOriginalName();
                $destinationPath = public_path('/profile_pics');
                $profile_pic->move($destinationPath, $input['imagename']);
                $profile_pic = url('/')."/profile_pics/".$input['imagename'];
                /////////////////// Array to update data ////////////////////////
                $user_profile = array('first_name'=>$first_name,'last_name'=>$last_name,'mobile_number'=>$mobile_number,'dob'=>$dob,'building'=>$building,'street'=>$street,'town'=>$town,'country'=>$country,'post_code'=>$post_code,'profile_pic'=>$profile_pic);
            }else{
                /////////////////// Array to update data ////////////////////////
                $user_profile = array('first_name'=>$first_name,'last_name'=>$last_name,'mobile_number'=>$mobile_number,'dob'=>$dob,'building'=>$building,'street'=>$street,'town'=>$town,'country'=>$country,'post_code'=>$post_code);

            }
        }
       /////////////////// updating data using userInfoTbl Model ////////////////////////
        $res = userInfoTbl::updateAddress($user_profile,$user_id);
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getEmptyResponse(0,'User profile updated successfully','');
            return $response;
    }
/////////////////// GET COMPANY INFO SERVICE METHOD ////////////////////////
    public function getCompnyInfo($request)
    {
        /////////////////// Request Parameters ////////////////////////
        $companyNumber = $request->company_number;
        /////////////////// calling get compny info method ////////////////////////
        $res = $this->nodeApiController->getCompnyInfo($companyNumber);
        /////////////////// if response has result ////////////////////////
        if (count($res['result']) >0){
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0, 'Company Info Found', $res);
        }else{
            /////////////////// return response ////////////////////////
            $response = $this->HelperMethods->getObjectResponse(0, 'Company Info not Found', '');
        }
        return $response;
    }

    public function updateDriverInfo($request){
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $dob = $request->dob;
        $building = $request->building;
        $street = $request->street;
        $town = $request->town;
        $country = $request->country;
        $post_code = $request->post_code;
        $mobile_number = $request->mobile_number;
        $waste_carier_license = $request->waste_carier_license;
        $driving_license = $request->driving_license;
        $insurance_cert = $request->insurance_cert;
        $recent_pic = $request->recent_pic;
        $user_id = $request->user_id;
        $updateDriverDetail = array();
        /////////////////////////////// uploading Image ////////////////////////////////////
        if(!empty($waste_carier_license)){
            $input['imagename'] = time().'.'.$waste_carier_license->getClientOriginalName();
            $destinationPath = public_path('/driverDocs/waste_carrier_license');
            $waste_carier_license->move($destinationPath, $input['imagename']);
            $waste_carier_licenseu = url('/')."/driverDocs/waste_carrier_license/".$input['imagename'];
            $updateDriverDetail['waste_carier_license']=$waste_carier_licenseu;
        }
        if(!empty($driving_license)) {
            /////////////////////////////// uploading Image ////////////////////////////////////
            $input['imagename'] = time() . '.' . $driving_license->getClientOriginalName();
            $destinationPath = public_path('/driverDocs/driving_license');
            $driving_license->move($destinationPath, $input['imagename']);
            $driving_licenseu = url('/') . "/driverDocs/driving_license/" . $input['imagename'];
            $updateDriverDetail['driving_license']=$driving_licenseu;
        }
        if(!empty($insurance_cert)) {
            /////////////////////////////// uploading Image ////////////////////////////////////
            $input['imagename'] = time() . '.' . $insurance_cert->getClientOriginalName();
            $destinationPath = public_path('/driverDocs/insurance_cert');
            $insurance_cert->move($destinationPath, $input['imagename']);
            $insurance_certu = url('/') . "/driverDocs/insurance_cert/" . $input['imagename'];
            $updateDriverDetail['insurance_cert']=$insurance_certu;
        }
        if(!empty($recent_pic)) {
            /////////////////////////////// uploading Image ////////////////////////////////////
            $input['imagename'] = time() . '.' . $recent_pic->getClientOriginalName();
            $destinationPath = public_path('/driverDocs/recent_pic');
            $recent_pic->move($destinationPath, $input['imagename']);
            $recent_picu = url('/') . "/driverDocs/recent_pic/" . $input['imagename'];
            $updateDriverDetail['recent_pic']=$recent_picu;
        }
        //////////////// creating array to insert data //////////////////////////////
        $updateUserInfo = array('first_name'=>$first_name,'last_name'=>$last_name,'dob'=>$dob,'building'=>$building,'street'=>$street,'town'=>$town,'country'=>$country,'post_code'=>$post_code,'mobile_number'=>$mobile_number);
        //////////////// creating array to insert data //////////////////////////////
        if(count($updateDriverDetail)>0) {
           $res = driverTbl::updateDriverInfo($updateDriverDetail,$user_id);
        }
        $res2 = userInfoTbl::updateAddress($updateUserInfo,$user_id);
        $response = $this->HelperMethods->getEmptyResponse(0, 'Driver detail updated successfully','');
        return $response;
    }

    public function giveUserRating($request){
        $user_id = $request->rating_to_id;
        $ratingby_id = $request->rating_by_id;
        $rating = $request->rating;
        $reviews = $request->reviews;
        $appointment_id = $request->appointment_id;

        $rating_arr = array('rating'=>$rating,'user_id'=>$user_id,'ratingby_id'=>$ratingby_id,'reviews'=>$reviews,'appointment_id'=>$appointment_id);
        $res = ratingTbl::insertion($rating_arr);
        if($res == 1){
            $response = $this->HelperMethods->getEmptyResponse(0,'Rating added successfully','');
            return $response;
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1,'Error adding rating','');
            return $response;
        }
    }

    public function getProviderListing($request){
        $verified_status = $request->verified_status;
        $providers = rawQuery::getProviderListing($verified_status);
        if(count($providers)>0){
            $response = $this->HelperMethods->getArrayResponse(0,'Providers found successfully',$providers);
            return $response;
        }else{
            $response = $this->HelperMethods->getArrayResponse(1,'Providers data not found','');
            return $response;
        }
    }

    public function verifyProvider($request){
        $user_id = $request->user_id;
        $verify = $request->verify;
        $providers = userTbl::verifyProvider($user_id,$verify);
        if($providers == 1){
            $response = $this->HelperMethods->getEmptyResponse(0,'Provider activated successfully','');
            return $response;
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1,'Error activating provider','');
            return $response;
        }
    }

    public function getProviderDetail($request){
        $provider_user_id = $request->provider_user_id;
        $provider = rawQuery::getProviderDetail($provider_user_id);
        $providerServices = rawQuery::getProviderServices($provider_user_id);
        $providerServicesAreas = rawQuery::getProviderServiceAreas($provider_user_id);
        $providerDetail = array('provider'=>$provider[0],'providerServices'=>$providerServices,'providerServicesAreas'=>$providerServicesAreas);
        if(count($provider)>0){
            $response = $this->HelperMethods->getObjectResponse(0,'Provider found successfully',$providerDetail);
            return $response;
        }else{
            $response = $this->HelperMethods->getObjectResponse(1,'Provider data not found','');
            return $response;
        }
    }
    public function changeDriverJobReceiveStatus($request){
        $driver_id = $request->driver_id;
        $status = $request->status;
        $res = driverTbl::updateDriverJobReciveStatus($driver_id,$status);
        if($res == 1){
            $response = $this->HelperMethods->getEmptyResponse(0,'Driver job receiving status changed successfully','');
        }else{
            $response = $this->HelperMethods->getEmptyResponse(1,'Error changing driver job receiving status','');
        }
        return $response;
    }

    public function getProviderInstances($request){
        $user_id = $request->user_id;
        $number_of_drivers = 0;$number_of_vehicles = 0;$total_jobs = 0;$current_job = 0;$completed_jobs = 0;$cancelled_jobs = 0;
        $total_earnings = 0;$pending_payments = 0;$processed_payments = 0;
        $number_of_drivers = providerTbl::getDriversCount($user_id);
        $number_of_vehicles = providerTbl::getVehiclesCount($user_id);
        $total_jobs = providerTbl::getJobsCount($user_id);
        $current_job = providerTbl::getCurrentJobsCount($user_id);
        $completed_jobs = providerTbl::getCompletedJobsCount($user_id);
        $cancelled_jobs = providerTbl::getCancelledJobsCount($user_id);
        $total_earnings = providerTbl::getTotalEarnings($user_id);
        $processed_payments = providerTbl::getProcessedEarnings($user_id);
        $pending_payments = providerTbl::getPendingEarnings($user_id);
        $cancelled_payments = providerTbl::getCancelledEarnings($user_id);
        $instances = array('number_of_drivers'=>$number_of_drivers,'number_of_vehicles'=>$number_of_vehicles,'total_jobs'=>$total_jobs,
            'current_jobs'=>$current_job,'completed_jobs'=>$completed_jobs,'cancelled_jobs'=>$cancelled_jobs,
            'total_earnings'=>$total_earnings[0]->transactionCost,'processed_payments'=>$processed_payments[0]->transactionCost,
            'pending_payments'=>$pending_payments[0]->transactionCost, 'cancelled_payments'=>$cancelled_payments[0]->transactionCost);
        $response = $this->HelperMethods->getObjectResponse(0,'Provider Instance found Successfully',$instances);
        return $response;
    }
    public function getCustomerListing($request){
        $filter = $request->filter;
        $customers = userTbl::getCustomersListing();
        if(count($customers)>0){
            $response = $this->HelperMethods->getArrayResponse(0,'Customers found Successfully',$customers);
        }else{
            $response = $this->HelperMethods->getArrayResponse(1,'Customers not found','');
        }
        return $response;
    }

    public function DeleteUser($user_id,$mobile_number){
        $mobile = $mobile_number.'00';
        $resp = userTbl::DeleteUser($user_id,$mobile);
        $response = $this->HelperMethods->getEmptyResponse(0,'Customers deleted','');
        return $response;
    }
    public function getUserCredentials($request){
        $user_name = $request->mobile_number;
        $user_type = 1;
        $credentials = userTbl::getCredentials($user_name,$user_type);
        if(count($credentials)>0){
            $response = $this->HelperMethods->getObjectResponse(0,'customer found',$credentials[0]);
            return $response;
        } else {
            $response = $this->HelperMethods->getObjectResponse(1,'customer not found','');
            return $response;
        }
    }

    public function sendMarketingMsg($request){
        $title = $request->title;
        $text = $request->text;
        $is_sms = $request->is_sms;
        $is_noti = $request->is_noti;
        if($is_sms == true){
            $is_sms = 1;
        }else{
            $is_sms = 0;
        }
        if($is_noti == true){
            $is_noti = 1;
        }else{
            $is_noti = 0;
        }
        $getMarketingUsers = userTbl::getMarketingUsers();
        if(count($getMarketingUsers) > 0){
            $device_ids = $getMarketingUsers->pluck('device_id')->values()->toArray();
            $ios_ids = $getMarketingUsers->pluck('ios_id')->values()->toArray();
            $mobile_number = $getMarketingUsers->pluck('mobile_number')->values()->toArray();
            $type = 15;
            $pushresp = [];
            $pushresp2 = [];
            $smsResp = [];
            $noti_data = array('notification_type'=>$type, 'notification_data'=>'{}', 'title'=>$title, 'body'=>$text, 'is_noti'=>$is_noti, 'is_sms'=>$is_sms);
            $notiResp = notificationsTbl::insertion($noti_data);
            try{
                if($is_noti == 1){
                    $data = array('notification_id'=>$notiResp, 'title'=> $title, 'body'=>$text);
                    $pushresp = $this->FCMcontroller->FCMSendNotifications($device_ids, $data, $title, $text, $type);
                    $pushresp2 = $this->FCMcontroller->FCMSendNotificationsIos($ios_ids, $data, $title, $text, $type);
                }if ($is_sms == 1){
                    $smsArray = array('from'=> 'Skrap Team', 'to'=> $mobile_number, 'text'=> $text);
                    $smsResp = $this->nodeApiController->multipleSms($smsArray);
                }
                $resp = array('android'=>$pushresp, 'ios'=>$pushresp2, 'sms'=>$smsResp);
                if(($pushresp || $pushresp2) || $smsResp){
                    return $response = $this->HelperMethods->getArrayResponse(0,'Message sent successfully',$resp);
                }else{
                    return $response = $this->HelperMethods->getArrayResponse(0,'Error sending Message',$resp);
                }
            }
            catch (Exception $e){
                return $response = $this->HelperMethods->getArrayResponse(2,'Error sending notifications',$e);
            }
        }
    }
    public function enableMarketingMsg($request){
        $user_id = $request->user_id;
        $status = $request->status;

        $resp = userInfoTbl::enableMarketingMsg($user_id,$status);
        if($resp == 1){
            return $response = $this->HelperMethods->getEmptyResponse(0,'Status updated successfully','');
        }else{
            return $response = $this->HelperMethods->getEmptyResponse(1,'Error updating status','');
        }
    }
}
