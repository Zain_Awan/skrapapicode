<?php

namespace App\Http\Controllers;

use App\rawQuery;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientErrorResponseException;
use Carbon\Carbon;

class nodeApiController extends Controller
{
    public $endurl;

    public function __construct()
    {
        $this->endurl = "http://35.178.68.114/staging/";
        //$this->endurl = "http://192.168.10.117:3000/scrapapi/";
    }

    /*public function findDrivers($lat,$lng){
        $data = array("lng"=>$lng,"lat"=>$lat);
        $client = new Client();
        $res = $client->request('POST',$this->endurl.'findDrivers',array(
        'form_params' => $data));
        $StatusCode = $res->getStatusCode();
        $res->getHeader('content-type');
        $resdata = '';
        if($StatusCode == '200'){
            $resp = $res->getBody()->getContents();
            $resdata = json_decode($resp);
        }
        $response = array('StatusCode'=>$StatusCode,'result'=>$resdata);
        return $response;  
    }*/
    public function findDrivers($post_code)
    {
        $response = rawQuery::findDrivers($post_code);
        return $response;
    }

    public function getProviderDriversLocation($driver_ids)
    {
        $request = array('driver_ids' => $driver_ids);
        $client = new Client();
        $res = $client->request('POST', $this->endurl . 'getDriversLocation', array(
            'form_params' => $request));
        $StatusCode = $res->getStatusCode();
        $res->getHeader('content-type');
        $resdata = '';
        if ($StatusCode == '200') {
            $resp = $res->getBody()->getContents();
            $resdata = json_decode($resp);
        }
        //$response = array('StatusCode'=>$StatusCode,'result'=>$resdata);
        return (array)$resdata->result;
    }

    public function getProviderDriverLocation($driver_id)
    {
        $client = new Client();
        $res = $client->request('GET', $this->endurl . 'getDriverLocation/' . $driver_id);
        $StatusCode = $res->getStatusCode();
        $res->getHeader('content-type');
        $resdata = '';
        if ($StatusCode == '200') {
            $resp = $res->getBody()->getContents();
            $resdata = json_decode($resp);
        }
        //$response = array('StatusCode'=>$StatusCode,'result'=>$resdata);
        return (array)$resdata->result;
    }

    public function updateDriverStatus($user_id, $online_status)
    {
        $data = array("user_id" => $user_id, "online_status" => $online_status);
        $client = new Client();
        $res = $client->request('POST', $this->endurl . 'updateDriverStatus', array(
            'form_params' => $data));
        $StatusCode = $res->getStatusCode();
        $res->getHeader('content-type');
        $resdata = '';
        if ($StatusCode == '200') {
            $resp = $res->getBody()->getContents();
            $resdata = json_decode($resp);
        }
        $response = array('StatusCode' => $StatusCode, 'result' => $resdata);
        return $response;
    }

    public function getCompnyInfo($companyNumber)
    {
        $url = 'http://environment.data.gov.uk/public-register/waste-carriers-brokers/registration/' . $companyNumber . '.json';
        $client = new Client();
        // Get the actual response without headers
        try {
            $request = $client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json'
                ]
            ]);
            $StatusCode = $request->getStatusCode();
            //$type = $request->getHeader('content-type');
            if ($StatusCode == '200') {
                $res = $request->getBody()->getContents();
                $resdata = json_decode($res);
                $applicant_type = $resdata->items[0]->applicantType->label;
                $address = $resdata->items[0]->holder->hasSite->siteAddress->extended_address;
                $locality = $resdata->items[0]->holder->hasSite->siteAddress->locality;
                $organization_name = $resdata->items[0]->holder->hasSite->siteAddress->organization_name;
                $postal_code = $resdata->items[0]->holder->hasSite->siteAddress->postal_code;
                $street_address = $resdata->items[0]->holder->hasSite->siteAddress->street_address;
                $registrationDate = $resdata->items[0]->registrationDate;
                $registrationNumber = $resdata->items[0]->registrationNumber;
                $registrationType = $resdata->items[0]->registrationType->label;
                $tier = $resdata->items[0]->tier->label;
                $companyInfo = array('Registration_number' => $registrationNumber, 'Business_name' => $organization_name, 'Registered_as' => $registrationType, 'tier' => $tier, 'Applicant_type' => $applicant_type, 'Registration_date' => $registrationDate, 'Address' => $address, 'street_address' => $street_address, 'locality' => $locality, 'Postcode' => $postal_code);
            }
            $response = array('StatusCode' => $StatusCode, 'result' => $companyInfo);
        } catch (Exception $exception) {
            $StatusCode = $exception->getResponse()->getStatusCode();
            $result = array('error' => 'sorry the page could not be found');
            $response = array('StatusCode' => $StatusCode, 'result' => $result);
        }
        return $response;
    }

    public function multipleSms($body)
    {
        $url = 'https://4x9ln.api.infobip.com/sms/2/text/single';
        $client = new Client();
        $headers = array('Accept' => 'application/json','Content-Type'=>'application/json','Authorization' => 'Basic ' . $_ENV['INFO_BIP_KEY']);
        // Get the actual response without headers
        try {
            $request = $client->request('POST', $url, array('headers' => $headers, 'body' => json_encode($body)));
            $StatusCode = $request->getStatusCode();
            if ($StatusCode == '200') {
                $res = json_decode($request->getBody()->getContents());
                $response = array('StatusCode' => $StatusCode, 'description' => 'Request successful', 'result' => $res);
            } else {
                $response = array('StatusCode' => $StatusCode, 'description' => 'Request unsuccessful', 'result' => '');
            }
        } catch (Exception $exception) {
            $StatusCode = $exception->getResponse()->getStatusCode();
            $response = array('StatusCode' => $StatusCode, 'result' => $exception->getResponse()->getBody()->getContents());
        }
        return $response;
    }


    public function sendSms($body)
    {
        $url = 'https://4x9ln.api.infobip.com/sms/1/text/single';
        $client = new Client();
        $headers = array('Accept' => 'application/json', 'Authorization' => 'Basic ' . $_ENV['INFO_BIP_KEY']);
        // Get the actual response without headers
        try {
            $request = $client->request('POST', $url, array('headers' => $headers, 'form_params' => $body));
            $StatusCode = $request->getStatusCode();
            if ($StatusCode == '200') {
                $res = json_decode($request->getBody()->getContents());
                $response = array('StatusCode' => $StatusCode, 'description' => 'Request successful', 'result' => $res);
            } else {
                $response = array('StatusCode' => $StatusCode, 'description' => 'Request un successful', 'result' => '');
            }
        } catch (Exception $exception) {
            $StatusCode = $exception->getResponse()->getStatusCode();
            $result = array('error' => 'sorry the page could not be found');
            $response = array('StatusCode' => $StatusCode, 'result' => $result);
        }
        return $response;
    }

    public function sendJobSms($jobDetails)
    {
        $customer = $jobDetails['data']['first_name'] . ' ' . $jobDetails['data']['last_name'];
        $mobile = $jobDetails['data']['mobile_number'];
        $email = $jobDetails['data']['email'];
        $address = $jobDetails['job_detail']['job_address'];
        $service = $jobDetails['service_detail']['service_name'];
        $cost = $jobDetails['job_detail']['transaction_cost'];
        $job_id = $jobDetails['job_detail']['job_id'];
        $seconds = $jobDetails['appointment_detail']['appointment_date'] / 1000;
        $a_date = Carbon::createFromTimestamp($seconds, 'Europe/London')->format('D, M d, Y');
        $s_time = Carbon::createFromTimestamp($seconds, 'Europe/London')->format('H:i');
        $text = 'Thank you for ordering with Skrap! Your ' . $service . ' has been ordered for ' . $a_date . ' ' . $s_time . ' at ' . $address . '. For any issue with your delivery contact Skrap 0330 133 1561.';
        $text1 = 'A Customer Placed an order! ' . $service . ' has been ordered for ' . $a_date . ' ' . $s_time . ' at ' . $address . '. Order Id: SK' . $job_id . '';
        $body = array('to' => $mobile, 'text' => $text);
        $body1 = array('to' => $_ENV['ADMINMOBILE'], 'text' => $text1);
        $this->sendSms($body);
        $this->sendSms($body1);
        return 1;
    }
}
