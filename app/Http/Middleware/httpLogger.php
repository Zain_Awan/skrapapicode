<?php

namespace App\Http\Middleware;

use Closure;
use File;

class httpLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public $start;
    public function handle($request, Closure $next)
    {
        $this->start = microtime(true);
        return $next($request);
    }
    public function terminate($request, $response)
    {
        $this->end = microtime(true);

        $this->log($request, $response);
    }

    protected function log($request, $response)
    {
        $date_time = date("d-m-Y (D) H:i:s");
        $duration = $this->end - $this->start;
        $url = $request->fullUrl();
        $method = $request->getMethod();
        $ip = $request->getClientIp();
        $status = $response->getStatusCode();
        //$headers = $request->getHeaders();
        $todays = date("d-m-Y (D)");
        $context =  array(
            'Request_Url'=>"{$url}",
            'Request_method'=>$method,
            'Request_Status_code'=>$status,
            'Request_Ip'=>$ip,
            'Requested at'=>$date_time,
            'Request' => "[{$request}]",
            'Response' => "[{$response}]",
            'Request time'=>"{$duration}",
        );
        if(strpos($url, 'getHttpLogs') == false && strpos($url, 'SkrapLogs') == false && strpos($url, 'assets') == false && strpos($url, 'getNotificationList') == false){
            if (file_exists(storage_path('logs/http'.$todays.'.log'))) {
                echo 'file exists';
                    exit;
            } else {
                echo 'file not exists';
                fopen(storage_path('logs/http'.$todays.'.log'), "w");
                exit;
            }
            File::append(storage_path('logs/http'.$todays.'.log'),print_r($context, true));
        }
    }
}
