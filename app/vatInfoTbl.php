<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vatInfoTbl extends Model
{
    protected $table = 'tbl_vat_detail';
    public $timestamps = false;

    protected function insertion($vatInfo){
        $result = vatInfoTbl::insert($vatInfo);
        return $result;
    }
    protected function vatNumCheck($full_vat_number){
        $result = vatInfoTbl::select('vat_number')->where('query','=',$full_vat_number)->get();
        return $result;
    }
}
