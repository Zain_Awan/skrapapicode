<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class userInfoTbl extends Model
{
    protected $table = 'tbl_user_info';
    public $timestamps = false;

    protected function insertion($data){
    	$result = userInfoTbl::insert($data);
     	return $result;
    }
    protected function emailCheck($email,$user_type){
        $userExists = DB::table('tbl_users')
            ->leftJoin('tbl_user_info','tbl_users.user_id','=','tbl_user_info.user_id')
            ->select('tbl_users.user_id')
            ->where('tbl_users.user_type', $user_type)
            ->where('tbl_user_info.email', $email)
            ->get();
        return $userExists;
    }
    protected function mobileCheck($mobile,$user_type){
        $result = DB::table('tbl_users')
            ->leftJoin('tbl_user_info','tbl_users.user_id','=','tbl_user_info.user_id')
            ->select('tbl_users.user_id')
            ->where('tbl_users.user_type', $user_type)
            ->where('tbl_user_info.mobile_number', $mobile)
            ->get();
        return $result;
    }
    protected function selection(){

    }
    protected function selectSingle($id){
    	$result = userInfoTbl::select('first_name','last_name','mobile_number','email','profile_pic','profile_pic as profile_pic_android','is_messages')->where('status','=',0)->where('is_delete','=',0)->where('user_id','=',$id)->get();
        return $result;
    }
    protected function updateAddress($address,$user_id){
        $result = userInfoTbl::where('user_id',$user_id)->update($address);
        return $result;
    }
    protected function getBillingAddress($user_id){
        $result = userInfoTbl::select('building','street','town','country','post_code','dob')
            ->where('user_id',$user_id)
            ->where('status',0)
            ->where('is_delete',0)
            ->where('building','!=', '')
            ->where('street','!=', '')
            ->where('town','!=', '')
            ->where('country','!=', '')
            ->where('post_code','!=', '')
            ->where('dob','!=', '')
            ->get();
        return $result;
    }
    protected function updation(){

    }
    protected function deletion(){

    }
    protected function getcustomerEmailPhone($user_id){
        $result = userInfoTbl::select('first_name','last_name','mobile_number','email')
            ->where('status','=',0)
            ->where('is_delete','=',0)
            ->where('user_id','=',$user_id)
            ->get();
        return $result;
    }
    protected function enableMarketingMsg($user_id, $status){
        $result = userInfoTbl::where('user_id', '=', $user_id)->update(array('is_messages'=>$status));
        return $result;
    }
}
