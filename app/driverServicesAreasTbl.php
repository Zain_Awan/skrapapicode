<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class driverServicesAreasTbl extends Model
{
    protected $table = 'tbl_driver_service_areas';
    public $timestamps = false;
    protected function insertion($data){
        $result = driverServicesAreasTbl::insert($data);
        return $result;
    }
    protected function deleteServiceAreas($driverIds, $districtIds){
        $result = driverServicesAreasTbl::whereIn('driver_id',$driverIds)->whereIn('district_code_id',$districtIds)->delete();
        return $result;
    }
    protected function deleteServiceAreasSingle($driverId, $districtIds){
        $result = driverServicesAreasTbl::where('driver_id','=',$driverId)->whereIn('district_code_id',$districtIds)->delete();
        return $result;
    }
}
