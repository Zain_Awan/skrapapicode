<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stripeRefunTbl extends Model
{
    protected $table = 'tbl_stripe_customer_refund';
    public $timestamps = false;
    protected function refundInsert($refund){
        $result = stripeRefunTbl::insert($refund);
        return $result;
    }
}
