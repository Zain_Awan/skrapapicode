<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class companyTbl extends Model
{
    protected $table = 'tbl_company';
    public $timestamps = false;

    protected function insertion($data){
    	$result = companyTbl::insert($data);
     	return $result;
    }
    protected function selection(){

    }
    protected function selectSingle(){
    	
    }
    protected function compNumCheck($comp_number){

        $result = companyTbl::select('Registration_number')->where('Registration_number','=',$comp_number)->get();
        return $result;

    }
    protected function updation(){

    }
    protected function deletion(){

    }
}
