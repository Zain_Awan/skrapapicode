<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class providerWalletTbl extends Model
{
    protected $table = 'tbl_provider_wallet';
    public $timestamps = false;
    protected function insertWallet($data){
        $result = providerWalletTbl::insert($data);
        return $result;
    }
    protected function updateWallet($data, $user_id){
        $result = providerWalletTbl::where('provider_user_id',$user_id)->update($data);
        return $result;
    }
    protected function selectSingle($user_id){
        $result = providerWalletTbl::select('provider_user_id','provider_mango_user','provider_wallet')
            ->where('status',0)
            ->where('is_delete',0)
            ->where('provider_user_id',$user_id)
            ->get();
        return $result;
    }
}
