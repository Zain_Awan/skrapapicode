<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class couponTbl extends Model
{
    protected $table = 'tbl_coupon';
    public $timestamps = false;

    protected function insertCoupon($coupon){
        $couponIn = couponTbl::insert($coupon);
        if($couponIn == 1){
            $result = couponTbl::select('coupon_id')->orderBy('coupon_id', 'desc')->first();
            return $result;
        }else {
            return $couponIn;
        }
    }
    protected function selectCoupon($type){
        $result = couponTbl::select('coupon_id','valid_for_days','max_uses')
            ->where('coupon_type','=',$type)
            ->where('status','=',0)
            ->where('is_delete','=',0)
            ->get();
        return $result;
    }
    protected function findActiveCoupon($user_id){
        $result = DB::table('tbl_coupon')
            ->leftJoin('tbl_coupon_user', function($leftjoin) {
                $leftjoin->on('tbl_coupon_user.coupon_id','=','tbl_coupon.coupon_id')
                    ->on('tbl_coupon_user.uses','<','tbl_coupon.max_uses');
            })
             ->select('tbl_coupon_user.coupon_user_id','tbl_coupon_user.uses','tbl_coupon.coupon_id','tbl_coupon.coupon_code','tbl_coupon.discount','tbl_coupon.max_discount','tbl_coupon.is_fixed','tbl_coupon.is_service','tbl_coupon.is_service_area')
            ->where('tbl_coupon_user.user_id', '=', $user_id)
            ->where('tbl_coupon_user.is_delete','=',0)
            ->where('tbl_coupon_user.status','=',0)
            ->where('tbl_coupon.is_delete','=',0)
            ->where('tbl_coupon.status','=',0)
            ->whereRaw('FROM_UNIXTIME(tbl_coupon_user.start_date/1000, "%Y-%m-%d") <=  DATE_FORMAT(NOW(),"%Y-%m-%d") and FROM_UNIXTIME(tbl_coupon_user.end_date/1000, "%Y-%m-%d") >=  DATE_FORMAT(NOW(),"%Y-%m-%d")')
            ->where('tbl_coupon_user.is_active','=',1)
            ->limit(1)
            ->get();
        return $result;
    }
    protected function couponWithService($service_id, $coupon_id){
        $result = DB::table('tbl_coupon')
            ->leftJoin('tbl_coupon_user', function($leftjoin) {
                $leftjoin->on('tbl_coupon_user.coupon_id','=','tbl_coupon.coupon_id')
                    ->on('tbl_coupon_user.uses','<','tbl_coupon.max_uses');
            })
            ->select('tbl_coupon_user.coupon_user_id','tbl_coupon_user.uses','tbl_coupon.coupon_id','tbl_coupon.coupon_code','tbl_coupon.discount','tbl_coupon.max_discount','tbl_coupon.is_fixed','tbl_coupon.is_service','tbl_coupon.is_service_area')
            ->where('tbl_coupon_service.user_id', '=', $service_id)
            ->where('tbl_coupon_service.is_delete','=',0)
            ->where('tbl_coupon_service.status','=',0)
            ->where('tbl_coupon.is_delete','=',0)
            ->where('tbl_coupon.status','=',0)
            ->where('tbl_coupon.coupon_id','=',$coupon_id)
            ->get();
        return $result;
    }
    protected function couponWithArea($area_id, $coupon_id){
        $result = DB::table('tbl_coupon')
            ->leftJoin('tbl_coupon_user', function($leftjoin) {
                $leftjoin->on('tbl_coupon_user.coupon_id','=','tbl_coupon.coupon_id')
                    ->on('tbl_coupon_user.uses','<','tbl_coupon.max_uses');
            })
            ->select('tbl_coupon_user.coupon_user_id','tbl_coupon_user.uses','tbl_coupon.coupon_id','tbl_coupon.coupon_code','tbl_coupon.discount','tbl_coupon.max_discount','tbl_coupon.is_fixed','tbl_coupon.is_service','tbl_coupon.is_service_area')
            ->where('tbl_coupon_area.user_id', '=', $area_id)
            ->where('tbl_coupon_area.is_delete','=',0)
            ->where('tbl_coupon_area.status','=',0)
            ->where('tbl_coupon.is_delete','=',0)
            ->where('tbl_coupon.status','=',0)
            ->where('tbl_coupon.coupon_id','=',$coupon_id)
            ->get();
        return $result;
    }
    protected function findCoupon($user_id, $coupon){
        $result = DB::table('tbl_coupon')
            ->leftJoin('tbl_coupon_user', function($leftjoin) {
                $leftjoin->on('tbl_coupon_user.coupon_id','=','tbl_coupon.coupon_id')
                    ->on('tbl_coupon_user.uses','<','tbl_coupon.max_uses');
            })
            ->select('tbl_coupon_user.coupon_user_id','tbl_coupon.coupon_id','tbl_coupon.discount','tbl_coupon.max_discount','tbl_coupon.is_fixed','tbl_coupon.is_service','tbl_coupon.is_service_area')
            ->where('tbl_coupon_user.user_id', '=', $user_id)
            ->where('tbl_coupon.coupon_code', '=', $coupon)
            ->where('tbl_coupon_user.is_delete','=',0)
            ->where('tbl_coupon_user.status','=',0)
            ->where('tbl_coupon.is_delete','=',0)
            ->where('tbl_coupon.status','=',0)
            ->whereRaw('FROM_UNIXTIME(tbl_coupon_user.start_date/1000, "%Y-%m-%d") <=  DATE_FORMAT(NOW(),"%Y-%m-%d") and FROM_UNIXTIME(tbl_coupon_user.end_date/1000, "%Y-%m-%d") >=  DATE_FORMAT(NOW(),"%Y-%m-%d")')
            ->get();
        return $result;
    }
    protected function getCoupons($user_id){
        $result = DB::table('tbl_coupon')
            ->leftJoin('tbl_coupon_user', function($leftjoin) {
                $leftjoin->on('tbl_coupon_user.coupon_id','=','tbl_coupon.coupon_id')
                    ->on('tbl_coupon_user.uses','<','tbl_coupon.max_uses');
            })
            ->select('tbl_coupon.coupon_id','tbl_coupon.coupon_code','tbl_coupon.title','tbl_coupon.short_desc','tbl_coupon.ful_desc','tbl_coupon.media','tbl_coupon.discount','tbl_coupon.max_discount','tbl_coupon.max_uses','tbl_coupon.is_fixed','tbl_coupon.is_service','tbl_coupon.is_service_area','tbl_coupon.status','tbl_coupon.is_delete','tbl_coupon.save_date','tbl_coupon_user.start_date','tbl_coupon_user.end_date','tbl_coupon_user.is_active')
            ->where('tbl_coupon_user.user_id', '=', $user_id)
            ->where('tbl_coupon_user.is_delete','=',0)
            ->where('tbl_coupon_user.status','=',0)
            ->where('tbl_coupon.is_delete','=',0)
            ->where('tbl_coupon.status','=',0)
            ->where('tbl_coupon.coupon_type','!=',4)
            ->whereRaw('FROM_UNIXTIME(tbl_coupon_user.start_date/1000, "%Y-%m-%d") <=  DATE_FORMAT(NOW(),"%Y-%m-%d") and FROM_UNIXTIME(tbl_coupon_user.end_date/1000, "%Y-%m-%d") >=  DATE_FORMAT(NOW(),"%Y-%m-%d")')
            ->get();
        return $result;
    }
    protected function getGenericCoupon(){
        $result = DB::table('tbl_coupon')
            ->select('tbl_coupon.coupon_id','tbl_coupon.coupon_code','tbl_coupon.title','tbl_coupon.short_desc','tbl_coupon.ful_desc','tbl_coupon.media','tbl_coupon.discount','tbl_coupon.max_discount','tbl_coupon.max_uses','tbl_coupon.is_fixed','tbl_coupon.is_service','tbl_coupon.is_service_area','tbl_coupon.status','tbl_coupon.is_delete','tbl_coupon.save_date','tbl_coupon.start_date','tbl_coupon.end_date')
            ->where('tbl_coupon.is_delete','=',0)
            ->where('tbl_coupon.status','=',0)
            ->where('tbl_coupon.coupon_type','=',4)
            ->whereRaw('FROM_UNIXTIME(tbl_coupon.start_date/1000, "%Y-%m-%d") <=  DATE_FORMAT(NOW(),"%Y-%m-%d") and FROM_UNIXTIME(tbl_coupon.end_date/1000, "%Y-%m-%d") >=  DATE_FORMAT(NOW(),"%Y-%m-%d")')
            ->get();
        return $result;
    }
    protected function getCouponList(){
        $result = couponTbl::select('coupon_id','coupon_code','short_desc','title','ful_desc','coupon_type','valid_for_days','discount','max_discount','max_uses','save_date','is_service','is_service_area','media','is_fixed')
            ->orderBy('coupon_id', 'desc')
            ->get();
        return $result;
    }
    protected function deactivateAllCoupons($user_id){

    }
    protected function getRefCoupon(){
        $result = couponTbl::select('coupon_id','coupon_code','short_desc','title','ful_desc','coupon_type','valid_for_days','discount','max_discount','max_uses','save_date','is_service','is_service_area','is_fixed','media')
            ->where('coupon_id','=', 4)
            ->orderBy('coupon_id', 'desc')
            ->get();
        return $result;
    }
    protected function getgenCoupon($coupon){
        $result = DB::table('tbl_coupon')
            ->select('tbl_coupon.coupon_id','tbl_coupon.coupon_code','tbl_coupon.short_desc','tbl_coupon.title','tbl_coupon.ful_desc','tbl_coupon.coupon_type','tbl_coupon.valid_for_days','tbl_coupon.discount','tbl_coupon.max_discount','tbl_coupon.max_uses','tbl_coupon.save_date','tbl_coupon.is_service','tbl_coupon.is_service_area','tbl_coupon.is_fixed','tbl_coupon.media','tbl_coupon.start_date','tbl_coupon.end_date')
            ->where('tbl_coupon.coupon_code','=', $coupon)
            ->where('tbl_coupon.coupon_type','=', 4)
            ->whereRaw('FROM_UNIXTIME(tbl_coupon.start_date/1000, "%Y-%m-%d") <=  DATE_FORMAT(NOW(),"%Y-%m-%d") and FROM_UNIXTIME(tbl_coupon.end_date/1000, "%Y-%m-%d") >=  DATE_FORMAT(NOW(),"%Y-%m-%d")')
            ->get();
        return $result;
    }
    protected function deleteCoupon($coupon_id){
        $result = couponTbl::where('coupon_id','=',$coupon_id)->delete();
        return $result;
    }
    protected function updateCoupon($coupon_id, $data){
        $result = couponTbl::where('coupon_id','=',$coupon_id)->update($data);
        return $result;
    }
}
