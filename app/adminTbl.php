<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class adminTbl extends Model
{
    public $timestamps = false;
    protected function getJobsCount(){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->count();
        return $result;
    }
    protected function getCurrentJobsCount(){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_appointments.appointment_status','!=',3)
            ->where('tbl_appointments.cancel_status','!=',1)
            ->count();
        return $result;
    }
    protected function getCompletedJobsCount(){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_appointments.appointment_status','=',3)
            ->where('tbl_appointments.cancel_status','!=',1)
            ->count();
        return $result;
    }
    protected function getCancelledJobsCount(){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',1)
            ->count();
        return $result;
    }
    protected function getFailedJobsCount(){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_driver', 'tbl_job.driver_user_id', '=', 'tbl_driver.user_id')
            ->leftJoin('tbl_provider','tbl_provider.provider_id','=','tbl_driver.provider_id')
            ->leftJoin('tbl_appointments', 'tbl_job.job_id', '=', 'tbl_appointments.job_id')
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->where('tbl_appointments.is_delete','=',1)
            ->count();
        return $result;
    }
    protected function getTotalEarnings(){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->whereRaw('(tbl_job.is_paid = 0 OR tbl_job.is_paid = 1 OR tbl_job.is_paid = 2 OR tbl_job.is_paid = 3 OR tbl_job.is_paid = 4)')
            ->get();
        return $ProviderEarnings;
    }
    protected function getPendingEarnings(){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->whereRaw('(tbl_job.is_paid = 0 OR tbl_job.is_paid = 1 OR tbl_job.is_paid = 2)')
            ->get();
        return $ProviderEarnings;
    }
    protected function getProcessedEarnings(){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->where('tbl_job.is_paid','=',3)
            ->get();
        return $ProviderEarnings;
    }
    protected function getCancelledEarnings(){
        $ProviderEarnings = DB::table('tbl_appointments')
            ->leftJoin('tbl_job', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_driver', 'tbl_driver.user_id', '=', 'tbl_job.driver_user_id')
            ->leftJoin('tbl_provider', 'tbl_provider.provider_id', '=', 'tbl_driver.provider_id')
            ->select(DB::raw('SUM(tbl_job.transaction_cost) As transactionCost'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',1)
            ->whereRaw('(tbl_appointments.appointment_type = 1 OR tbl_appointments.appointment_type = 2 )')
            ->where('tbl_provider.status','=',0)
            ->where('tbl_provider.is_delete','=',0)
            ->where('tbl_driver.status','=',0)
            ->where('tbl_driver.is_delete','=',0)
            ->whereRaw('(tbl_job.is_paid = 0 OR tbl_job.is_paid = 1 OR tbl_job.is_paid = 2 OR tbl_job.is_paid = 4)')
            ->get();
        return $ProviderEarnings;
    }
    protected function getOneMonthUsers($oneMonth){
        $oneMonthUsers = DB::table('tbl_users')
            ->leftJoin('tbl_user_info', 'tbl_user_info.user_id', '=', 'tbl_users.user_id')
            ->select(DB::raw('DATE_FORMAT(tbl_users.save_date,"%Y-%m-%d") As Date , count(tbl_users.user_id) as Users'))
            ->where('tbl_users.status','=',0)
            ->where('tbl_users.is_delete','=',0)
            ->where('tbl_user_info.status','=',0)
            ->where('tbl_user_info.is_delete','=',0)
            ->where('tbl_users.user_type','=',1)
            ->whereRaw('DATE_FORMAT(tbl_users.save_date,"%Y-%m-%d") >= DATE_FORMAT("'.$oneMonth.'","%Y-%m-%d") and DATE_FORMAT(tbl_users.save_date,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-%d")')
            //->whereRaw('DATE_FORMAT(NOW(),"%Y-%m-%d") >= DATE_FORMAT(tbl_users.save_date,"%Y-%m-%d")')
            ->groupBy(DB::raw('DATE_FORMAT(tbl_users.save_date,"%Y-%m-%d")'))
            ->get();
        return $oneMonthUsers;
    }
    protected function getOneMonthJobs($oneMonth){
        $oneMonthUsers = DB::table('tbl_job')
            ->leftJoin('tbl_appointments', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->select(DB::raw('DATE_FORMAT(tbl_job.save_date,"%Y-%m-%d") As Date , count(tbl_job.job_id) as Jobs'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->whereRaw('DATE_FORMAT(tbl_job.save_date,"%Y-%m-%d") >= DATE_FORMAT("'.$oneMonth.'","%Y-%m-%d") and DATE_FORMAT(tbl_job.save_date,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-%d")')
            //->whereRaw('DATE_FORMAT(NOW(),"%Y-%m-%d") >= DATE_FORMAT(tbl_users.save_date,"%Y-%m-%d")')
            ->groupBy(DB::raw('DATE_FORMAT(tbl_job.save_date,"%Y-%m-%d")'))
            ->get();
        return $oneMonthUsers;
    }
    protected function getOneMonthEarnings($oneMonth){
        $oneMonthUsers = DB::table('tbl_job')
            ->leftJoin('tbl_appointments', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->select(DB::raw('DATE_FORMAT(tbl_job.save_date,"%Y-%m-%d") As Date , SUM(tbl_job.transaction_cost) as Earnings'))
            ->where('tbl_job.status','=',0)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_appointments.cancel_status','=',0)
            ->whereRaw('DATE_FORMAT(tbl_job.save_date,"%Y-%m-%d") >= DATE_FORMAT("'.$oneMonth.'","%Y-%m-%d") and DATE_FORMAT(tbl_job.save_date,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-%d")')
            //->whereRaw('DATE_FORMAT(NOW(),"%Y-%m-%d") >= DATE_FORMAT(tbl_users.save_date,"%Y-%m-%d")')
            ->groupBy(DB::raw('DATE_FORMAT(tbl_job.save_date,"%Y-%m-%d")'))
            ->get();
        return $oneMonthUsers;
    }
}
