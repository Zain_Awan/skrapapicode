<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class driverJobAreaTbl extends Model
{
    protected $table='tbl_drive_job_area';
    public $timestamps = false;

    protected function insertion($jobArea){
        $result = driverJobAreaTbl::insert($jobArea);
        return $result;
    }
}
