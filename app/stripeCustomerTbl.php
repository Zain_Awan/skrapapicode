<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stripeCustomerTbl extends Model
{
    protected $table = 'tbl_stripe_customer';
    public $timestamps = false;

    protected function insertCustomer($customer){
        $result = stripeCustomerTbl::insert($customer);
        return $result;
    }
    protected function checkStripeCustomerExists($user_id){
        $result = stripeCustomerTbl::select('customer_id')
            ->where('user_id','=', $user_id)
            ->where('status','=', 0)
            ->where('is_delete', '=', 1)
            ->get();
        return $result;
    }
    protected function selectCustomerId($user_id){
        $result = stripeCustomerTbl::select('stripe_customer_id','customer_id')
            ->where('user_id','=', $user_id)
            ->where('status','=', 0)
            ->where('is_delete','=', 0)
            ->get();
        return $result;
    }
}
