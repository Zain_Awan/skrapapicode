<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class custCardsTbl extends Model
{
    protected $table = 'tbl_cust_cards';
    public $timestamps = false;

    protected function insertion($data){
        $result = custCardsTbl::insert($data);
        return $result;
    }
    protected function selectCreateId(){
        $result = custCardsTbl::select('create_id')->orderBy('cust_card_id', 'desc')->first();
        return $result;
    }
    protected function removeDefault($user_id){
        $result = custCardsTbl::where('user_id','=',$user_id)->update(array('is_default'=>0));
        return $result;
    }
    protected function addDefault($card_id){
        $result = custCardsTbl::where('card_id','=',$card_id)->update(array('is_default'=>1));
        return $result;
    }
    protected function getDefaultCard($user_id){
        $result = custCardsTbl::select('card_id')->where('user_id','=',$user_id)->where('is_default','=',1)->get();
        return $result;
    }
}
