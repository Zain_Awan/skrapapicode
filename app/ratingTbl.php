<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ratingTbl extends Model
{
    protected $table = 'tbl_rating';
    public $timestamps = false;

    protected function insertion($data){
    	$result = ratingTbl::insert($data);
     	return $result;
    }
    protected function selection(){

    }
    protected function selectSingle(){
    	
    }
    protected function updation(){

    }
    protected function deletion(){

    }
}
