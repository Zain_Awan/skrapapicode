<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class driverVehicleTbl extends Model
{
    protected $table = 'tbl_driver_vehicle';
    public $timestamps = false;

    protected function insertion($data){
    	$result = driverVehicleTbl::insert($data);
    	return $result;
    }
    protected function vehicleDriverCheck($driver_id){
        $result = driverVehicleTbl::select('driver_id')->where('driver_id','=',$driver_id)->where('status','=', 0)->where('is_delete','=', 0)->get();
        return $result;
    }
    protected function UnAssignVehicle($driver_id, $vehicle_id){
        $data = array('is_delete'=>1);
        $result = driverVehicleTbl::where('driver_id','=',$driver_id)->where('vehicle_id','=',$vehicle_id)->where('status','=', 0)->where('is_delete','=', 0)->update($data);
        return $result;
    }
}
