<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class jobTbl extends Model
{
   	protected $table = 'tbl_job';
    public $timestamps = false;

	protected function insertion($data){
    	$result = jobTbl::insert($data);
    	if($result == 1){
    	$res = jobTbl::select('job_id')->where('is_delete',0)->orderBy('job_id', 'desc')->first();
    	return $res;
    	}else{
    		return $result;
    	}
    }
    protected function updateJob($data,$job_id){
        $res = jobTbl::where('job_id','=',$job_id)->update($data);
        return $res;
    }
    protected function updateDriveId($job_id,$driver){
        $res = jobTbl::where('job_id','=',$job_id)->update($driver);
        return $res;
    }
    protected function selectSingle($job_id){
        $res = jobTbl::select()->where('status','=',0)->where('is_delete','=',0)->where('job_id','=',$job_id)->get();
        return $res;
    }
    protected function checkAccepted($job_id){
        $res = jobTbl::select('driver_user_id','job_id','status')
            ->where('job_id','=',$job_id)
            ->get();
        return $res;
    }
    protected function getJobData($job_id){
        $res = jobTbl::select('is_permit','skip_req_days','skip_loc_type','card_id','transaction_cost','customer_user_id')->where('job_id','=',$job_id)->where('status','!=',2)->where('is_delete','=',0)->get();
        return $res;
    }
    protected function UnAssignDriverJob($job_id){
        $res = jobTbl::where('job_id',$job_id)->update(array('driver_user_id'=>null));
        return $res;
    }
    protected function updatePaymentStatus($job_id,$status){
        $res = jobTbl::where('job_id',$job_id)->update(array('is_paid'=>$status));
        return $res;
    }
    protected function updatePaymentStatusManual($job_ids){
        $res = jobTbl::whereIn('job_id',$job_ids)->update(array('is_paid'=>3));
        return $res;
    }
    protected function getjobCoupon($appointment_id){
        $jobCoupon = DB::table('tbl_job')
            ->leftJoin('tbl_appointments', 'tbl_appointments.job_id', '=', 'tbl_job.job_id')
            ->leftJoin('tbl_coupon_user', 'tbl_coupon_user.coupon_user_id', '=', 'tbl_job.coupon_id')
            ->select('tbl_coupon_user.coupon_user_id','tbl_job.job_id','tbl_appointments.appointment_id','tbl_coupon_user.uses')
            ->where('tbl_appointments.status','=',0)
            ->where('tbl_appointments.is_delete','=',0)
            ->where('tbl_job.status','!=',2)
            ->where('tbl_job.is_delete','=',0)
            ->where('tbl_job.is_coupon','=',1)
            ->where('tbl_appointments.appointment_id','=',$appointment_id)
            ->get();
        return $jobCoupon;
    }
    protected function getjobDetail($job_id){
        $result = jobTbl::select('job_address','service_id','job_start_time','job_end_time')
            ->where('job_id','=',$job_id)
            ->get();
        return $result;
    }
    protected function getjobSerDetail($job_id){
        $result = DB::table('tbl_job')
            ->leftJoin('tbl_services', 'tbl_services.service_id', '=', 'tbl_job.service_id')
            ->select('tbl_job.job_address','tbl_services.service_name','tbl_job.job_start_time','tbl_job.job_end_time','tbl_job.transaction_cost','tbl_job.skip_loc_type')
            ->where('job_id','=',$job_id)
            ->get();
        return $result;
    }
    protected function getCouponId($job_id){
        $result = jobTbl::select('coupon_id')
            ->where('job_id','=',$job_id)
            ->get();
        return $result;
    }
    protected function cancelJobReq($job_id){
        $result = jobTbl::where('job_id','=',$job_id)
            ->update(array('status'=>2));
        return $result;
    }
    protected function updateExtention($job_id,$update_job){
        $result = jobTbl::where('job_id','=',$job_id)
            ->update($update_job);
        return $result;
    }
    protected function updateJobCardId($job_id,$card_id){
        $result = jobTbl::where('job_id','=',$job_id)
            ->update(array('card_id'=> $card_id));
        return $result;
    }
}
