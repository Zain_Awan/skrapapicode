<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cronAlertTbl extends Model
{
    protected $table = 'tbl_cron_alert';
    public $timestamps = false;

    protected function getCronJobList(){
        $res = cronAlertTbl::select('alert_id','alert_time','alert_range','alert_title','alert_body')
            ->where('alert_status','=',0)
            ->where('is_delete','=',0)
            ->get();
        return $res;
    }
}
