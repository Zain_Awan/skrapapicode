<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class providerBankManualTbl extends Model
{
    protected $table = 'tbl_provider_bank_manual';
    public $timestamps = false;

    protected function insertion($accountInfo){
        $result = providerBankManualTbl::insert($accountInfo);
        return $result;
    }
    protected function getAccountDetail($provider_id){
        $result = providerBankManualTbl::select('bank_id','provider_id','account_number','account_name','sort_code','account_type','status','save_date')
            ->where('provider_id','=',$provider_id)
            ->get();
        return $result;
    }
    protected function updateAccountInfo($accountDetail,$bank_id){
        $result = providerBankManualTbl::where('bank_id','=', $bank_id)->update($accountDetail);
        return $result;
    }
}
