<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class couponServicesTbl extends Model
{
    protected $table = 'tbl_coupon_service';
    public $timestamps = false;

    protected function insertServices($data){
        $result = couponServicesTbl::insert($data);
        return $result;
    }
}
