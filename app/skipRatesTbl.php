<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class skipRatesTbl extends Model
{
    protected $table = 'tbl_skip_rates';
    public $timestamps = false;

    protected function getAll(){
        $result = skipRatesTbl::select('service_rate_id','district_id','district_code','Borough','service6','service7',
            'service8','service9','service10','service11','service12','service13','service14','service16','service17',
            'service18','service19','service20','service21','service22','service23','service24','service25')
            ->groupBy('district_code')
            ->get();
        return $result;
    }
    protected function insertRates($data){
        $result = skipRatesTbl::insert($data);
        return $result;
    }
    protected function updateServiceRate($rate_id, $data){
        $result = skipRatesTbl::where('service_rate_id', '=', $rate_id)->update($data);
        return $result;
    }
    protected function deleteServiceRate($rate_id){
        $result = skipRatesTbl::where('service_rate_id', '=', $rate_id)->delete();
        return $result;
    }
}
