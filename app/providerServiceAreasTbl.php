<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class providerServiceAreasTbl extends Model
{
    protected $table = 'tbl_provider_service_areas';
    public $timestamps = false;
    protected function insertion($data){
        $result = providerServiceAreasTbl::insert($data);
        return $result;
    }
    protected function deleteServiceArea($serviceAreaids, $provider_id){
        $result = providerServiceAreasTbl::where('provider_id','=',$provider_id)->whereIn('district_code_id',$serviceAreaids)->delete();
        return $result;
    }
}
