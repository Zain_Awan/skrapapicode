<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class driverServicesTbl extends Model
{
    protected $table = "tbl_driver_services";
    public $timestamps = false;

    protected function insertion($service){
    	$result = driverServicesTbl::insert($service);
    	return $result;
    }
    protected function DeleteService($service_id, $driver_id, $data){
        $result = driverServicesTbl::where('driver_id', '=', $driver_id)->where('service_id','=', $service_id)->update($data);
        return $result;
    }
    protected function deleteServices($driverIds, $service_id){
        $result = driverServicesTbl::where('service_id','=', $service_id)->whereIn('driver_id',$driverIds)->update(array('is_delete'=>1));
        return $result;
    }
}
