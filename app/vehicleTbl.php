<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vehicleTbl extends Model
{
    protected $table = 'tbl_vehicals';
    public $timestamps = false;

    protected function insertion($data){
    	$result = vehicleTbl::insert($data);
     	return $result;
    }
    protected function vehicleCheck($reg_number){
        $result = vehicleTbl::select('reg_number')->where('reg_number','=',$reg_number)->where('status','=', 0)->where('is_delete','=', 0)->get();
        return $result;
    }
    protected function selection(){

    }
    protected function getVehicleDetail($vehicle_id){
    	$result = vehicleTbl::select('reg_number','make','model','vehicle_type','owner_name','vehicle_id','status','doc_image','save_date')->where('vehicle_id','=',$vehicle_id)->get();
    	return $result;
    }
    protected function updation($vehicle_id,$updateVehicle){
        $result = vehicleTbl::where('vehicle_id','=',$vehicle_id)->update($updateVehicle);
        return $result;
    }
    protected function deletion(){

    }
}
