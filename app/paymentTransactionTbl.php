<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymentTransactionTbl extends Model
{
    protected $table = 'tbl_payment_transaction';
    public $timestamps = false;

    protected function insertion($paymentInfo){
        $result = paymentTransactionTbl::insert($paymentInfo);
        return $result;
    }
}
