<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\cronJobController;
use Mail;
use File;

class cronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:cronJob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'testing cron Job every 15 minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        Log::info("hi from send __construct");
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new cronJobController(); // make sure to import the controller
        $response = $controller->cronJobAppointments();
        if($response['code'] = 0){
            File::append(storage_path('logs/cron.log'),'Cron Job Notification sent');
        }
        /*Mail::send('emails.forgotPass',['data'=>'testCronJob'],function ($message){
            $message->from('info@skrap.xyz', 'Skrap');
            $message->to('zain@celvas.com.pk')->subject('cronJob testing email');
        });*/
    }
}
