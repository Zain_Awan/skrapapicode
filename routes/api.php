<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->post('details', 'API\UserController@details');
Route::middleware('auth:api')->get('home', 'Apicontroller@home')->name('home');

///////////////// Services Routes /////////////////////////////

Route::middleware('auth:web')->post('getServices','Apicontroller@getServices')->name('getServices');
Route::middleware('auth:api')->post('getProviderServices','Apicontroller@getProviderServices')->name('getProviderServices');
Route::middleware('auth:api')->post('getProviderUnassignedServices','Apicontroller@getProviderUnassignedServices')->name('getProviderUnassignedServices');
Route::middleware('auth:api')->post('getDriverUnAssignedServices','Apicontroller@getDriverUnAssignedServices')->name('getDriverUnAssignedServices');
Route::middleware('auth:api')->post('getServiceRates','Apicontroller@getServiceRates')->name('getServiceRates');
Route::middleware('auth:api')->post('getSkipPermitRate','Apicontroller@getSkipPermitRate')->name('getSkipPermitRate');
Route::middleware('auth:api')->post('AddDriverService','Apicontroller@AddDriverService')->name('AddDriverService');
Route::middleware('auth:api')->post('AddProviderService','Apicontroller@AddProviderService')->name('AddProviderService');
Route::middleware('auth:api')->post('DeleteProviderService','Apicontroller@DeleteProviderService')->name('DeleteProviderService');
Route::middleware('auth:api')->post('AddProviderServiceAreas','Apicontroller@AddProviderServiceAreas')->name('AddProviderServiceAreas');
Route::middleware('auth:api')->post('DeleteProviderServiceAreas','Apicontroller@DeleteProviderServiceAreas')->name('DeleteProviderServiceAreas');
Route::middleware('auth:api')->post('getDriverServiceAreas','Apicontroller@getDriverServiceAreas')->name('getDriverServiceAreas');
Route::middleware('auth:api')->post('getDriverUnAssignServiceAreas','Apicontroller@getDriverUnAssignServiceAreas')->name('getDriverUnAssignServiceAreas');
Route::middleware('auth:api')->post('AddDriverServiceAreas','Apicontroller@AddDriverServiceAreas')->name('AddDriverServiceAreas');
Route::middleware('auth:api')->post('DeleteDriverServiceAreas','Apicontroller@DeleteDriverServiceAreas')->name('DeleteDriverServiceAreas');

/////////////////// Vehicle Routes ///////////////////////

Route::middleware('auth:api')->post('addVehicle','Apicontroller@addVehicle')->name('addVehicle');
Route::middleware('auth:api')->post('getVehiclesListing','Apicontroller@getVehiclesListing')->name('getVehiclesListing');
Route::middleware('auth:api')->post('getProviderUnAssignedVehicles','Apicontroller@getProviderUnAssignedVehicles')->name('getProviderUnAssignedVehicles');
Route::middleware('auth:api')->post('assignDriveVehicle','Apicontroller@assingDriverToVehicle')->name('assignDriveVehicle');
Route::middleware('auth:api')->post('DeleteDriverService','Apicontroller@DeleteDriverService')->name('DeleteDriverService');
Route::middleware('auth:api')->post('getVehicle','Apicontroller@getVehicle')->name('getVehicle');
Route::middleware('auth:api')->post('updateVehicleInfo','Apicontroller@updateVehicleInfo')->name('updateVehicleInfo');

/////////////////// User Routes ///////////////////////

Route::middleware('auth:web')->post('registerProvider', 'Apicontroller@registerProvider')->name('registerProvider');
Route::middleware('auth:web')->post('registerCustomer', 'Apicontroller@registerCustomer')->name('registerCustomer');
Route::middleware('auth:api')->post('addDriver','Apicontroller@addDriver')->name('addDriver');
Route::middleware('auth:web')->post('login', 'Apicontroller@login')->name('login');
Route::middleware('auth:api')->post('getDrversListing','Apicontroller@getDrversListing')->name('getDrversListing');
Route::middleware('auth:api')->get('verification','Apicontroller@verifyUser')->name('verification');
Route::middleware('auth:web')->post('userExists','Apicontroller@checkUserEmail')->name('userExists');
Route::middleware('auth:web')->post('checkUserMobile','Apicontroller@checkUserMobile')->name('checkUserMobile');
Route::middleware('auth:web')->post('compExists','Apicontroller@checkCompNumber')->name('compExists');
Route::middleware('auth:web')->post('vatExists','Apicontroller@checkVatNumber')->name('vatExists');
Route::middleware('auth:api')->post('updateDeviceId','Apicontroller@updateDeviceId')->name('updateDeviceId');
Route::middleware('auth:api')->post('updateDriverOnlineStatus','Apicontroller@updateDriverOnlineStatus')->name('updateDriverOnlineStatus');
Route::middleware('auth:api')->get('getDriverOnlineStatus/{id}','Apicontroller@getDriverOnlineStatus')->name('getDriverOnlineStatus');
Route::middleware('auth:api')->post('changePassword','Apicontroller@changePassword')->name('changePassword');
Route::middleware('auth:web')->post('forgotPassword','Apicontroller@forgotPassword')->name('forgotPassword');
Route::middleware('auth:api')->post('getUserProfile','Apicontroller@getUserProfile')->name('getUserProfile');
Route::middleware('auth:api')->post('updateUserProfile','Apicontroller@updateUserProfile')->name('updateUserProfile');
Route::middleware('auth:api')->post('getDriver','Apicontroller@getDriver')->name('getDriver');
Route::middleware('auth:api')->post('UnAssignDriverVehicle','Apicontroller@UnAssignDriverVehicle')->name('UnAssignDriverVehicle');
Route::middleware('auth:api')->post('addDriverJobArea','Apicontroller@addDriverJobArea')->name('addDriverJobArea');
Route::middleware('auth:web')->post('getCompnyInfo','Apicontroller@getCompnyInfo')->name('getCompnyInfo');
Route::middleware('auth:api')->post('updateDriverInfo','Apicontroller@updateDriverInfo')->name('updateDriverInfo');
Route::middleware('auth:api')->post('giveUserRating','Apicontroller@giveUserRating')->name('giveUserRating');
Route::middleware('auth:api')->post('checkUserPassword','Apicontroller@checkOldPassword')->name('checkUserPassword');
Route::middleware('auth:api')->post('changeDriverJobReceiveStatus','Apicontroller@changeDriverJobReceiveStatus')->name('changeDriverJobReceiveStatus');
Route::middleware('auth:api')->post('getProviderInstances','Apicontroller@getProviderInstances')->name('getProviderInstances');
Route::middleware('auth:api')->post('getUserCredentials','adminApiController@getUserCredentials')->name('getUserCredentials');
/////////////////// Job Routes ///////////////////////

Route::middleware('auth:api')->post('jobRequest','Apicontroller@jobRequest')->name('jobRequest');
Route::middleware('auth:api')->post('jobAccepted','Apicontroller@jobAccepted')->name('jobAccepted');
Route::middleware('auth:api')->post('getJobDetail','Apicontroller@getJobDetail')->name('getJobDetail');
Route::middleware('auth:api')->post('getCurrentJob','Apicontroller@getCurrentJob')->name('getCurrentJob');
Route::middleware('auth:api')->post('getCurrentJobListing','Apicontroller@getCurrentJobListing')->name('getCurrentJobListing');
Route::middleware('auth:api')->post('getPreviousJobListing','Apicontroller@getPreviousJobListing')->name('getPreviousJobListing');
Route::middleware('auth:api')->post('getAppointment','Apicontroller@getAppointment')->name('getAppointment');
Route::middleware('auth:api')->post('updateJobStatus','Apicontroller@updateJobStatus')->name('updateJobStatus');
Route::middleware('auth:api')->post('cancelAppointment','Apicontroller@cancelAppointment')->name('cancelAppointment');
Route::middleware('auth:api')->get('cities','Apicontroller@cities')->name('cities');
Route::middleware('auth:api')->post('getProviderJobListing','Apicontroller@getProviderJobListing')->name('getProviderJobListing');
Route::middleware('auth:api')->post('getProviderJobDetail','Apicontroller@getProviderJobDetail')->name('getProviderJobDetail');
Route::middleware('auth:api')->post('findProviderDrivers','Apicontroller@findProviderDrivers')->name('findProviderDrivers');
Route::middleware('auth:api')->post('getPendingJobListing','Apicontroller@getPendingJobListing')->name('getPendingJobListing');
Route::middleware('auth:api')->post('getTimeSlots','Apicontroller@getTimeSlots')->name('getTimeSlots');
Route::middleware('auth:api')->post('findAutoDrivers','adminApiController@findAutoDrivers')->name('findAutoDrivers');
Route::middleware('auth:api')->post('getAutoAcceptJobListing','adminApiController@getAutoAcceptJobListing')->name('getAutoAcceptJobListing');
Route::middleware('auth:api')->post('getExtensions','Apicontroller@getExtentions')->name('getExtensions');
Route::middleware('auth:api')->post('addExtensions','Apicontroller@addExtention')->name('addExtensions');
Route::middleware('auth:api')->post('requestCollection','Apicontroller@requestCollection')->name('requestCollection');
Route::middleware('auth:api')->post('chargeCustomer','Apicontroller@chargeCustomer')->name('chargeCustomer');

/////////////////// mango pay Routes///////////////////////

Route::middleware('auth:api')->post('createMangoUser','Apicontroller@createMangoUser')->name('createMangoUser');
Route::middleware('auth:api')->post('createCompUserWallet','Apicontroller@createCompUserWallet')->name('createCompUserWallet');
Route::middleware('auth:api')->post('createCard','Apicontroller@createCard')->name('createCard');
Route::middleware('auth:api')->get('cardFinal/{id}','MangoPayController@cardFinal')->name('cardFinal');
Route::middleware('auth:api')->post('getBillingAddress','Apicontroller@getBillingAddress')->name('getBillingAddress');
Route::middleware('auth:api')->post('getCardList','Apicontroller@getCardList')->name('getCardList');
Route::middleware('auth:api')->post('deactivateCard','Apicontroller@deactivateCard')->name('deactivateCard');
Route::middleware('auth:api')->post('viewWallet','Apicontroller@viewWallet')->name('viewWallet');
Route::middleware('auth:api')->post('setDefaultCard','Apicontroller@setDefaultCard')->name('setDefaultCard');
Route::middleware('auth:api')->post('getDefaultCard','Apicontroller@getDefaultCard')->name('getDefaultCard');

/////////////////// test services Routes /////////////////////////////

Route::middleware('auth:api')->get('getCities','Apicontroller@getCities')->name('getCities');
Route::middleware('auth:web')->post('getTowns','Apicontroller@getTowns')->name('getTowns');
Route::middleware('auth:api')->get('payintest','PaymentController@cardPayIn')->name('payintest');
Route::middleware('auth:api')->get('viewWallet','PaymentController@viewWallet')->name('viewWallet');
Route::middleware('auth:api')->get('nodetest','Apicontroller@saveApiData')->name('nodetest');
Route::middleware('auth:api')->post('getProviderAccountDetail','Apicontroller@getProviderAccountDetail')->name('getProviderAccountDetail');
Route::middleware('auth:api')->post('updateProviderAccountDetail','Apicontroller@updateProviderAccountDetail')->name('updateProviderAccountDetail');
Route::middleware('auth:api')->post('test','Apicontroller@testCheck')->name('test');
Route::middleware('auth:api')->get('testTrans','PaymentController@testTrans')->name('testTrans');
Route::middleware('auth:api')->post('getdistrictCodes','Apicontroller@getdistrictCodes')->name('getdistrictCodes');
Route::middleware('auth:api')->post('getProviderServiceAreas','Apicontroller@getProviderServiceAreas')->name('getProviderServiceAreas');
Route::middleware('auth:api')->post('getProviderUnServiceAreas','Apicontroller@getProviderUnassignedServiceAreas')->name('getProviderUnServiceAreas');
Route::middleware('auth:api')->post('findDrivers','nodeApiController@findDrivers')->name('findDrivers');
Route::middleware('auth:api')->post('FCMtest','FCMcontroller@FCMtest')->name('FCMtest');
Route::middleware('auth:api')->get('emailTest','Apicontroller@emailTest')->name('emailTest');
Route::middleware('auth:web')->get('getHttpLogs','Apicontroller@getHttpLogs')->name('getHttpLogs');
Route::middleware('auth:api')->post('reportAnIssue','Apicontroller@reportAnIssue')->name('reportAnIssue');

/////////////////// Notifications services Routes /////////////////////////////

Route::middleware('auth:api')->post('updateNotiReadStatus','Apicontroller@updateNotiReadStatus')->name('updateNotiReadStatus');
Route::middleware('auth:api')->post('getNotificationList','Apicontroller@getNotificationList')->name('getNotificationList');

////////////////////////////// Admin Routes //////////////////////////////////////

Route::middleware('auth:api')->post('getAllJobs','adminApiController@getAllJobs')->name('getAllJobs');
Route::middleware('auth:api')->post('getProviderListing','adminApiController@getProviderListing')->name('getProviderListing');
Route::middleware('auth:api')->post('getProviderDetail','adminApiController@getProviderDetail')->name('getProviderDetail');
Route::middleware('auth:api')->post('verifyProvider', 'adminApiController@verifyProvider')->name('verifyProvider');
Route::middleware('auth:api')->post('getProviderEarnings','adminApiController@getProviderEarnings')->name('getProviderEarnings');
Route::middleware('auth:api')->post('getProviderTotalEarnings','adminApiController@getProviderTotalEarnings')->name('getProviderTotalEarnings');
Route::middleware('auth:api')->post('providerTransferPayout','adminApiController@providerTransferPayout')->name('providerTransferPayout');
Route::middleware('auth:api')->get('cronJob','Apicontroller@cronJob')->name('cronJob');
Route::middleware('auth:api')->post('stripeTest','stripeController@stripeTest')->name('stripeTest');
Route::middleware('auth:api')->get('getMatchedDistrics','Apicontroller@getMatchedDistrics')->name('getMatchedDistrics');
Route::middleware('auth:api')->post('ActivateCoupon','Apicontroller@ActivateCoupon')->name('ActivateCoupon');
Route::middleware('auth:api')->post('getCoupons','Apicontroller@getCoupons')->name('getCoupons');
Route::middleware('auth:api')->post('getCustomerListing','adminApiController@getCustomerListing')->name('getCustomerListing');
Route::middleware('auth:api')->post('getAdminInstances','adminApiController@getAdminInstances')->name('getAdminInstances');
Route::middleware('auth:api')->post('getUsersChartData','adminApiController@getUsersChartData')->name('getUsersChartData');
Route::middleware('auth:api')->post('getJobsChartData','adminApiController@getJobsChartData')->name('getJobsChartData');
Route::middleware('auth:api')->post('getOneMonthEarningsData','adminApiController@getOneMonthEarningsData')->name('getOneMonthEarningsData');
Route::middleware('auth:api')->post('deleteCoupon','adminApiController@deleteCoupon')->name('deleteCoupon');
Route::middleware('auth:api')->post('getAllServiceRates','adminApiController@getAllServiceRates')->name('getAllServiceRates');
Route::middleware('auth:api')->post('getAllAreaCodes','adminApiController@getAllAreaCodes')->name('getAllAreaCodes');
Route::middleware('auth:api')->post('AddServiceRates','adminApiController@AddServiceRates')->name('AddServiceRates');
Route::middleware('auth:api')->post('updateServiceRates','adminApiController@updateServiceRates')->name('updateServiceRates');
Route::middleware('auth:api')->post('deleteServiceRate','adminApiController@deleteServiceRate')->name('deleteServiceRate');
Route::middleware('auth:api')->post('updateCoupon','adminApiController@updateCoupon')->name('updateCoupon');
Route::middleware('auth:api')->post('updatePaymentStatus','adminApiController@updatePaymentStatus')->name('updatePaymentStatus');
Route::middleware('auth:api')->post('sendMarketingMsg','adminApiController@sendMarketingMsg')->name('sendMarketingMsg');
Route::middleware('auth:api')->post('enableMarketingMsg','adminApiController@enableMarketingMsg')->name('enableMarketingMsg');

///////////////////////////////// REPORTS ROUTES //////////////////////////////////////////

Route::middleware('auth:api')->post('getProviderPaymentReport','Apicontroller@getProviderPaymentReport')->name('getProviderPaymentReport');
Route::middleware('auth:api')->post('createReferal','Apicontroller@createReferal')->name('createReferal');
Route::middleware('auth:api')->post('AssignCoupon','Apicontroller@AssignCoupon')->name('AssignCoupon');
Route::middleware('auth:api')->get('cronJobs','Apicontroller@cronJobs')->name('cronJobs');
Route::middleware('auth:api')->get('AssignCoupon','HelperMethods@AssignCoupon')->name('AssignCoupon');
Route::middleware('auth:api')->post('deleteUser','Apicontroller@deleteUser')->name('deleteUser');
Route::middleware('auth:api')->post('extraCharge','Apicontroller@extraCharge')->name('extraCharge');
Route::middleware('auth:api')->post('createCoupon','Apicontroller@createCoupon')->name('createCoupon');
Route::middleware('auth:api')->post('getCouponList','Apicontroller@getCouponList')->name('getCouponList');
Route::middleware('auth:api')->post('cancelJobCheck','Apicontroller@cancelJobCheck')->name('cancelJobCheck');
Route::middleware('auth:api')->post('updateJob','Apicontroller@updateJobData')->name('updateJob');
Route::middleware('auth:api')->get('getRefCoupon','Apicontroller@getRefCoupon')->name('getRefCoupon');
Route::middleware('auth:api')->post('jobfailedCheck','Apicontroller@jobfailedCheck')->name('jobfailedCheck');
Route::middleware('auth:web')->post('testcron','Apicontroller@testcron')->name('testcron');
Route::middleware('auth:web')->get('sendGenNoti','Apicontroller@sendGenNoti')->name('sendGenNoti');
Route::middleware('auth:api')->post('cancelJobRequest','Apicontroller@cancelJobRequest')->name('cancelJobRequest');