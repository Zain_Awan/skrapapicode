<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth:web')->get('SkrapLogs', 'Webcontroller@SkrapLogs')->name('SkrapLogs');
Route::middleware('auth:web')->get('stripeTest', 'Webcontroller@stripeTesting')->name('stripeTest');
Route::middleware('auth:web')->post('getAccessToken', 'API\UserController@login');
Route::middleware('auth:web')->post('register', 'API\UserController@register');
