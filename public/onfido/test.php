<?php
require_once(__DIR__ . '/autoload.php');

// Configure API key authorization: Token
Onfido\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'token=' . 'test_kqDNdtkrRqjBV4xhngYxkQgLpq9K-e55');
Onfido\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Token');

$api_instance = new Onfido\Api\DefaultApi();

		// setting applicant details
		$applicant = new Onfido\Models\Applicant();
		$applicant->setFirstName($firstname);
		$applicant->setLastName($lastname);
		$applicant->setDob(new DateTime($dob));
		$applicant->setEmail($email);
		$applicant->setCountry($country);

		$address = new Onfido\Models\Address();
		$address->setBuildingNumber($building);
		$address->setStreet($street);
		$address->setTown($town);
		$address->setPostcode($postcode);
		$address->setCountry($country);

		$applicant->setAddresses(array($address));

		//$type = $idtype;
		//$side = 'front';
		//$file = "user_ids/$imgname";

		// setting check request details
		$check = new Onfido\Models\CheckCreationRequest();
		$check->setType('express');

		$report = new Onfido\Models\Report();
		$report->setName('identity');
		//$reportdoc = new Onfido\Models\Report();
		//$reportdoc->setName('document');

		$check->setReports(array($report));

		
		    $result = $api_instance->createApplicant($applicant);
		    $applicant_id = $result->getId();
		    //$result = $api_instance->uploadDocument($applicant_id,$type,$side,$file);
		    $result = $api_instance->createCheck($applicant_id, $check);

		    print_r($result);
		

?>